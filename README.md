# Sapienza - Statistics Homeworks

In this repository I will manintain projects for the Statistics course of the Cybersecurity Master degree in Sapienza.
- Website: https://mazzinistatistics.wordpress.com/

# Index

- [Homework 1](./homework1/)
- [Homework 2](./homework2/)
- [Homework 3](./homework3/)
- [Homework 4](./homework4/)
- [Homework 5](./homework5/)
- [Homework 6](./homework6/)
- [Homework 7](./homework7/)
- [Homework 8](./homework8/)
- [Homework 9](./homework9/)
