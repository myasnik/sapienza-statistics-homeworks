﻿
namespace Statistics___Homework_8B
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.numVarsBar = new System.Windows.Forms.TrackBar();
            this.varLowerBoundLabel = new System.Windows.Forms.Label();
            this.varNumLabel = new System.Windows.Forms.Label();
            this.varUpperBoundLabel = new System.Windows.Forms.Label();
            this.upperBoundRepLabel = new System.Windows.Forms.Label();
            this.repetitions = new System.Windows.Forms.Label();
            this.lowerBoundRepLabel = new System.Windows.Forms.Label();
            this.repetitionsBar = new System.Windows.Forms.TrackBar();
            this.histPosUpperBoundLabel = new System.Windows.Forms.Label();
            this.histPosLabel = new System.Windows.Forms.Label();
            this.histPosLowerBoundLabel = new System.Windows.Forms.Label();
            this.histPosBar = new System.Windows.Forms.TrackBar();
            this.pictureBox = new System.Windows.Forms.PictureBox();
            this.runButton = new System.Windows.Forms.Button();
            this.bernProbLab = new System.Windows.Forms.Label();
            this.upperLambdaLab = new System.Windows.Forms.Label();
            this.lowerLambdaLab = new System.Windows.Forms.Label();
            this.bernProbBar = new System.Windows.Forms.TrackBar();
            this.relRadio = new System.Windows.Forms.RadioButton();
            this.absRadio = new System.Windows.Forms.RadioButton();
            this.normRadio = new System.Windows.Forms.RadioButton();
            ((System.ComponentModel.ISupportInitialize)(this.numVarsBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repetitionsBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.histPosBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bernProbBar)).BeginInit();
            this.SuspendLayout();
            // 
            // numVarsBar
            // 
            this.numVarsBar.Location = new System.Drawing.Point(66, 46);
            this.numVarsBar.Maximum = 10000;
            this.numVarsBar.Minimum = 100;
            this.numVarsBar.Name = "numVarsBar";
            this.numVarsBar.Size = new System.Drawing.Size(179, 45);
            this.numVarsBar.TabIndex = 0;
            this.numVarsBar.Value = 100;
            this.numVarsBar.ValueChanged += new System.EventHandler(this.numVarsBar_ValueChanged);
            // 
            // varLowerBoundLabel
            // 
            this.varLowerBoundLabel.AutoSize = true;
            this.varLowerBoundLabel.Location = new System.Drawing.Point(66, 76);
            this.varLowerBoundLabel.Name = "varLowerBoundLabel";
            this.varLowerBoundLabel.Size = new System.Drawing.Size(25, 15);
            this.varLowerBoundLabel.TabIndex = 1;
            this.varLowerBoundLabel.Text = "100";
            // 
            // varNumLabel
            // 
            this.varNumLabel.AutoSize = true;
            this.varNumLabel.Location = new System.Drawing.Point(97, 24);
            this.varNumLabel.Name = "varNumLabel";
            this.varNumLabel.Size = new System.Drawing.Size(114, 15);
            this.varNumLabel.TabIndex = 2;
            this.varNumLabel.Text = "Number of variables";
            // 
            // varUpperBoundLabel
            // 
            this.varUpperBoundLabel.AutoSize = true;
            this.varUpperBoundLabel.Location = new System.Drawing.Point(214, 75);
            this.varUpperBoundLabel.Name = "varUpperBoundLabel";
            this.varUpperBoundLabel.Size = new System.Drawing.Size(37, 15);
            this.varUpperBoundLabel.TabIndex = 3;
            this.varUpperBoundLabel.Text = "10000";
            // 
            // upperBoundRepLabel
            // 
            this.upperBoundRepLabel.AutoSize = true;
            this.upperBoundRepLabel.Location = new System.Drawing.Point(432, 75);
            this.upperBoundRepLabel.Name = "upperBoundRepLabel";
            this.upperBoundRepLabel.Size = new System.Drawing.Size(31, 15);
            this.upperBoundRepLabel.TabIndex = 7;
            this.upperBoundRepLabel.Text = "3000";
            // 
            // repetitions
            // 
            this.repetitions.AutoSize = true;
            this.repetitions.Location = new System.Drawing.Point(346, 24);
            this.repetitions.Name = "repetitions";
            this.repetitions.Size = new System.Drawing.Size(66, 15);
            this.repetitions.TabIndex = 6;
            this.repetitions.Text = "Repetitions";
            // 
            // lowerBoundRepLabel
            // 
            this.lowerBoundRepLabel.AutoSize = true;
            this.lowerBoundRepLabel.Location = new System.Drawing.Point(284, 76);
            this.lowerBoundRepLabel.Name = "lowerBoundRepLabel";
            this.lowerBoundRepLabel.Size = new System.Drawing.Size(25, 15);
            this.lowerBoundRepLabel.TabIndex = 5;
            this.lowerBoundRepLabel.Text = "100";
            // 
            // repetitionsBar
            // 
            this.repetitionsBar.Location = new System.Drawing.Point(284, 46);
            this.repetitionsBar.Maximum = 3000;
            this.repetitionsBar.Minimum = 100;
            this.repetitionsBar.Name = "repetitionsBar";
            this.repetitionsBar.Size = new System.Drawing.Size(179, 45);
            this.repetitionsBar.TabIndex = 4;
            this.repetitionsBar.Value = 100;
            // 
            // histPosUpperBoundLabel
            // 
            this.histPosUpperBoundLabel.AutoSize = true;
            this.histPosUpperBoundLabel.Location = new System.Drawing.Point(649, 75);
            this.histPosUpperBoundLabel.Name = "histPosUpperBoundLabel";
            this.histPosUpperBoundLabel.Size = new System.Drawing.Size(31, 15);
            this.histPosUpperBoundLabel.TabIndex = 11;
            this.histPosUpperBoundLabel.Text = "3000";
            // 
            // histPosLabel
            // 
            this.histPosLabel.AutoSize = true;
            this.histPosLabel.Location = new System.Drawing.Point(532, 24);
            this.histPosLabel.Name = "histPosLabel";
            this.histPosLabel.Size = new System.Drawing.Size(109, 15);
            this.histPosLabel.TabIndex = 10;
            this.histPosLabel.Text = "Histogram position";
            // 
            // histPosLowerBoundLabel
            // 
            this.histPosLowerBoundLabel.AutoSize = true;
            this.histPosLowerBoundLabel.Location = new System.Drawing.Point(501, 76);
            this.histPosLowerBoundLabel.Name = "histPosLowerBoundLabel";
            this.histPosLowerBoundLabel.Size = new System.Drawing.Size(19, 15);
            this.histPosLowerBoundLabel.TabIndex = 9;
            this.histPosLowerBoundLabel.Text = "10";
            // 
            // histPosBar
            // 
            this.histPosBar.Location = new System.Drawing.Point(501, 46);
            this.histPosBar.Maximum = 3000;
            this.histPosBar.Minimum = 10;
            this.histPosBar.Name = "histPosBar";
            this.histPosBar.Size = new System.Drawing.Size(179, 45);
            this.histPosBar.TabIndex = 8;
            this.histPosBar.Value = 10;
            // 
            // pictureBox
            // 
            this.pictureBox.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBox.Location = new System.Drawing.Point(13, 113);
            this.pictureBox.Name = "pictureBox";
            this.pictureBox.Size = new System.Drawing.Size(1336, 586);
            this.pictureBox.TabIndex = 12;
            this.pictureBox.TabStop = false;
            // 
            // runButton
            // 
            this.runButton.Location = new System.Drawing.Point(1135, 24);
            this.runButton.Name = "runButton";
            this.runButton.Size = new System.Drawing.Size(175, 65);
            this.runButton.TabIndex = 13;
            this.runButton.Text = "Run simulation";
            this.runButton.UseVisualStyleBackColor = true;
            this.runButton.Click += new System.EventHandler(this.runButton_Click);
            // 
            // bernProbLab
            // 
            this.bernProbLab.AutoSize = true;
            this.bernProbLab.Location = new System.Drawing.Point(764, 24);
            this.bernProbLab.Name = "bernProbLab";
            this.bernProbLab.Size = new System.Drawing.Size(120, 15);
            this.bernProbLab.TabIndex = 17;
            this.bernProbLab.Text = "Bernoulli λ parameter";
            // 
            // upperLambdaLab
            // 
            this.upperLambdaLab.AutoSize = true;
            this.upperLambdaLab.Location = new System.Drawing.Point(888, 77);
            this.upperLambdaLab.Name = "upperLambdaLab";
            this.upperLambdaLab.Size = new System.Drawing.Size(25, 15);
            this.upperLambdaLab.TabIndex = 20;
            this.upperLambdaLab.Text = "500";
            // 
            // lowerLambdaLab
            // 
            this.lowerLambdaLab.AutoSize = true;
            this.lowerLambdaLab.Location = new System.Drawing.Point(734, 77);
            this.lowerLambdaLab.Name = "lowerLambdaLab";
            this.lowerLambdaLab.Size = new System.Drawing.Size(25, 15);
            this.lowerLambdaLab.TabIndex = 19;
            this.lowerLambdaLab.Text = "100";
            // 
            // bernProbBar
            // 
            this.bernProbBar.Location = new System.Drawing.Point(734, 47);
            this.bernProbBar.Maximum = 500;
            this.bernProbBar.Minimum = 100;
            this.bernProbBar.Name = "bernProbBar";
            this.bernProbBar.Size = new System.Drawing.Size(179, 45);
            this.bernProbBar.TabIndex = 18;
            this.bernProbBar.Value = 100;
            this.bernProbBar.Scroll += new System.EventHandler(this.bernProbBar_Scroll);
            // 
            // relRadio
            // 
            this.relRadio.AutoSize = true;
            this.relRadio.Location = new System.Drawing.Point(961, 20);
            this.relRadio.Name = "relRadio";
            this.relRadio.Size = new System.Drawing.Size(122, 19);
            this.relRadio.TabIndex = 21;
            this.relRadio.TabStop = true;
            this.relRadio.Text = "Relative frequency";
            this.relRadio.UseVisualStyleBackColor = true;
            this.relRadio.CheckedChanged += new System.EventHandler(this.relRadio_CheckedChanged);
            // 
            // absRadio
            // 
            this.absRadio.AutoSize = true;
            this.absRadio.Location = new System.Drawing.Point(961, 47);
            this.absRadio.Name = "absRadio";
            this.absRadio.Size = new System.Drawing.Size(128, 19);
            this.absRadio.TabIndex = 22;
            this.absRadio.TabStop = true;
            this.absRadio.Text = "Absolute frequency";
            this.absRadio.UseVisualStyleBackColor = true;
            this.absRadio.CheckedChanged += new System.EventHandler(this.absRadio_CheckedChanged);
            // 
            // normRadio
            // 
            this.normRadio.AutoSize = true;
            this.normRadio.Location = new System.Drawing.Point(961, 75);
            this.normRadio.Name = "normRadio";
            this.normRadio.Size = new System.Drawing.Size(142, 19);
            this.normRadio.TabIndex = 23;
            this.normRadio.TabStop = true;
            this.normRadio.Text = "Normalized frequency";
            this.normRadio.UseVisualStyleBackColor = true;
            this.normRadio.CheckedChanged += new System.EventHandler(this.normRadio_CheckedChanged);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1361, 711);
            this.Controls.Add(this.normRadio);
            this.Controls.Add(this.absRadio);
            this.Controls.Add(this.relRadio);
            this.Controls.Add(this.upperLambdaLab);
            this.Controls.Add(this.lowerLambdaLab);
            this.Controls.Add(this.bernProbBar);
            this.Controls.Add(this.bernProbLab);
            this.Controls.Add(this.runButton);
            this.Controls.Add(this.pictureBox);
            this.Controls.Add(this.histPosUpperBoundLabel);
            this.Controls.Add(this.histPosLabel);
            this.Controls.Add(this.histPosLowerBoundLabel);
            this.Controls.Add(this.histPosBar);
            this.Controls.Add(this.upperBoundRepLabel);
            this.Controls.Add(this.repetitions);
            this.Controls.Add(this.lowerBoundRepLabel);
            this.Controls.Add(this.repetitionsBar);
            this.Controls.Add(this.varUpperBoundLabel);
            this.Controls.Add(this.varNumLabel);
            this.Controls.Add(this.varLowerBoundLabel);
            this.Controls.Add(this.numVarsBar);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.numVarsBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repetitionsBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.histPosBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bernProbBar)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TrackBar numVarsBar;
        private System.Windows.Forms.Label varLowerBoundLabel;
        private System.Windows.Forms.Label varNumLabel;
        private System.Windows.Forms.Label varUpperBoundLabel;
        private System.Windows.Forms.Label upperBoundRepLabel;
        private System.Windows.Forms.Label repetitions;
        private System.Windows.Forms.Label lowerBoundRepLabel;
        private System.Windows.Forms.TrackBar repetitionsBar;
        private System.Windows.Forms.Label histPosUpperBoundLabel;
        private System.Windows.Forms.Label histPosLabel;
        private System.Windows.Forms.Label histPosLowerBoundLabel;
        private System.Windows.Forms.TrackBar histPosBar;
        private System.Windows.Forms.PictureBox pictureBox;
        private System.Windows.Forms.Button runButton;
        private System.Windows.Forms.Label bernProbLab;
        private System.Windows.Forms.Label upperLambdaLab;
        private System.Windows.Forms.Label lowerLambdaLab;
        private System.Windows.Forms.TrackBar bernProbBar;
        private System.Windows.Forms.RadioButton relRadio;
        private System.Windows.Forms.RadioButton absRadio;
        private System.Windows.Forms.RadioButton normRadio;
    }
}



