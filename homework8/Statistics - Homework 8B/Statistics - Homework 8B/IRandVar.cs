﻿namespace Statistics___Homework_8B
{
    interface IRandVar
    {
        public string ToString();
        public double GetSample();

        double Mean { get; }
        double StdDev { get; }
        double Variance { get; }
    }
}
