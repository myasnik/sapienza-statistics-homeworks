﻿using System;

namespace Statistics___Homework_8B
{
    public class BernoulliRandomVariable : IRandVar
    {
        Random _random;

        double _p;

        public double Mean => _p;

        public double StdDev => Math.Sqrt(_p * (1 - _p));

        public double Variance => _p * (1 - _p);

        public BernoulliRandomVariable(double p)
        {
            if (!IsValidParameterSet(p))
            {
                throw new ArgumentException("Invalid parametrization for the distribution.");
            }

            _random = new Random();
            _p = p;
        }

        public override string ToString()
        {
            return $"Bernoulli(p = {_p})";
        }

        public bool IsValidParameterSet(double p)
        {
            return p >= 0.0 && p <= 1.0;
        }

        public double GetSample()
        {
            if (_random.NextDouble() < _p)
            {
                return 1;
            }

            return 0;
        }
    }
}