﻿using System;
using System.Collections.Generic;
using System.Drawing;

namespace Statistics___Homework_8B
{
    class Path
    {
        private IRandVar var;
        private List<double> calculatedSequence;
        private List<double> calculatedPath;
        private double positive = 0;

        public Path(IRandVar var, int steps, EnumPathFrequency enumPathFrequency)
        {
            Var = var;
            CalculatedSequence = new List<double>();
            CalculatedPath = new List<double>();

            for (int i = 0; i < steps; i++)
            {
                double sample = Var.GetSample();
                CalculatedSequence.Add(sample);
                if (sample == 1) Positive++;

                switch (enumPathFrequency)
                {
                    case EnumPathFrequency.ABSOLUTE:
                        CalculatedPath.Add(Positive);
                        break;
                    case EnumPathFrequency.RELATIVE:
                        CalculatedPath.Add(Positive / (CalculatedPath.Count + 1));
                        break;
                    case EnumPathFrequency.NORMALIZED:
                        if (i == 0) CalculatedPath.Add(0);
                        else CalculatedPath.Add(Positive / Math.Sqrt(i));
                        break;
                }
            }
        }

        public List<double> CalculatedSequence { get => calculatedSequence; private set => calculatedSequence = value; }
        public List<double> CalculatedPath { get => calculatedPath; private set => calculatedPath = value; }
        public double Positive { get => positive; private set => positive = value; }
        internal IRandVar Var { get => var; set => var = value; }

        public void DrawPath(ViewPort view, Color color)
        {
            List<PointF> points = new List<PointF>();
            for (int i = 0; i < CalculatedPath.Count; i++)
            {
                points.Add(new PointF(i, (float)CalculatedPath[i]));
            }
            view.DrawCurve(color, points);
        }
    }
}
