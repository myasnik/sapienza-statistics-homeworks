﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace Statistics___Homework_8B
{
    public partial class Form1 : Form
    {
        Graphics g;
        ViewPort vp;
        ViewPort endHistVp;
        ViewPort midHistVp;
        ViewPort jumpsOrigHistVp;
        ViewPort jumpsConsHistVp;

        Random rand;
        List<Path> paths;
        EnumPathFrequency enumPathFrequency;
        List<List<double>> jumpsFromOrigin;
        List<List<double>> jumpsCons;

        double maxXJumps = 0;
        double maxYJumps = 0;
        double intervalsNumber = 20;
        double intervalSize = 0;

        public Form1()
        {
            InitializeComponent();
            SetStyle(ControlStyles.OptimizedDoubleBuffer | ControlStyles.AllPaintingInWmPaint | ControlStyles.UserPaint, true);
            UpdateStyles();
        }
        private void Form1_Load(object sender, EventArgs e)
        {
            rand = new Random();
            pictureBox.Image = new Bitmap(pictureBox.Width, pictureBox.Height);
            g = Graphics.FromImage(pictureBox.Image);

            relRadio.Checked = true;
            enumPathFrequency = EnumPathFrequency.RELATIVE;

            bernProbBar.Maximum = numVarsBar.Value;
            upperLambdaLab.Text = bernProbBar.Maximum.ToString();

            runButton.Enabled = false;

            UpdateBars();
        }

        private void numVarsBar_ValueChanged(object sender, EventArgs e)
        {
            UpdateBars();

            bernProbBar.Maximum = numVarsBar.Value;
            upperLambdaLab.Text = bernProbBar.Maximum.ToString();

            if (numVarsBar.Value != bernProbBar.Value) runButton.Enabled = true;
            else runButton.Enabled = false;
        }

        private void Draw()
        {
            paths = new List<Path>();
            jumpsFromOrigin = new List<List<double>>();
            jumpsCons = new List<List<double>>();

            double maxY = 1;
            double minY = 0;

            for (int i = 0; i < repetitionsBar.Value; i++)
            {
                paths.Add(new Path(new BernoulliRandomVariable(bernProbBar.Value / (double)numVarsBar.Value), numVarsBar.Value, enumPathFrequency));
                jumpsFromOrigin.Add(new List<double>());
                jumpsCons.Add(new List<double>());

                int previous = 0;
                bool firstCase = true;
                for (int j = 0; j < paths[i].CalculatedSequence.Count; j++)
                {
                    if (paths[i].CalculatedSequence[j] == 1)
                    {
                        jumpsFromOrigin[i].Add(j);

                        if (!firstCase) jumpsCons[i].Add(j - previous);
                        else firstCase = false;
                        previous = j;
                    }
                }
            }

            switch (enumPathFrequency)
            {
                case EnumPathFrequency.RELATIVE:
                    maxY = 1;
                    break;
                case EnumPathFrequency.ABSOLUTE:
                case EnumPathFrequency.NORMALIZED:
                    foreach (Path p in paths)
                    {
                        foreach (double e in p.CalculatedPath)
                        {
                            if (e > maxY) maxY = e;
                            if (e < minY) minY = e;
                        }
                    }
                    break;
            }

            vp = new ViewPort(g, new Rectangle(0, 0, pictureBox.Width, pictureBox.Height), 0, numVarsBar.Value, minY, maxY);
            endHistVp = new ViewPort(g, new RectangleF(pictureBox.Width - pictureBox.Width / 10, 0, pictureBox.Width / 10, pictureBox.Height), 0, repetitionsBar.Value, minY, maxY);
            midHistVp = new ViewPort(g, new RectangleF(histPosBar.Value * (float)vp.XScale, 0, pictureBox.Width / 10, pictureBox.Height), 0, repetitionsBar.Value, minY, maxY);
            HistogramPlot endHist = new HistogramPlot(minY, Math.Abs(maxY) + Math.Abs(minY), 15, Color.Orange, EnumOrientation.LEFTRIGHT);
            HistogramPlot midHist = new HistogramPlot(minY, Math.Abs(maxY) + Math.Abs(minY), 30, Color.Orange, EnumOrientation.LEFTRIGHT);
            HistogramPlot jumpConsHist = new HistogramPlot(FillJumpHistogram(jumpsCons, intervalsNumber, Color.FromArgb(150, rand.Next(256), rand.Next(256), rand.Next(256))), EnumOrientation.BOTTOMUP, intervalSize);
            jumpsConsHistVp = new ViewPort(g, new RectangleF(0, pictureBox.Height / 2, pictureBox.Width, pictureBox.Height / 2), 1, intervalsNumber, 0, maxYJumps);
            HistogramPlot jumpOrigHist = new HistogramPlot(FillJumpHistogram(jumpsFromOrigin, intervalsNumber, Color.FromArgb(150, rand.Next(256), rand.Next(256), rand.Next(256))), EnumOrientation.BOTTOMUP, intervalSize);
            jumpsOrigHistVp = new ViewPort(g, new RectangleF(0, pictureBox.Height / 2, pictureBox.Width, pictureBox.Height / 2), 0, maxXJumps, 0, maxYJumps);

            maxXJumps = 0;
            maxYJumps = 0;

            for (int i = 0; i < repetitionsBar.Value; i++)
            {
                endHist.AddNumber(paths[i].CalculatedPath[paths[i].CalculatedPath.Count - 1]);
                midHist.AddNumber(paths[i].CalculatedPath[histPosBar.Value - 1]);
            }

            g.Clear(BackColor);
            foreach (Path p in paths)
            {
                p.DrawPath(vp, Color.FromArgb(rand.Next(256), rand.Next(256), rand.Next(256)));
            }
            endHist.DrawPlot(endHistVp);
            midHist.DrawPlot(midHistVp);
            jumpOrigHist.DrawPlot(jumpsOrigHistVp);
            jumpConsHist.DrawPlot(jumpsConsHistVp);
            pictureBox.Refresh();
        }

        private void runButton_Click(object sender, EventArgs e)
        {
            Draw();
        }

        private void UpdateBars()
        {
            histPosUpperBoundLabel.Text = numVarsBar.Value.ToString();
            histPosBar.Maximum = numVarsBar.Value;
            histPosBar.Value = histPosBar.Minimum;
        }

        private void relRadio_CheckedChanged(object sender, EventArgs e)
        {
            if (relRadio.Checked)
            {
                absRadio.Checked = false;
                normRadio.Checked = false;
                enumPathFrequency = EnumPathFrequency.RELATIVE;
            }
        }

        private void absRadio_CheckedChanged(object sender, EventArgs e)
        {
            if (absRadio.Checked)
            {
                relRadio.Checked = false;
                normRadio.Checked = false;
                enumPathFrequency = EnumPathFrequency.ABSOLUTE;
            }
        }

        private void normRadio_CheckedChanged(object sender, EventArgs e)
        {
            if (normRadio.Checked)
            {
                relRadio.Checked = false;
                absRadio.Checked = false;
                enumPathFrequency = EnumPathFrequency.NORMALIZED;
            }
        }

        private List<HistogramUnit> FillJumpHistogram(List<List<double>> values, double intervalsNumber, Color color)
        {
            List<HistogramUnit> intervals = new List<HistogramUnit>();
            double min = double.MaxValue;
            double max = 0;

            foreach (List<double> l in values)
            {
                foreach (double val in l)
                {
                    if (val > max)
                    {
                        max = val;
                    }
                    if (val < min)
                    {
                        min = val;
                    }
                }
            }

            if (max > maxXJumps) maxXJumps = max;
            intervalSize = (max - min) / intervalsNumber;
            for (double i = min; i < max; i += intervalSize)
            {
                intervals.Add(new HistogramUnit(color, new Interval(i, i + intervalSize, true, false)));
            }

            foreach (List<double> l in values)
            {
                foreach (int val in l)
                {
                    foreach (HistogramUnit currentInterval in intervals)
                    {
                        if (currentInterval.Interval.Contains(val))
                        {
                            currentInterval.Interval.AddElement(val);
                        }
                    }
                }
            }

            foreach (HistogramUnit currentInterval in intervals)
            {
                if (currentInterval.Interval.Frequency > maxYJumps)
                {
                    maxYJumps = currentInterval.Interval.Frequency;
                }
            }

            return intervals;
        }

        private void bernProbBar_Scroll(object sender, EventArgs e)
        {
            if (numVarsBar.Value != bernProbBar.Value) runButton.Enabled = true;
            else runButton.Enabled = false;
        }
    }
}

