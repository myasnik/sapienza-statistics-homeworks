﻿
namespace Statistics___Homework_8A
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBox = new System.Windows.Forms.PictureBox();
            this.drawButton = new System.Windows.Forms.Button();
            this.pathsBar = new System.Windows.Forms.TrackBar();
            this.rvBar = new System.Windows.Forms.TrackBar();
            this.rvLab = new System.Windows.Forms.Label();
            this.pathsLab = new System.Windows.Forms.Label();
            this.maxRvLab = new System.Windows.Forms.Label();
            this.pathsMaxLab = new System.Windows.Forms.Label();
            this.minRvLab = new System.Windows.Forms.Label();
            this.pathsMinLab = new System.Windows.Forms.Label();
            this.curRvNumLab = new System.Windows.Forms.Label();
            this.curPathsNumLab = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pathsBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rvBar)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox
            // 
            this.pictureBox.Location = new System.Drawing.Point(13, 84);
            this.pictureBox.Name = "pictureBox";
            this.pictureBox.Size = new System.Drawing.Size(1418, 547);
            this.pictureBox.TabIndex = 0;
            this.pictureBox.TabStop = false;
            // 
            // drawButton
            // 
            this.drawButton.Location = new System.Drawing.Point(1098, 12);
            this.drawButton.Name = "drawButton";
            this.drawButton.Size = new System.Drawing.Size(243, 55);
            this.drawButton.TabIndex = 1;
            this.drawButton.Text = "Draw";
            this.drawButton.UseVisualStyleBackColor = true;
            this.drawButton.Click += new System.EventHandler(this.drawButton_Click);
            // 
            // pathsBar
            // 
            this.pathsBar.Location = new System.Drawing.Point(536, 33);
            this.pathsBar.Maximum = 1000;
            this.pathsBar.Minimum = 100;
            this.pathsBar.Name = "pathsBar";
            this.pathsBar.Size = new System.Drawing.Size(465, 45);
            this.pathsBar.TabIndex = 2;
            this.pathsBar.Value = 100;
            this.pathsBar.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pathsBar_MouseUp);
            // 
            // rvBar
            // 
            this.rvBar.Location = new System.Drawing.Point(13, 33);
            this.rvBar.Maximum = 1000;
            this.rvBar.Minimum = 10;
            this.rvBar.Name = "rvBar";
            this.rvBar.Size = new System.Drawing.Size(465, 45);
            this.rvBar.TabIndex = 3;
            this.rvBar.Value = 10;
            this.rvBar.MouseUp += new System.Windows.Forms.MouseEventHandler(this.rvBar_MouseUp);
            // 
            // rvLab
            // 
            this.rvLab.AutoSize = true;
            this.rvLab.Location = new System.Drawing.Point(203, 9);
            this.rvLab.Name = "rvLab";
            this.rvLab.Size = new System.Drawing.Size(101, 15);
            this.rvLab.TabIndex = 4;
            this.rvLab.Text = "Random variables";
            // 
            // pathsLab
            // 
            this.pathsLab.AutoSize = true;
            this.pathsLab.Location = new System.Drawing.Point(753, 9);
            this.pathsLab.Name = "pathsLab";
            this.pathsLab.Size = new System.Drawing.Size(36, 15);
            this.pathsLab.TabIndex = 5;
            this.pathsLab.Text = "Paths";
            // 
            // maxRvLab
            // 
            this.maxRvLab.AutoSize = true;
            this.maxRvLab.Location = new System.Drawing.Point(451, 63);
            this.maxRvLab.Name = "maxRvLab";
            this.maxRvLab.Size = new System.Drawing.Size(38, 15);
            this.maxRvLab.TabIndex = 6;
            this.maxRvLab.Text = "label1";
            // 
            // pathsMaxLab
            // 
            this.pathsMaxLab.AutoSize = true;
            this.pathsMaxLab.Location = new System.Drawing.Point(973, 63);
            this.pathsMaxLab.Name = "pathsMaxLab";
            this.pathsMaxLab.Size = new System.Drawing.Size(38, 15);
            this.pathsMaxLab.TabIndex = 7;
            this.pathsMaxLab.Text = "label1";
            // 
            // minRvLab
            // 
            this.minRvLab.AutoSize = true;
            this.minRvLab.Location = new System.Drawing.Point(12, 63);
            this.minRvLab.Name = "minRvLab";
            this.minRvLab.Size = new System.Drawing.Size(38, 15);
            this.minRvLab.TabIndex = 8;
            this.minRvLab.Text = "label1";
            // 
            // pathsMinLab
            // 
            this.pathsMinLab.AutoSize = true;
            this.pathsMinLab.Location = new System.Drawing.Point(536, 63);
            this.pathsMinLab.Name = "pathsMinLab";
            this.pathsMinLab.Size = new System.Drawing.Size(38, 15);
            this.pathsMinLab.TabIndex = 9;
            this.pathsMinLab.Text = "label1";
            // 
            // curRvNumLab
            // 
            this.curRvNumLab.AutoSize = true;
            this.curRvNumLab.Location = new System.Drawing.Point(228, 63);
            this.curRvNumLab.Name = "curRvNumLab";
            this.curRvNumLab.Size = new System.Drawing.Size(38, 15);
            this.curRvNumLab.TabIndex = 10;
            this.curRvNumLab.Text = "label1";
            // 
            // curPathsNumLab
            // 
            this.curPathsNumLab.AutoSize = true;
            this.curPathsNumLab.Location = new System.Drawing.Point(751, 63);
            this.curPathsNumLab.Name = "curPathsNumLab";
            this.curPathsNumLab.Size = new System.Drawing.Size(38, 15);
            this.curPathsNumLab.TabIndex = 11;
            this.curPathsNumLab.Text = "label1";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1443, 643);
            this.Controls.Add(this.curPathsNumLab);
            this.Controls.Add(this.curRvNumLab);
            this.Controls.Add(this.pathsMinLab);
            this.Controls.Add(this.minRvLab);
            this.Controls.Add(this.pathsMaxLab);
            this.Controls.Add(this.maxRvLab);
            this.Controls.Add(this.pathsLab);
            this.Controls.Add(this.rvLab);
            this.Controls.Add(this.rvBar);
            this.Controls.Add(this.pathsBar);
            this.Controls.Add(this.drawButton);
            this.Controls.Add(this.pictureBox);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pathsBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rvBar)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox;
        private System.Windows.Forms.Button drawButton;
        private System.Windows.Forms.TrackBar pathsBar;
        private System.Windows.Forms.TrackBar rvBar;
        private System.Windows.Forms.Label rvLab;
        private System.Windows.Forms.Label pathsLab;
        private System.Windows.Forms.Label maxRvLab;
        private System.Windows.Forms.Label pathsMaxLab;
        private System.Windows.Forms.Label minRvLab;
        private System.Windows.Forms.Label pathsMinLab;
        private System.Windows.Forms.Label curRvNumLab;
        private System.Windows.Forms.Label curPathsNumLab;
    }
}

