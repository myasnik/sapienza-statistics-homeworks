﻿using System.Collections.Generic;

namespace Statistics___Homework_8A
{
    class Path
    {
        private OnlineMean mean;
        private List<double> pathVals;
        private IRandVar var;

        public Path(IRandVar var, int steps)
        {
            Var = var;
            mean = new OnlineMean();
            PathVals = new List<double>();

            for (int j = 0; j < steps; j++)
            {
                double val = var.GetSample();
                PathVals.Add(val);
                Mean.AddValue(val);
            }
        }

        public List<double> PathVals { get => pathVals; private set => pathVals = value; }
        public IRandVar Var { get => var; private set => var = value; }
        internal OnlineMean Mean { get => mean; private set => mean = value; }
    }
}
