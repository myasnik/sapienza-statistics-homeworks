﻿using System;

namespace Statistics___Homework_8A
{
    class Interval
    {
        private double lowerEnd;
        private double upperEnd;
        private bool rightComprehend;
        private bool leftComprehend;
        private int frequency = 0;
        private OnlineMean mean;

        public double LowerEnd { get => lowerEnd; private set => lowerEnd = value; }
        public double UpperEnd { get => upperEnd; private set => upperEnd = value; }
        public bool RightComprehend { get => rightComprehend; private set => rightComprehend = value; }
        public bool LeftComprehend { get => leftComprehend; private set => leftComprehend = value; }
        public int Frequency { get => frequency; private set => frequency = value; }
        internal OnlineMean Mean { get => mean; private set => mean = value; }

        public Interval(double lowerEnd, double upperEnd, bool leftComprehend, bool rightComprehend)
        {
            if (lowerEnd > upperEnd)
            {
                throw new ArgumentException("lowerEnd can't be greater than upperEnd");
            }

            LowerEnd = lowerEnd;
            UpperEnd = upperEnd;
            RightComprehend = rightComprehend;
            LeftComprehend = leftComprehend;
            Mean = new OnlineMean();
        }

        public bool Contains(double val)
        {
            if (LeftComprehend && RightComprehend)
            {
                return val <= UpperEnd && val >= LowerEnd;
            }
            if (LeftComprehend)
            {
                return val < UpperEnd && val >= LowerEnd;
            }
            else
            {
                return val <= UpperEnd && val > LowerEnd;
            }
        }

        public bool LessThan(double val)
        {
            if (RightComprehend)
            {
                return UpperEnd < val;
            }
            else
            {
                return UpperEnd <= val;
            }
        }

        public bool MoreThan(double val)
        {
            if (LeftComprehend)
            {
                return LowerEnd > val;
            }
            else
            {
                return LowerEnd >= val;
            }
        }

        public void AddElement(double e)
        {
            Mean.AddValue(e);
            Frequency++;
        }

        public override string ToString()
        {
            string s = "";
            if (LeftComprehend) s += "["; else s += "(";
            s += Math.Round(LowerEnd, 3) + "," + Math.Round(UpperEnd, 3);
            if (RightComprehend) s += "]"; else s += ")";
            return s;
        }

        public bool GenericBoundsEquals(Interval i)
        {
            return i.LowerEnd == LowerEnd && i.UpperEnd == UpperEnd;
        }
    }
}
