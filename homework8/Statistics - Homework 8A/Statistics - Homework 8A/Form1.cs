﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace Statistics___Homework_8A
{
    public partial class Form1 : Form
    {
        Graphics g;
        ViewPort vpGeneral;
        ViewPort vpFirst;
        ViewPort vpLast;

        Random rand = new Random();

        List<double> means;
        List<List<double>> orderedPaths;
        List<double> first;
        List<double> last;
        UniformRandomVariable var;
        List<Path> paths;

        public Form1()
        {
            InitializeComponent();
            SetStyle(ControlStyles.OptimizedDoubleBuffer | ControlStyles.AllPaintingInWmPaint | ControlStyles.UserPaint, true);
            UpdateStyles();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            pictureBox.Image = new Bitmap(pictureBox.Width, pictureBox.Height);
            g = Graphics.FromImage(pictureBox.Image);

            int inf = rand.Next(0, 1000);
            int sup = rand.Next(inf, 1000);
            var = new UniformRandomVariable(inf, sup);

            maxRvLab.Text = rvBar.Maximum.ToString();
            minRvLab.Text = rvBar.Minimum.ToString();
            curRvNumLab.Text = rvBar.Minimum.ToString();
            pathsMaxLab.Text = pathsBar.Maximum.ToString();
            pathsMinLab.Text = pathsBar.Minimum.ToString();
            curPathsNumLab.Text = pathsBar.Minimum.ToString();
        }

        private void drawButton_Click(object sender, EventArgs e)
        {
            means = new List<double>();
            paths = new List<Path>();
            orderedPaths = new List<List<double>>();
            first = new List<double>();
            last = new List<double>();
            for (int i = 0; i < pathsBar.Value; i++)
            {
                paths.Add(new Path(var, rvBar.Value));
                means.Add(paths[i].Mean.CurrentAvg);
                List<double> orderedPath = new List<double>(paths[i].PathVals);
                orderedPath.Sort();
                orderedPaths.Add(orderedPath);
            }
            foreach (List<double> l in orderedPaths)
            {
                first.Add(l[0]);
                last.Add(l[l.Count - 1]);
            }

            Draw();
        }

        private void Draw()
        {
            g.Clear(BackColor);
            means.Sort();
            HistogramPlot histGeneral = new HistogramPlot(means[0], means[0], means[means.Count - 1], means.Count, Color.Orange, EnumOrientation.BOTTOMUP);
            for (int i = 0; i < means.Count - 1; i++)
            {
                histGeneral.AddNumber(means[i]);
            }

            double start = double.MaxValue;
            double stop = 0;
            foreach (double e in first)
            {
                if (e < start) start = e;
                if (e > stop) stop = e;
            }
            HistogramPlot histFirst = new HistogramPlot(start, start, stop, first.Count, Color.Black, EnumOrientation.BOTTOMUP);
            for (int i = 0; i < first.Count - 1; i++)
            {
                histFirst.AddNumber(first[i]);
            }

            start = double.MaxValue;
            stop = 0;
            foreach (double e in last)
            {
                if (e < start) start = e;
                if (e > stop) stop = e;
            }
            HistogramPlot histLast = new HistogramPlot(start, start, stop, last.Count, Color.Green, EnumOrientation.BOTTOMUP, false, true);
            for (int i = 0; i < last.Count - 1; i++)
            {
                histLast.AddNumber(last[i]);
            }

            vpGeneral = new ViewPort(g, new RectangleF(0, 0, pictureBox.Width, pictureBox.Height), means[0], means[means.Count - 1], 0, histGeneral.GetMaxFreq());
            histGeneral.DrawPlot(vpGeneral);
            first.Sort();
            vpFirst = new ViewPort(g, new RectangleF(0, 0, pictureBox.Width, pictureBox.Height), first[0], first[first.Count - 1], 0, histFirst.GetMaxFreq());
            histFirst.DrawPlot(vpFirst);
            last.Sort();
            vpLast = new ViewPort(g, new RectangleF(0, 0, pictureBox.Width, pictureBox.Height), last[0], last[last.Count - 1], 0, histLast.GetMaxFreq());
            histLast.DrawPlot(vpLast);
            pictureBox.Refresh();
        }

        private void pathsBar_MouseUp(object sender, MouseEventArgs e)
        {
            curPathsNumLab.Text = pathsBar.Value.ToString();
        }

        private void rvBar_MouseUp(object sender, MouseEventArgs e)
        {
            curRvNumLab.Text = rvBar.Value.ToString();
        }
    }
}
