﻿Imports System.Drawing
Imports System.Windows.Forms

Namespace Statistics___Homework_1
	Partial Public Class FormBall
		Inherits Form

		Private ball As ColoredBall
		Public Sub New()
			InitializeComponent()
			Me.SetStyle(ControlStyles.OptimizedDoubleBuffer Or ControlStyles.AllPaintingInWmPaint Or ControlStyles.UserPaint, True)
			Me.UpdateStyles()
		End Sub

		Private Sub FormBall_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
			ball = New ColoredBall(100, 100, 0, 0, 4, 4, Brushes.Orange, Pens.Black)
			pictureBoxBall.Image = New Bitmap(pictureBoxBall.Width, pictureBoxBall.Height)
		End Sub

		Private Sub ButtonPress(ByVal sender As Object, ByVal e As EventArgs) Handles button1.Click
			If Not Me.button1.IsHandleCreated Then Return

			textBox.Text = "Welcome in Statistics - Homework 1!"
		End Sub

		Private Sub MoveBall(ByVal sender As Object, ByVal e As EventArgs) Handles timer1.Tick
			' https://www.youtube.com/watch?v=54GnbNEvLTk
			ball.BallPosX += ball.MoveStepX
			If ball.BallPosX < 0 OrElse ball.BallPosX + ball.BallWidth > pictureBoxBall.Width Then
				ball.MoveStepX = -ball.MoveStepX
			End If
			ball.BallPosY += ball.MoveStepY
			If ball.BallPosY < 0 OrElse ball.BallPosY + ball.BallHeight > pictureBoxBall.Height Then
				ball.MoveStepY = -ball.MoveStepY
			End If

			Dim g = Graphics.FromImage(pictureBoxBall.Image)
			g.Clear(Me.BackColor)
			g.FillEllipse(ball.BallColor, ball.BallPosX, ball.BallPosY, ball.BallWidth, ball.BallHeight)
			g.DrawEllipse(ball.BallCircumferenceColor, ball.BallPosX, ball.BallPosY, ball.BallWidth, ball.BallHeight)
			pictureBoxBall.Refresh()
		End Sub

		Private Sub bounceButton_Click(ByVal sender As Object, ByVal e As EventArgs) Handles bounceButton.Click
			If Not Me.bounceButton.IsHandleCreated Then Return

			' https://stackoverflow.com/questions/27766600/how-to-call-paint-event-from-a-button-click-event
			Dim g = Graphics.FromImage(pictureBoxBall.Image)
			g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias
			g.Clear(Me.BackColor)
			g.FillEllipse(ball.BallColor, ball.BallPosX, ball.BallPosY, ball.BallWidth, ball.BallHeight)
			g.DrawEllipse(ball.BallCircumferenceColor, ball.BallPosX, ball.BallPosY, ball.BallWidth, ball.BallHeight)

			timer1.Enabled = True
			pictureBoxBall.Refresh()
		End Sub
	End Class
End Namespace
