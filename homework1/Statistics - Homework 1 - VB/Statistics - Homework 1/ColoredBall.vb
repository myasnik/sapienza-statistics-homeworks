﻿Imports System
Imports System.Collections.Generic
Imports System.Text
Imports System.Drawing

Namespace Statistics___Homework_1
	Friend Class ColoredBall
		Private ballWidthAttr As Integer = 300
		Private ballHeightAttr As Integer = 300
		Private ballPosXAttr As Integer = 0
		Private ballPosYAttr As Integer = 0
		Private moveStepXAttr As Integer = 4
		Private moveStepYAttr As Integer = 4
		Private ballColorAttr As Brush = Brushes.Red
		Private ballCircumferenceColorAttr As Pen = Pens.Black

		Public Sub New(ByVal ballWidth As Integer, ByVal ballHeight As Integer, ByVal ballPosX As Integer, ByVal ballPosY As Integer, ByVal moveStepX As Integer, ByVal moveStepY As Integer, ByVal ballColor As Brush, ByVal ballCircumferenceColor As Pen)
			Me.ballWidthAttr = ballWidth
			Me.ballHeightAttr = ballHeight
			Me.ballPosXAttr = ballPosX
			Me.ballPosYAttr = ballPosY
			Me.moveStepXAttr = moveStepX
			Me.moveStepYAttr = moveStepY
			Me.ballColorAttr = ballColor
			Me.ballCircumferenceColorAttr = ballCircumferenceColor
		End Sub

		Public Property BallWidth() As Integer
			Get
				Return ballWidthAttr
			End Get
			Set(ByVal value As Integer)
				ballWidthAttr = value
			End Set
		End Property
		Public Property BallHeight() As Integer
			Get
				Return ballHeightAttr
			End Get
			Set(ByVal value As Integer)
				ballHeightAttr = value
			End Set
		End Property
		Public Property BallPosX() As Integer
			Get
				Return ballPosXAttr
			End Get
			Set(ByVal value As Integer)
				ballPosXAttr = value
			End Set
		End Property
		Public Property BallPosY() As Integer
			Get
				Return ballPosYAttr
			End Get
			Set(ByVal value As Integer)
				ballPosYAttr = value
			End Set
		End Property
		Public Property MoveStepX() As Integer
			Get
				Return moveStepXAttr
			End Get
			Set(ByVal value As Integer)
				moveStepXAttr = value
			End Set
		End Property
		Public Property MoveStepY() As Integer
			Get
				Return moveStepYAttr
			End Get
			Set(ByVal value As Integer)
				moveStepYAttr = value
			End Set
		End Property
		Public Property BallColor() As Brush
			Get
				Return ballColorAttr
			End Get
			Set(ByVal value As Brush)
				ballColorAttr = value
			End Set
		End Property
		Public Property BallCircumferenceColor() As Pen
			Get
				Return ballCircumferenceColorAttr
			End Get
			Set(ByVal value As Pen)
				ballCircumferenceColorAttr = value
			End Set
		End Property
	End Class
End Namespace
