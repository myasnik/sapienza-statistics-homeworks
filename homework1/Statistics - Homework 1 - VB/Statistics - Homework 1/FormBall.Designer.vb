﻿Imports System.Windows.Forms

Namespace Statistics___Homework_1
	Partial Public Class FormBall
		Inherits Form

		''' <summary>
		''' Required designer variable.
		''' </summary>
		Private components As System.ComponentModel.IContainer = Nothing

		''' <summary>
		''' Clean up any resources being used.
		''' </summary>
		''' <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		Protected Overrides Sub Dispose(ByVal disposing As Boolean)
			If disposing AndAlso (components IsNot Nothing) Then
				components.Dispose()
			End If
			MyBase.Dispose(disposing)
		End Sub

#Region "Windows Form Designer generated code"

		''' <summary>
		''' Required method for Designer support - do not modify
		''' the contents of this method with the code editor.
		''' </summary>
		Private Sub InitializeComponent()
            Me.components = New System.ComponentModel.Container()
            Me.button1 = New System.Windows.Forms.Button()
            Me.textBox = New System.Windows.Forms.RichTextBox()
            Me.bounceButton = New System.Windows.Forms.Button()
            Me.pictureBoxBall = New System.Windows.Forms.PictureBox()
            Me.timer1 = New System.Windows.Forms.Timer(Me.components)
            CType(Me.pictureBoxBall, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'button1
            '
            Me.button1.Location = New System.Drawing.Point(13, 22)
            Me.button1.Name = "button1"
            Me.button1.Size = New System.Drawing.Size(75, 23)
            Me.button1.TabIndex = 1
            Me.button1.Text = "Press Me!"
            Me.button1.UseVisualStyleBackColor = True
            '
            'textBox
            '
            Me.textBox.Location = New System.Drawing.Point(94, 22)
            Me.textBox.Name = "textBox"
            Me.textBox.Size = New System.Drawing.Size(694, 23)
            Me.textBox.TabIndex = 2
            Me.textBox.Text = ""
            '
            'bounceButton
            '
            Me.bounceButton.Location = New System.Drawing.Point(12, 70)
            Me.bounceButton.Name = "bounceButton"
            Me.bounceButton.Size = New System.Drawing.Size(75, 368)
            Me.bounceButton.TabIndex = 3
            Me.bounceButton.Text = "Bounce!"
            Me.bounceButton.UseVisualStyleBackColor = True
            '
            'pictureBoxBall
            '
            Me.pictureBoxBall.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
            Me.pictureBoxBall.Location = New System.Drawing.Point(94, 70)
            Me.pictureBoxBall.Name = "pictureBoxBall"
            Me.pictureBoxBall.Size = New System.Drawing.Size(694, 368)
            Me.pictureBoxBall.TabIndex = 4
            Me.pictureBoxBall.TabStop = False
            '
            'timer1
            '
            Me.timer1.Interval = 10
            '
            'FormBall
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.ClientSize = New System.Drawing.Size(800, 450)
            Me.Controls.Add(Me.pictureBoxBall)
            Me.Controls.Add(Me.bounceButton)
            Me.Controls.Add(Me.textBox)
            Me.Controls.Add(Me.button1)
            Me.Name = "FormBall"
            Me.Text = "FormBall"
            CType(Me.pictureBoxBall, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)

        End Sub

#End Region
        Private WithEvents button1 As System.Windows.Forms.Button
		Private textBox As System.Windows.Forms.RichTextBox
		Private WithEvents bounceButton As System.Windows.Forms.Button
		Private pictureBoxBall As System.Windows.Forms.PictureBox
		Private WithEvents timer1 As System.Windows.Forms.Timer
	End Class
End Namespace