﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Statistics___Homework_1
{
    public partial class FormBall : Form
    {
        private ColoredBall ball;
        public FormBall()
        {
            InitializeComponent();
            this.SetStyle(ControlStyles.OptimizedDoubleBuffer | ControlStyles.AllPaintingInWmPaint | ControlStyles.UserPaint, true);
            this.UpdateStyles();
        }

        private void FormBall_Load(object sender, EventArgs e)
        {
            ball = new ColoredBall(100, 100, 0, 0, 4, 4, Brushes.Orange, Pens.Black);
            pictureBoxBall.Image = new Bitmap(pictureBoxBall.Width, pictureBoxBall.Height);
        }

        private void ButtonPress(object sender, EventArgs e)
        {
            textBox.Text = "Welcome in Statistics - Homework 1!";
        }

        private void MoveBall(object sender, EventArgs e)
        {
            // https://www.youtube.com/watch?v=54GnbNEvLTk
            ball.BallPosX += ball.MoveStepX;
            if (ball.BallPosX < 0 || ball.BallPosX + ball.BallWidth > pictureBoxBall.Width)
            {
                ball.MoveStepX = -ball.MoveStepX;
            }
            ball.BallPosY += ball.MoveStepY;
            if (ball.BallPosY < 0 || ball.BallPosY + ball.BallHeight > pictureBoxBall.Height)
            {
                ball.MoveStepY = -ball.MoveStepY;
            }

            var g = Graphics.FromImage(pictureBoxBall.Image);
            g.Clear(this.BackColor);
            g.FillEllipse(ball.BallColor, ball.BallPosX, ball.BallPosY, ball.BallWidth, ball.BallHeight);
            g.DrawEllipse(ball.BallCircumferenceColor, ball.BallPosX, ball.BallPosY, ball.BallWidth, ball.BallHeight);
            pictureBoxBall.Refresh();
        }

        private void bounceButton_Click(object sender, EventArgs e)
        {
            // https://stackoverflow.com/questions/27766600/how-to-call-paint-event-from-a-button-click-event
            var g = Graphics.FromImage(pictureBoxBall.Image);
            g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
            g.Clear(this.BackColor);
            g.FillEllipse(ball.BallColor, ball.BallPosX, ball.BallPosY, ball.BallWidth, ball.BallHeight);
            g.DrawEllipse(ball.BallCircumferenceColor, ball.BallPosX, ball.BallPosY, ball.BallWidth, ball.BallHeight);

            timer1.Enabled = true;
            pictureBoxBall.Refresh();
        }
    }
}
