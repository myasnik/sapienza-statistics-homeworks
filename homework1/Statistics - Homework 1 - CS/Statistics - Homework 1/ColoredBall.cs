﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

namespace Statistics___Homework_1
{
    class ColoredBall
    {
        private int ballWidth = 300;
        private int ballHeight = 300;
        private int ballPosX = 0;
        private int ballPosY = 0;
        private int moveStepX = 4;
        private int moveStepY = 4;
        private Brush ballColor = Brushes.Red;
        private Pen ballCircumferenceColor = Pens.Black;

        public ColoredBall(int ballWidth, int ballHeight, int ballPosX, int ballPosY, int moveStepX, int moveStepY, Brush ballColor, Pen ballCircumferenceColor)
        {
            this.ballWidth = ballWidth;
            this.ballHeight = ballHeight;
            this.ballPosX = ballPosX;
            this.ballPosY = ballPosY;
            this.moveStepX = moveStepX;
            this.moveStepY = moveStepY;
            this.ballColor = ballColor;
            this.ballCircumferenceColor = ballCircumferenceColor;
        }

        public int BallWidth { get => ballWidth; set => ballWidth = value; }
        public int BallHeight { get => ballHeight; set => ballHeight = value; }
        public int BallPosX { get => ballPosX; set => ballPosX = value; }
        public int BallPosY { get => ballPosY; set => ballPosY = value; }
        public int MoveStepX { get => moveStepX; set => moveStepX = value; }
        public int MoveStepY { get => moveStepY; set => moveStepY = value; }
        public Brush BallColor { get => ballColor; set => ballColor = value; }
        public Pen BallCircumferenceColor { get => ballCircumferenceColor; set => ballCircumferenceColor = value; }
    }
}
