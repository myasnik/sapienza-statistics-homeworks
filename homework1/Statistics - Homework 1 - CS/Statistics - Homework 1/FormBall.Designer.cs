﻿
namespace Statistics___Homework_1
{
    partial class FormBall
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.button1 = new System.Windows.Forms.Button();
            this.textBox = new System.Windows.Forms.RichTextBox();
            this.bounceButton = new System.Windows.Forms.Button();
            this.pictureBoxBall = new System.Windows.Forms.PictureBox();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxBall)).BeginInit();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(13, 22);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 1;
            this.button1.Text = "Press Me!";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.ButtonPress);
            // 
            // textBox
            // 
            this.textBox.Location = new System.Drawing.Point(94, 22);
            this.textBox.Name = "textBox";
            this.textBox.Size = new System.Drawing.Size(694, 23);
            this.textBox.TabIndex = 2;
            this.textBox.Text = "";
            // 
            // bounceButton
            // 
            this.bounceButton.Location = new System.Drawing.Point(12, 70);
            this.bounceButton.Name = "bounceButton";
            this.bounceButton.Size = new System.Drawing.Size(75, 368);
            this.bounceButton.TabIndex = 3;
            this.bounceButton.Text = "Bounce!";
            this.bounceButton.UseVisualStyleBackColor = true;
            this.bounceButton.Click += new System.EventHandler(this.bounceButton_Click);
            // 
            // pictureBoxBall
            // 
            this.pictureBoxBall.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBoxBall.Location = new System.Drawing.Point(94, 70);
            this.pictureBoxBall.Name = "pictureBoxBall";
            this.pictureBoxBall.Size = new System.Drawing.Size(694, 368);
            this.pictureBoxBall.TabIndex = 4;
            this.pictureBoxBall.TabStop = false;
            // 
            // timer1
            // 
            this.timer1.Interval = 10;
            this.timer1.Tick += new System.EventHandler(this.MoveBall);
            // 
            // FormBall
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.pictureBoxBall);
            this.Controls.Add(this.bounceButton);
            this.Controls.Add(this.textBox);
            this.Controls.Add(this.button1);
            this.Name = "FormBall";
            this.Text = "FormBall";
            this.Load += new System.EventHandler(this.FormBall_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxBall)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.RichTextBox textBox;
        private System.Windows.Forms.Button bounceButton;
        private System.Windows.Forms.PictureBox pictureBoxBall;
        private System.Windows.Forms.Timer timer1;
    }
}