﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;

namespace Statistics___Homework_3A
{
    class CSVParser
    {
        private string path;
        private Type classType;
        private List<object> data = new List<object>();

        public CSVParser(string path, Type classType, bool hasHeader)
        {
            Path = path;
            ClassType = classType;

            StreamReader reader = new StreamReader(File.OpenRead(path));
            if (hasHeader) reader.ReadLine();
            while (!reader.EndOfStream)
            {
                string line = reader.ReadLine();
                if (!String.IsNullOrWhiteSpace(line))
                {
                    line = string.Join("", line.Split('"', ' ', '\\', '\"'));
                    string[] values = line.Split(',');
                    object newValue = Activator.CreateInstance(classType);

                    int i = 0;
                    foreach (FieldInfo field in classType.GetFields())
                    {
                        field.SetValue(newValue, Convert.ChangeType(values[i], field.FieldType));
                        i++;
                    }

                    data.Add(newValue);
                }
            }
        }

        public string Path { get => path; private set => path = value; }
        public Type ClassType { get => classType; private set => classType = value; }
        public List<object> Data { get => data; private set => data = value; }
    }
}
