﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Statistics___Homework_3A
{
    class Program
    {
        static void Main(string[] args)
        {
            // Parse CSV
            CSVParser treeCSV = new CSVParser("C:\\Users\\student\\Downloads\\trees.csv", typeof(Tree), true);

            // Mean
            OnlineMean mean = new OnlineMean();
            foreach (Tree tree in treeCSV.Data)
            {
                mean.AddValue(tree.height);
            }
            Console.WriteLine("Tree height mean: " + mean.CurrentAvg);

            // Standard deviation
            List<int> treesHeights = new List<int>();
            foreach (Tree e in treeCSV.Data)
            {
                treesHeights.Add(e.height);
            }
            StandardDeviation deviation = new StandardDeviation(treesHeights);
            Console.WriteLine("Tree height standard deviation: " + deviation.StdDeviation);

            // Discrete distribution
            Console.WriteLine("Discrete ditribution of trees heights: ");
            DiscreteDistribution heightDistribution = new DiscreteDistribution();
            foreach (Tree e in treeCSV.Data)
            {
                heightDistribution.AddValue(e.height);
            }
            foreach (KeyValuePair<object, int> e in heightDistribution.Distribution)
            {
                Console.WriteLine(e.Key + ": " + e.Value);
            }

            // Discrete distribution of bivariate
            Console.WriteLine("Discrete ditribution of trees heights and volumes: ");
            DiscreteDistribution biDistribution = new DiscreteDistribution();
            foreach (Tree e in treeCSV.Data)
            {
                biDistribution.AddValue(new Tuple<int, double>(e.height, e.volume));
            }
            foreach (KeyValuePair<object, int> e in biDistribution.Distribution)
            {
                Console.WriteLine(e.Key + ": " + e.Value);
            }

        }
    }
}
