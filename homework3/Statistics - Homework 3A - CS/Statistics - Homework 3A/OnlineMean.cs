﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Statistics___Homework_3A
{
    class OnlineMean
    {
        private double currentAvg = 0;
        private int index = 0;

        public double CurrentAvg { get => currentAvg; }
        public int Index { get => index; }

        public void AddValue(double val)
        {
            index++;
            currentAvg = CurrentAvg + ((val - CurrentAvg) / Index);
        }
    }
}
