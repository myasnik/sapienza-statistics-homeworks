﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace Statistics___Homework_3B
{
    public partial class WordCloudForm : Form
    {
        public WordCloudForm()
        {
            InitializeComponent();
        }

        private void WordCloudForm_Load(object sender, EventArgs e)
        {
            pictureBox.Image = new Bitmap(pictureBox.Width, pictureBox.Height);
        }

        private void buttonClick(object sender, MouseEventArgs e)
        {
            DiscreteDistribution distribution = CalculateDistribution(textBox.Text);
            PlotWordCloud(distribution);
        }

        private DiscreteDistribution CalculateDistribution(string text)
        {
            DiscreteDistribution distribution = new DiscreteDistribution();
            List<string> stopWords = File.ReadAllLines("StopWords.txt").ToList();

            foreach (string element in text.Split('!', '.', '"', ' ', '\\', '\"', ':', ';', ',', '=', '{', '}', '[', ']', '^', '?',
                '!', '\'', '(', ')', '/', '&', '%', '$', '£', '"', '|', '+', '*', '§', '@', '#', '_', '-', '<', '>'))
            {
                string elementUpper = element.ToUpper();
                if (!String.IsNullOrWhiteSpace(element) && !stopWords.Contains(elementUpper))
                {
                    distribution.AddValue(elementUpper);
                }
            }
            return distribution;
        }

        private void PlotWordCloud(DiscreteDistribution distribution)
        {
            var g = Graphics.FromImage(pictureBox.Image);
            List<Rectangle> rectangles = new List<Rectangle>();
            Random rand = new Random();

            g.Clear(Color.White);

            foreach (KeyValuePair<object, int> kvp in distribution.Distribution)
            {
                Rectangle tryRect;
                Font f = new Font("arial", kvp.Value * 3);
                Size s = Size.Truncate(g.MeasureString(kvp.Key.ToString(), f));
                bool allGood = false;

                if (kvp.Value < 3 || kvp.Key.ToString().Length > 10) continue;

                int tries = 0;
                int maxTries = 1000;
                do
                {

                    int x = rand.Next((int)g.VisibleClipBounds.Left, (int)g.VisibleClipBounds.Right - s.Width);
                    int y = rand.Next((int)g.VisibleClipBounds.Top, (int)g.VisibleClipBounds.Bottom - s.Height);

                    tryRect = new Rectangle(new Point(x, y), s);
                    if (!SpotOccupied(tryRect, rectangles))
                    {
                        allGood = true;
                        break;
                    }
                    tries++;
                }
                while (tries < maxTries);

                if (allGood)
                {
                    rectangles.Add(tryRect);
                    g.DrawString(kvp.Key.ToString(), f, new SolidBrush(Color.FromArgb(rand.Next(256), rand.Next(256), rand.Next(256))), new Point(tryRect.X, tryRect.Y));
                }
            }

            pictureBox.Refresh();
        }

        private bool SpotOccupied(Rectangle target, List<Rectangle> rectList)
        {
            foreach (Rectangle r in rectList)
            {
                if (r.IntersectsWith(target) || r.Contains(target) || target.Contains(r)) return true;
            }
            return false;
        }

    }
}
