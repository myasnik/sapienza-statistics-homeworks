﻿
namespace Statistics___Homework_3B
{
    partial class WordCloudForm
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBox = new System.Windows.Forms.RichTextBox();
            this.generateButton = new System.Windows.Forms.Button();
            this.pictureBox = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // textBox
            // 
            this.textBox.Location = new System.Drawing.Point(13, 40);
            this.textBox.Name = "textBox";
            this.textBox.Size = new System.Drawing.Size(327, 398);
            this.textBox.TabIndex = 0;
            this.textBox.Text = "";
            // 
            // generateButton
            // 
            this.generateButton.Location = new System.Drawing.Point(13, 11);
            this.generateButton.Name = "generateButton";
            this.generateButton.Size = new System.Drawing.Size(327, 23);
            this.generateButton.TabIndex = 1;
            this.generateButton.Text = "Generate";
            this.generateButton.UseVisualStyleBackColor = true;
            this.generateButton.MouseClick += new System.Windows.Forms.MouseEventHandler(this.buttonClick);
            // 
            // pictureBox
            // 
            this.pictureBox.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBox.Location = new System.Drawing.Point(347, 11);
            this.pictureBox.Name = "pictureBox";
            this.pictureBox.Size = new System.Drawing.Size(441, 427);
            this.pictureBox.TabIndex = 2;
            this.pictureBox.TabStop = false;
            // 
            // WordCloudForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.pictureBox);
            this.Controls.Add(this.generateButton);
            this.Controls.Add(this.textBox);
            this.Name = "WordCloudForm";
            this.Text = "WordCloudForm";
            this.Load += new System.EventHandler(this.WordCloudForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.RichTextBox textBox;
        private System.Windows.Forms.Button generateButton;
        private System.Windows.Forms.PictureBox pictureBox;
    }
}

