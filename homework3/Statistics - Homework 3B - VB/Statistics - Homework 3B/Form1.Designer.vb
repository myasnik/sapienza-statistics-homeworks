﻿Imports System.Windows.Forms

Namespace Statistics___Homework_3B
	Partial Public Class WordCloudForm
		Inherits Form

		''' <summary>
		'''  Required designer variable.
		''' </summary>
		Private components As System.ComponentModel.IContainer = Nothing

		''' <summary>
		'''  Clean up any resources being used.
		''' </summary>
		''' <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		Protected Overrides Sub Dispose(ByVal disposing As Boolean)
			If disposing AndAlso (components IsNot Nothing) Then
				components.Dispose()
			End If
			MyBase.Dispose(disposing)
		End Sub

#Region "Windows Form Designer generated code"

		''' <summary>
		'''  Required method for Designer support - do not modify
		'''  the contents of this method with the code editor.
		''' </summary>
		Private Sub InitializeComponent()
			Me.textBox = New System.Windows.Forms.RichTextBox()
			Me.generateButton = New System.Windows.Forms.Button()
			Me.pictureBox = New System.Windows.Forms.PictureBox()
			CType(Me.pictureBox, System.ComponentModel.ISupportInitialize).BeginInit()
			Me.SuspendLayout()
			' 
			' textBox
			' 
			Me.textBox.Location = New System.Drawing.Point(13, 40)
			Me.textBox.Name = "textBox"
			Me.textBox.Size = New System.Drawing.Size(327, 398)
			Me.textBox.TabIndex = 0
			Me.textBox.Text = ""
			' 
			' generateButton
			' 
			Me.generateButton.Location = New System.Drawing.Point(13, 11)
			Me.generateButton.Name = "generateButton"
			Me.generateButton.Size = New System.Drawing.Size(327, 23)
			Me.generateButton.TabIndex = 1
			Me.generateButton.Text = "Generate"
			Me.generateButton.UseVisualStyleBackColor = True
			'INSTANT VB NOTE: The following InitializeComponent event wireup was converted to a 'Handles' clause:
			'ORIGINAL LINE: this.generateButton.MouseClick += new System.Windows.Forms.MouseEventHandler(this.buttonClick);
			' 
			' pictureBox
			' 
			Me.pictureBox.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
			Me.pictureBox.Location = New System.Drawing.Point(347, 11)
			Me.pictureBox.Name = "pictureBox"
			Me.pictureBox.Size = New System.Drawing.Size(441, 427)
			Me.pictureBox.TabIndex = 2
			Me.pictureBox.TabStop = False
			' 
			' WordCloudForm
			' 
			Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0F, 15.0F)
			Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
			Me.ClientSize = New System.Drawing.Size(800, 450)
			Me.Controls.Add(Me.pictureBox)
			Me.Controls.Add(Me.generateButton)
			Me.Controls.Add(Me.textBox)
			Me.Name = "WordCloudForm"
			Me.Text = "WordCloudForm"
			'INSTANT VB NOTE: The following InitializeComponent event wireup was converted to a 'Handles' clause:
			'ORIGINAL LINE: this.Load += new System.EventHandler(this.WordCloudForm_Load);
			CType(Me.pictureBox, System.ComponentModel.ISupportInitialize).EndInit()
			Me.ResumeLayout(False)

		End Sub

#End Region

		Private textBox As System.Windows.Forms.RichTextBox
		Private WithEvents generateButton As System.Windows.Forms.Button
		Private pictureBox As System.Windows.Forms.PictureBox
	End Class
End Namespace

