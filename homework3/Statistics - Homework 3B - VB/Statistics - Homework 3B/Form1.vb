﻿Imports System
Imports System.Collections.Generic
Imports System.Drawing
Imports System.IO
Imports System.Linq
Imports System.Windows.Forms

Namespace Statistics___Homework_3B
	Partial Public Class WordCloudForm
		Inherits Form

		Public Sub New()
			InitializeComponent()
		End Sub

		Private Sub WordCloudForm_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
			pictureBox.Image = New Bitmap(pictureBox.Width, pictureBox.Height)
		End Sub

		Private Sub buttonClick(ByVal sender As Object, ByVal e As MouseEventArgs) Handles generateButton.MouseClick
			If Not Me.generateButton.IsHandleCreated Then Return

			Dim distribution As DiscreteDistribution = CalculateDistribution(textBox.Text)
			PlotWordCloud(distribution)
		End Sub

		Private Function CalculateDistribution(ByVal text As String) As DiscreteDistribution
			Dim distribution As New DiscreteDistribution()
			Dim stopWords As List(Of String) = File.ReadAllLines("StopWords.txt").ToList()

			For Each element As String In text.Split("!"c, "."c, """"c, " "c, "\"c, """"c, ":"c, ";"c, ","c, "="c, "{"c, "}"c, "["c, "]"c, "^"c, "?"c, "!"c, "'"c, "("c, ")"c, "/"c, "&"c, "%"c, "$"c, "£"c, """"c, "|"c, "+"c, "*"c, "§"c, "@"c, "#"c, "_"c, "-"c, "<"c, ">"c)
				Dim elementUpper As String = element.ToUpper()
				If Not String.IsNullOrWhiteSpace(element) AndAlso Not stopWords.Contains(elementUpper) Then
					distribution.AddValue(elementUpper)
				End If
			Next element
			Return distribution
		End Function

		Private Sub PlotWordCloud(ByVal distribution As DiscreteDistribution)
			Dim g = Graphics.FromImage(pictureBox.Image)
			Dim rectangles As New List(Of Rectangle)()
			Dim rand As New Random()

			g.Clear(Color.White)

			For Each kvp As KeyValuePair(Of Object, Integer) In distribution.Distribution
				Dim tryRect As Rectangle
				Dim f As New Font("arial", kvp.Value * 3)
				Dim s As Size = Size.Truncate(g.MeasureString(kvp.Key.ToString(), f))
				Dim allGood As Boolean = False

				If kvp.Value < 3 OrElse kvp.Key.ToString().Length > 10 Then
					Continue For
				End If

				Dim tries As Integer = 0
				Dim maxTries As Integer = 1000
				Do

					Dim x As Integer = rand.Next(CInt(Math.Truncate(g.VisibleClipBounds.Left)), CInt(Math.Truncate(g.VisibleClipBounds.Right)) - s.Width)
					Dim y As Integer = rand.Next(CInt(Math.Truncate(g.VisibleClipBounds.Top)), CInt(Math.Truncate(g.VisibleClipBounds.Bottom)) - s.Height)

					tryRect = New Rectangle(New Point(x, y), s)
					If Not SpotOccupied(tryRect, rectangles) Then
						allGood = True
						Exit Do
					End If
					tries += 1
				Loop While tries < maxTries

				If allGood Then
					rectangles.Add(tryRect)
					g.DrawString(kvp.Key.ToString(), f, New SolidBrush(Color.FromArgb(rand.Next(256), rand.Next(256), rand.Next(256))), New Point(tryRect.X, tryRect.Y))
				End If
			Next kvp

			pictureBox.Refresh()
		End Sub

		Private Function SpotOccupied(ByVal target As Rectangle, ByVal rectList As List(Of Rectangle)) As Boolean
			For Each r As Rectangle In rectList
				If r.IntersectsWith(target) OrElse r.Contains(target) OrElse target.Contains(r) Then
					Return True
				End If
			Next r
			Return False
		End Function

	End Class
End Namespace
