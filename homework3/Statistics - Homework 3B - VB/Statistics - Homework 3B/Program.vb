﻿Imports System
Imports System.Collections.Generic
Imports System.Linq
Imports System.Threading.Tasks
Imports System.Windows.Forms

Namespace Statistics___Homework_3B
	Friend Module Program
		''' <summary>
		'''  The main entry point for the application.
		''' </summary>
		<STAThread>
		Sub Main()
			Application.SetHighDpiMode(HighDpiMode.SystemAware)
			Application.EnableVisualStyles()
			Application.SetCompatibleTextRenderingDefault(False)
			Application.Run(New WordCloudForm())
		End Sub
	End Module
End Namespace
