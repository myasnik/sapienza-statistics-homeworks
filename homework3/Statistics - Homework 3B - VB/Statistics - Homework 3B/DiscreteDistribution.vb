﻿Imports System
Imports System.Collections.Generic

Namespace Statistics___Homework_3B
	Friend Class DiscreteDistribution
'INSTANT VB NOTE: The field distribution was renamed since Visual Basic does not allow fields to have the same name as other class members:
		Private distribution_Conflict As Dictionary(Of Object, Integer)
'INSTANT VB NOTE: The field totOccurrences was renamed since Visual Basic does not allow fields to have the same name as other class members:
		Private totOccurrences_Conflict As Integer

		Public Property Distribution() As Dictionary(Of Object, Integer)
			Get
				Return distribution_Conflict
			End Get
			Private Set(ByVal value As Dictionary(Of Object, Integer))
				distribution_Conflict = value
			End Set
		End Property
		Public Property TotOccurrences() As Integer
			Get
				Return totOccurrences_Conflict
			End Get
			Private Set(ByVal value As Integer)
				totOccurrences_Conflict = value
			End Set
		End Property

		Public Sub New()
			Distribution = New Dictionary(Of Object, Integer)()
			TotOccurrences = 0
		End Sub

		Protected Sub CheckValueToSum(ByVal valueToSum As Integer)
			If valueToSum <= 0 Then
				Throw New ArgumentOutOfRangeException("valueToSum must be greater than 0")
			End If
		End Sub

		Public Sub AddValue(ByVal key As Object)
			If Distribution.ContainsKey(key) Then
				Distribution(key) += 1
			Else
				Distribution.Add(key, 1)
			End If
			TotOccurrences += 1
		End Sub

		Public Sub AddValue(ByVal key As Object, ByVal valueToSum As Integer)
			CheckValueToSum(valueToSum)
			If Distribution.ContainsKey(key) Then
				Distribution(key) += valueToSum
			Else
				Distribution.Add(key, valueToSum)
			End If
			TotOccurrences += valueToSum
		End Sub

		Public Function GetKeyFrequency(ByVal key As Object) As Integer
			If Distribution.ContainsKey(key) Then
				Return Distribution(key)
			Else
				Throw New KeyNotFoundException()
			End If
		End Function

		Public Function GetKeyRelativeFrequency(ByVal key As Object) As Double
			If Distribution.ContainsKey(key) Then
				Return CDbl(Distribution(key)) / CDbl(TotOccurrences)
			Else
				Throw New KeyNotFoundException()
			End If
		End Function

		Public Function GetKeyPercentage(ByVal key As Object) As Double
			If Distribution.ContainsKey(key) Then
				Return (CDbl(Distribution(key)) / CDbl(TotOccurrences)) * 100
			Else
				Throw New KeyNotFoundException()
			End If
		End Function
	End Class
End Namespace
