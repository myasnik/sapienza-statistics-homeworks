﻿Imports System
Imports System.Collections.Generic
Imports System.Text

Namespace Statistics___Homework_3A
	Friend Class Tree
		Public index As Integer
		Public girth As Double
		Public height As Integer
		Public volume As Double

		Public Sub New()
			Me.index = 0
			Me.girth = 0
			Me.height = 0
			Me.volume = 0
		End Sub

		Public Sub New(Optional ByVal index As Integer = 0, Optional ByVal girth As Double = 0, Optional ByVal height As Integer = 0, Optional ByVal volume As Double = 0)
			Me.index = index
			Me.girth = girth
			Me.height = height
			Me.volume = volume
		End Sub
	End Class
End Namespace
