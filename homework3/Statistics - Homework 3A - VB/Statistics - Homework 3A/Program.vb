﻿Imports System
Imports System.Collections.Generic
Imports System.Linq

Namespace Statistics___Homework_3A
	Friend Class Program
		Shared Sub Main(ByVal args() As String)
			' Parse CSV
			Dim treeCSV As New CSVParser("C:\Users\student\Downloads\trees.csv", GetType(Tree), True)

			' Mean
			Dim mean As New OnlineMean()
			For Each tree As Tree In treeCSV.Data
				mean.AddValue(tree.height)
			Next tree
			Console.WriteLine("Tree height mean: " & mean.CurrentAvg)

			' Standard deviation
			Dim treesHeights As New List(Of Integer)()
			For Each e As Tree In treeCSV.Data
				treesHeights.Add(e.height)
			Next e
			Dim deviation As New StandardDeviation(treesHeights)
			Console.WriteLine("Tree height standard deviation: " & deviation.StdDeviation)

			' Discrete distribution
			Console.WriteLine("Discrete ditribution of trees heights: ")
			Dim heightDistribution As New DiscreteDistribution()
			For Each e As Tree In treeCSV.Data
				heightDistribution.AddValue(e.height)
			Next e
			For Each e As KeyValuePair(Of Object, Integer) In heightDistribution.Distribution
				Console.WriteLine(e.Key & ": " & e.Value)
			Next e

			' Discrete distribution of bivariate
			Console.WriteLine("Discrete ditribution of trees heights and volumes: ")
			Dim biDistribution As New DiscreteDistribution()
			For Each e As Tree In treeCSV.Data
				biDistribution.AddValue(New Tuple(Of Integer, Double)(e.height, e.volume))
			Next e
			For Each e As KeyValuePair(Of Object, Integer) In biDistribution.Distribution
				Console.WriteLine(e.Key.ToString() & ": " & e.Value)
			Next e

		End Sub
	End Class
End Namespace
