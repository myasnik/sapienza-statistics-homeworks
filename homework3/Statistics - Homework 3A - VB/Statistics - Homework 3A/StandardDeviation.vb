﻿Imports System
Imports System.Collections.Generic
Imports System.Math

Namespace Statistics___Homework_3A
	Friend Class StandardDeviation
'INSTANT VB NOTE: The field data was renamed since Visual Basic does not allow fields to have the same name as other class members:
		Private data_Conflict As List(Of Double)
'INSTANT VB NOTE: The field mean was renamed since Visual Basic does not allow fields to have the same name as other class members:
		Private mean_Conflict As OnlineMean
'INSTANT VB NOTE: The field stdDeviation was renamed since Visual Basic does not allow fields to have the same name as other class members:
		Private stdDeviation_Conflict As Double = 0

		Public Sub New(ByVal data As List(Of Integer))
			Me.Data = data.ConvertAll(Function(x) CDbl(x))
			computeMean()
			computeDeviation()
		End Sub

		Public Sub New(ByVal data As List(Of Double))
			Me.Data = data
			computeMean()
			computeDeviation()
		End Sub

		Public Property StdDeviation() As Double
			Get
				Return stdDeviation_Conflict
			End Get
			Private Set(ByVal value As Double)
				stdDeviation_Conflict = value
			End Set
		End Property
		Public Property Data() As List(Of Double)
			Get
				Return data_Conflict
			End Get
			Private Set(ByVal value As List(Of Double))
				data_Conflict = value
			End Set
		End Property
		Friend Property Mean() As OnlineMean
			Get
				Return mean_Conflict
			End Get
			Private Set(ByVal value As OnlineMean)
				mean_Conflict = value
			End Set
		End Property

		Private Sub computeDeviation()
			For Each e As Double In Data
				stdDeviation_Conflict += (e - Mean.CurrentAvg) ^ 2
			Next e
			stdDeviation_Conflict = Sqrt(stdDeviation_Conflict / Data.Count)
		End Sub

		Private Sub computeMean()
			Mean = New OnlineMean()
			For Each e As Double In Data
				mean_Conflict.AddValue(e)
			Next e
		End Sub
	End Class
End Namespace
