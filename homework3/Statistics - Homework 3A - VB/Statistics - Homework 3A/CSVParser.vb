﻿Imports System
Imports System.Collections.Generic
Imports System.IO
Imports System.Reflection

Namespace Statistics___Homework_3A
	Friend Class CSVParser
'INSTANT VB NOTE: The field path was renamed since Visual Basic does not allow fields to have the same name as other class members:
		Private path_Conflict As String
'INSTANT VB NOTE: The field classType was renamed since Visual Basic does not allow fields to have the same name as other class members:
		Private classType_Conflict As Type
'INSTANT VB NOTE: The field data was renamed since Visual Basic does not allow fields to have the same name as other class members:
		Private data_Conflict As New List(Of Object)()

		Public Sub New(ByVal path As String, ByVal classType As Type, ByVal hasHeader As Boolean)
			Me.Path = path
			Me.ClassType = classType

			Dim reader As New StreamReader(File.OpenRead(path))
			If hasHeader Then
				reader.ReadLine()
			End If
			Do While Not reader.EndOfStream
				Dim line As String = reader.ReadLine()
				If Not String.IsNullOrWhiteSpace(line) Then
					line = String.Join("", line.Split(""""c, " "c, "\"c, """"c))
					Dim values() As String = line.Split(","c)
					Dim newValue As Object = Activator.CreateInstance(classType)

					Dim i As Integer = 0
					For Each field As FieldInfo In classType.GetFields()
						field.SetValue(newValue, Convert.ChangeType(values(i), field.FieldType))
						i += 1
					Next field

					data_Conflict.Add(newValue)
				End If
			Loop
		End Sub

		Public Property Path() As String
			Get
				Return path_Conflict
			End Get
			Private Set(ByVal value As String)
				path_Conflict = value
			End Set
		End Property
		Public Property ClassType() As Type
			Get
				Return classType_Conflict
			End Get
			Private Set(ByVal value As Type)
				classType_Conflict = value
			End Set
		End Property
		Public Property Data() As List(Of Object)
			Get
				Return data_Conflict
			End Get
			Private Set(ByVal value As List(Of Object))
				data_Conflict = value
			End Set
		End Property
	End Class
End Namespace
