﻿Namespace Statistics___Homework_3A
	Friend Class City
		Public latD As Integer
		Public latM As Integer
		Public latS As Integer
		Public ns As String
		Public lonD As Integer
		Public lonM As Integer
		Public lonS As Integer
		Public ew As String
		Public cityName As String
		Public stateName As String

		Public Sub New()
			Me.latD = 0
			Me.latM = 0
			Me.latS = 0
			Me.ns = ""
			Me.lonD = 0
			Me.lonM = 0
			Me.lonS = 0
			Me.ew = ""
			Me.cityName = ""
			Me.stateName = ""
		End Sub

		Public Sub New(Optional ByVal latD As Integer = 0, Optional ByVal latM As Integer = 0, Optional ByVal latS As Integer = 0, Optional ByVal ns As String = Nothing, Optional ByVal lonD As Integer = 0, Optional ByVal lonM As Integer = 0, Optional ByVal lonS As Integer = 0, Optional ByVal ew As String = Nothing, Optional ByVal cityName As String = Nothing, Optional ByVal stateName As String = Nothing)
			Me.latD = latD
			Me.latM = latM
			Me.latS = latS
			Me.ns = ns
			Me.lonD = lonD
			Me.lonM = lonM
			Me.lonS = lonS
			Me.ew = ew
			Me.cityName = cityName
			Me.stateName = stateName
		End Sub
	End Class
End Namespace
