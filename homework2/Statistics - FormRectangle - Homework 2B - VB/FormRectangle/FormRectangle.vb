﻿Imports System
Imports System.Drawing
Imports System.Windows.Forms

Namespace FormRectangle
	Partial Public Class FormRectangle
		Inherits Form

		Private rectangle As Rectangle
		Private rectangleBorder As Pen

		Private firstPointDrag As Point

		Public Sub New()
			InitializeComponent()
			Me.SetStyle(ControlStyles.OptimizedDoubleBuffer Or ControlStyles.AllPaintingInWmPaint Or ControlStyles.UserPaint, True)
			Me.UpdateStyles()
		End Sub

		Private Sub FormRectangle_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
			rectangle = New Rectangle(100, 100, 200, 100)
			rectangleBorder = Pens.Orange
			pictureBoxRectangle.Image = New Bitmap(pictureBoxRectangle.Width, pictureBoxRectangle.Height)
		End Sub

		Private Sub DrawRectangle(ByVal sender As Object, ByVal e As EventArgs) Handles timer1.Tick
			Dim g = Graphics.FromImage(pictureBoxRectangle.Image)
			g.Clear(Me.BackColor)
			g.DrawRectangle(rectangleBorder, rectangle)
			pictureBoxRectangle.Refresh()
		End Sub

		Private Sub PictureBoxRectangle_MouseDown(ByVal sender As Object, ByVal e As MouseEventArgs) Handles pictureBoxRectangle.MouseDown
			If Not Me.pictureBoxRectangle.IsHandleCreated Then Return

			If (e.Button = MouseButtons.Left OrElse e.Button = MouseButtons.Right) AndAlso rectangle.Contains(PointToClient(MousePosition)) Then
				firstPointDrag = MousePosition
			Else
				firstPointDrag = New Point()
			End If
		End Sub

		Private Sub PictureBoxRectangle_MouseMove(ByVal sender As Object, ByVal e As MouseEventArgs) Handles pictureBoxRectangle.MouseMove
			If Not Me.pictureBoxRectangle.IsHandleCreated Then Return

			If Not firstPointDrag.IsEmpty Then
				Dim tempPoint As Point = MousePosition
				Dim resPoint As New Point(firstPointDrag.X - tempPoint.X, firstPointDrag.Y - tempPoint.Y)
				If e.Button = MouseButtons.Left Then
					rectangle.Location = New Point(rectangle.Location.X - resPoint.X, rectangle.Location.Y - resPoint.Y)
				ElseIf e.Button = MouseButtons.Right Then
					rectangle.Width += tempPoint.X - firstPointDrag.X
					rectangle.Height += tempPoint.Y - firstPointDrag.Y
				End If
				firstPointDrag = tempPoint
			End If
		End Sub
	End Class
End Namespace
