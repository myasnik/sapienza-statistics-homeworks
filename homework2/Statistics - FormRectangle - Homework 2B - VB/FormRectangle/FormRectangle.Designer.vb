﻿Imports System.Windows.Forms

Namespace FormRectangle
	Partial Public Class FormRectangle
		Inherits Form

		''' <summary>
		'''  Required designer variable.
		''' </summary>
		Private components As System.ComponentModel.IContainer = Nothing

		''' <summary>
		'''  Clean up any resources being used.
		''' </summary>
		''' <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		Protected Overrides Sub Dispose(ByVal disposing As Boolean)
			If disposing AndAlso (components IsNot Nothing) Then
				components.Dispose()
			End If
			MyBase.Dispose(disposing)
		End Sub

#Region "Windows Form Designer generated code"

		''' <summary>
		'''  Required method for Designer support - do not modify
		'''  the contents of this method with the code editor.
		''' </summary>
		Private Sub InitializeComponent()
			Me.components = New System.ComponentModel.Container()
			Me.pictureBoxRectangle = New System.Windows.Forms.PictureBox()
			Me.timer1 = New System.Windows.Forms.Timer(Me.components)
			CType(Me.pictureBoxRectangle, System.ComponentModel.ISupportInitialize).BeginInit()
			Me.SuspendLayout()
			' 
			' pictureBoxRectangle
			' 
			Me.pictureBoxRectangle.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
			Me.pictureBoxRectangle.Location = New System.Drawing.Point(12, 12)
			Me.pictureBoxRectangle.Name = "pictureBoxRectangle"
			Me.pictureBoxRectangle.Size = New System.Drawing.Size(776, 426)
			Me.pictureBoxRectangle.TabIndex = 0
			Me.pictureBoxRectangle.TabStop = False
			'INSTANT VB NOTE: The following InitializeComponent event wireup was converted to a 'Handles' clause:
			'ORIGINAL LINE: this.pictureBoxRectangle.MouseDown += new System.Windows.Forms.MouseEventHandler(this.PictureBoxRectangle_MouseDown);
			'INSTANT VB NOTE: The following InitializeComponent event wireup was converted to a 'Handles' clause:
			'ORIGINAL LINE: this.pictureBoxRectangle.MouseMove += new System.Windows.Forms.MouseEventHandler(this.PictureBoxRectangle_MouseMove);
			' 
			' timer1
			' 
			Me.timer1.Enabled = True
			Me.timer1.Interval = 10
			'INSTANT VB NOTE: The following InitializeComponent event wireup was converted to a 'Handles' clause:
			'ORIGINAL LINE: this.timer1.Tick += new System.EventHandler(this.DrawRectangle);
			' 
			' FormRectangle
			' 
			Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0F, 15.0F)
			Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
			Me.ClientSize = New System.Drawing.Size(800, 450)
			Me.Controls.Add(Me.pictureBoxRectangle)
			Me.Name = "FormRectangle"
			Me.Text = "FormRectangle"
			'INSTANT VB NOTE: The following InitializeComponent event wireup was converted to a 'Handles' clause:
			'ORIGINAL LINE: this.Load += new System.EventHandler(this.FormRectangle_Load);
			CType(Me.pictureBoxRectangle, System.ComponentModel.ISupportInitialize).EndInit()
			Me.ResumeLayout(False)

		End Sub

#End Region

		Private WithEvents pictureBoxRectangle As System.Windows.Forms.PictureBox
		Private WithEvents timer1 As System.Windows.Forms.Timer
	End Class
End Namespace

