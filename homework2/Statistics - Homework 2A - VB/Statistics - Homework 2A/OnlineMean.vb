﻿Imports System
Imports System.Collections.Generic
Imports System.Text

Namespace Statistics___Homework_2A
	Friend Class OnlineMean
'INSTANT VB NOTE: The field currentAvg was renamed since Visual Basic does not allow fields to have the same name as other class members:
		Private currentAvg_Conflict As Double = 0
'INSTANT VB NOTE: The field index was renamed since Visual Basic does not allow fields to have the same name as other class members:
		Private index_Conflict As Integer = 0

		Public ReadOnly Property CurrentAvg() As Double
			Get
				Return currentAvg_Conflict
			End Get
		End Property
		Public ReadOnly Property Index() As Integer
			Get
				Return index_Conflict
			End Get
		End Property

		Public Sub addValue(ByVal val As Integer)
			index_Conflict += 1
			currentAvg_Conflict = CurrentAvg + ((val - CurrentAvg) / Index)
		End Sub
	End Class
End Namespace
