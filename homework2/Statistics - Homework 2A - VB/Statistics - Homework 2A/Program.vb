﻿Imports Microsoft.VisualBasic
Imports System
Imports System.Collections.Generic

Namespace Statistics___Homework_2A
	Friend Class Program
		Private Shared rand As New Random()
		Private Shared Sub OnlineMeanVsNaiveMean()
			Dim mean As New OnlineMean()
			Dim sum As Integer = 0
			Dim meanNaive As Double
			Dim i As Integer = rand.Next(10, 100)

			Console.WriteLine(vbLf & "ONLINE MEAN VS NAIVE MEAN" & vbLf)

			Do While i > 0
				Dim randVal As Integer = rand.Next(1, 10)
				sum = sum + randVal
				Console.WriteLine("Random value in [1-100]: " & randVal)
				mean.addValue(randVal)
				Console.WriteLine("Current online mean: " & mean.CurrentAvg)
				i -= 1
			Loop

			meanNaive = CDbl(sum) / CDbl(mean.Index)

			Console.WriteLine("Naive mean: " & meanNaive)
		End Sub
		Private Shared Sub TestDiscreteDistribution()
			Dim distribution As New DiscreteDistribution()

			For i As Integer = rand.Next(10, 20) To 1 Step -1
				distribution.AddValue(rand.Next(18, 31), rand.Next(1, 5))
			Next i

			Console.WriteLine(vbLf & "TEST DISCRETE DISTRIBUTION" & vbLf)
			Console.WriteLine("Total elements: " & distribution.Distribution.Count)
			Console.WriteLine("Total occurrences: " & distribution.TotOccurrences)
			For Each entry As Object In distribution.Distribution.Keys
				Console.WriteLine(entry & " frequency: " & distribution.GetKeyFrequency(entry))
				Console.WriteLine(entry & " relative frequency: " & distribution.GetKeyRelativeFrequency(entry))
				Console.WriteLine(entry & " percentage: " & distribution.GetKeyPercentage(entry))
			Next entry
		End Sub

		Private Shared Sub TestContinuousDistribution()
			Dim startValue As Double = rand.NextDouble()
			Dim [step] As Double = 0.1
			Dim generatedValues As New List(Of Double)()
			Dim distribution As New ContinuousDistribution(startValue, [step], True)

			Dim i As Integer = 0
			Do While i < rand.Next(10, 20)
				generatedValues.Add(rand.NextDouble())
				distribution.AddValue(generatedValues(i), rand.Next(1, 5))
				i += 1
			Loop

			Console.WriteLine(vbLf & "TEST CONTINUOUS DISTRIBUTION" & vbLf)
			Console.WriteLine("Start value: " & startValue)
			Console.WriteLine("Total elements: " & distribution.Distribution.Count)
			Console.WriteLine("Total occurrences: " & distribution.TotOccurrences)
			For j As Integer = 0 To distribution.Distribution.Count - 1
				Console.WriteLine(distribution.Distribution(j).LowerEnd & " frequency: " & distribution.GetValueFrequency(distribution.Distribution(j).LowerEnd + ([step] / 2)))
				Console.WriteLine(distribution.Distribution(j).LowerEnd & " relative frequency: " & distribution.GetValueRelativeFrequency(distribution.Distribution(j).LowerEnd + ([step] / 2)))
				Console.WriteLine(distribution.Distribution(j).LowerEnd & " percentage: " & distribution.GetValuePercentage(distribution.Distribution(j).LowerEnd + ([step] / 2)))
			Next j
		End Sub

		Shared Sub Main(ByVal args() As String)
			OnlineMeanVsNaiveMean()
			TestDiscreteDistribution()
			TestContinuousDistribution()
		End Sub

	End Class
End Namespace
