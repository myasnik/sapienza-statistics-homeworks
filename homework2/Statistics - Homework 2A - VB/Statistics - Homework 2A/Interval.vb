﻿Imports System

Namespace Statistics___Homework_2A
	Friend Class Interval
'INSTANT VB NOTE: The field lowerEnd was renamed since Visual Basic does not allow fields to have the same name as other class members:
		Private lowerEnd_Conflict As Double
'INSTANT VB NOTE: The field upperEnd was renamed since Visual Basic does not allow fields to have the same name as other class members:
		Private upperEnd_Conflict As Double
'INSTANT VB NOTE: The field rightComprehend was renamed since Visual Basic does not allow fields to have the same name as other class members:
		Private rightComprehend_Conflict As Boolean
'INSTANT VB NOTE: The field leftComprehend was renamed since Visual Basic does not allow fields to have the same name as other class members:
		Private leftComprehend_Conflict As Boolean
'INSTANT VB NOTE: The field frequency was renamed since Visual Basic does not allow fields to have the same name as other class members:
		Private frequency_Conflict As Integer = 0

		Public Property LowerEnd() As Double
			Get
				Return lowerEnd_Conflict
			End Get
			Private Set(ByVal value As Double)
				lowerEnd_Conflict = value
			End Set
		End Property
		Public Property UpperEnd() As Double
			Get
				Return upperEnd_Conflict
			End Get
			Private Set(ByVal value As Double)
				upperEnd_Conflict = value
			End Set
		End Property
		Public Property RightComprehend() As Boolean
			Get
				Return rightComprehend_Conflict
			End Get
			Private Set(ByVal value As Boolean)
				rightComprehend_Conflict = value
			End Set
		End Property
		Public Property LeftComprehend() As Boolean
			Get
				Return leftComprehend_Conflict
			End Get
			Private Set(ByVal value As Boolean)
				leftComprehend_Conflict = value
			End Set
		End Property
		Public Property Frequency() As Integer
			Get
				Return frequency_Conflict
			End Get
			Set(ByVal value As Integer)
				If Frequency + value < 0 Then
					Throw New ArgumentException("Frequency can't become negative")
				End If
				frequency_Conflict = value
			End Set
		End Property

		Public Sub New(ByVal lowerEnd As Double, ByVal upperEnd As Double, ByVal leftComprehend As Boolean, ByVal rightComprehend As Boolean)
			If lowerEnd >= upperEnd Then
				Throw New ArgumentException("lowerEnd can't be greater or equal than upperEnd")
			End If

			Me.LowerEnd = lowerEnd
			Me.UpperEnd = upperEnd
			Me.RightComprehend = rightComprehend
			Me.LeftComprehend = leftComprehend
		End Sub

		Public Function Contains(ByVal val As Double) As Boolean
			If LeftComprehend AndAlso RightComprehend Then
				Return val <= UpperEnd AndAlso val >= LowerEnd
			End If
			If LeftComprehend Then
				Return val < UpperEnd AndAlso val >= LowerEnd
			Else
				Return val <= UpperEnd AndAlso val > LowerEnd
			End If
		End Function

		Public Function LessThan(ByVal val As Double) As Boolean
			If RightComprehend Then
				Return UpperEnd < val
			Else
				Return UpperEnd <= val
			End If
		End Function

		Public Function MoreThan(ByVal val As Double) As Boolean
			If LeftComprehend Then
				Return LowerEnd > val
			Else
				Return LowerEnd >= val
			End If
		End Function
	End Class
End Namespace
