﻿Imports System
Imports System.Collections.Generic

Namespace Statistics___Homework_2A
    Friend Class ContinuousDistribution
        Private stepField As Double
        Private leftComprehendField As Boolean
        Private distributionField As List(Of Interval)
        Private totOccurrencesField As Integer

        Public Property [Step] As Double
            Get
                Return stepField
            End Get
            Private Set(ByVal value As Double)
                stepField = value
            End Set
        End Property

        Public Property LeftComprehend As Boolean
            Get
                Return leftComprehendField
            End Get
            Private Set(ByVal value As Boolean)
                leftComprehendField = value
            End Set
        End Property

        Public Property Distribution As List(Of Interval)
            Get
                Return distributionField
            End Get
            Private Set(ByVal value As List(Of Interval))
                distributionField = value
            End Set
        End Property

        Public Property TotOccurrences As Integer
            Get
                Return totOccurrencesField
            End Get
            Private Set(ByVal value As Integer)
                totOccurrencesField = value
            End Set
        End Property

        Public Sub New(ByVal startValue As Double, ByVal [step] As Double, ByVal leftComprehend As Boolean)
            If [step] <= 0 Then
                Throw New ArgumentException()
            End If

            Me.Step = [step]
            Me.LeftComprehend = leftComprehend
            TotOccurrences = 0
            Distribution = New List(Of Interval)()

            If Me.LeftComprehend Then
                Distribution.Add(New Interval(startValue, startValue + [step], True, False))
            Else
                Distribution.Add(New Interval(startValue, startValue + [step], False, True))
            End If
        End Sub

        Private Sub Append(ByVal val As Integer)
            While val > 0

                If LeftComprehend Then
                    Distribution.Add(New Interval(Distribution(CInt(Distribution.Count - 1)).UpperEnd, Distribution(CInt(Distribution.Count - 1)).UpperEnd + stepField, True, False))
                Else
                    Distribution.Add(New Interval(Distribution(CInt(Distribution.Count - 1)).UpperEnd, Distribution(CInt(Distribution.Count - 1)).UpperEnd + stepField, False, True))
                End If

                val -= 1
            End While
        End Sub

        Private Sub Prepend(ByVal val As Integer)
            While val > 0

                If LeftComprehend Then
                    Distribution.Insert(0, New Interval(Distribution(CInt(0)).LowerEnd - stepField, Distribution(CInt(0)).LowerEnd, True, False))
                Else
                    Distribution.Insert(0, New Interval(Distribution(CInt(0)).LowerEnd - stepField, Distribution(CInt(0)).LowerEnd, False, True))
                End If

                val -= 1
            End While
        End Sub

        Private Sub CheckValueToSum(ByVal valueToSum As Integer)
            If valueToSum <= 0 Then
                Throw New ArgumentException("valueToSum must be greater than 0")
            End If
        End Sub

        Private Function GetIntervalIndex(ByVal val As Double) As Integer
            If Not Contains(val) Then
                Throw New IndexOutOfRangeException("The value requested isn't in the distribution")
            End If

            For i As Integer = 0 To Distribution.Count - 1

                If Distribution(i).Contains(val) Then
                    Return i
                End If
            Next

            Throw New Exception("Unexpected exception")
        End Function

        Public Sub AddValue(ByVal val As Double, ByVal valueToSum As Integer)
            CheckValueToSum(valueToSum)

            If Distribution(Distribution.Count - 1).LessThan(val) Then
                Append(1)
                AddValue(val, valueToSum)
            ElseIf Distribution(0).MoreThan(val) Then
                Prepend(1)
                AddValue(val, valueToSum)
            Else

                For Each i As Interval In Distribution

                    If i.Contains(val) Then
                        TotOccurrences += valueToSum
                        i.Frequency += valueToSum
                        Exit For
                    End If
                Next
            End If
        End Sub

        Public Function Contains(ByVal val As Double) As Boolean
            Return LeftComprehend AndAlso (Distribution(Distribution.Count - 1).UpperEnd > val AndAlso Distribution(0).LowerEnd <= val) OrElse Not LeftComprehend AndAlso (Distribution(Distribution.Count - 1).UpperEnd >= val AndAlso Distribution(0).LowerEnd < val)
        End Function

        Public Function GetValueFrequency(ByVal val As Double) As Integer
            Return Distribution(GetIntervalIndex(val)).Frequency
        End Function

        Public Function GetValueRelativeFrequency(ByVal val As Double) As Double
            Return If(Distribution(GetIntervalIndex(val)).Frequency = 0, 0, CDbl(Distribution(GetIntervalIndex(val)).Frequency) / TotOccurrences)
        End Function

        Public Function GetValuePercentage(ByVal val As Double) As Double
            Return If(Distribution(GetIntervalIndex(val)).Frequency = 0, 0, CDbl(Distribution(GetIntervalIndex(val)).Frequency) / TotOccurrences * 100.0)
        End Function
    End Class
End Namespace
