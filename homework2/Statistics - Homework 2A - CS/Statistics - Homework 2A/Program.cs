﻿using System;
using System.Collections.Generic;

namespace Statistics___Homework_2A
{
    class Program
    {
        private static Random rand = new Random();
        private static void OnlineMeanVsNaiveMean()
        {
            OnlineMean mean = new OnlineMean();
            int sum = 0;
            double meanNaive;
            int i = rand.Next(10, 100);

            Console.WriteLine("\nONLINE MEAN VS NAIVE MEAN\n");

            for(; i > 0; i--)
            {
                int randVal = rand.Next(1, 10);
                sum = sum + randVal;
                Console.WriteLine("Random value in [1-100]: " + randVal);
                mean.addValue(randVal);
                Console.WriteLine("Current online mean: " + mean.CurrentAvg);
            }

            meanNaive = (double)sum / (double)mean.Index;

            Console.WriteLine("Naive mean: " + meanNaive);
        }
        private static void TestDiscreteDistribution()
        {
            DiscreteDistribution distribution = new DiscreteDistribution();

            for (int  i = rand.Next(10, 20); i > 0; i--)
            {
                distribution.AddValue(rand.Next(18, 31), rand.Next(1, 5));
            }

            Console.WriteLine("\nTEST DISCRETE DISTRIBUTION\n");
            Console.WriteLine("Total elements: " + distribution.Distribution.Count);
            Console.WriteLine("Total occurrences: " + distribution.TotOccurrences);
            foreach (Object entry in distribution.Distribution.Keys)
            {
                Console.WriteLine(entry + " frequency: " + distribution.GetKeyFrequency(entry));
                Console.WriteLine(entry + " relative frequency: " + distribution.GetKeyRelativeFrequency(entry));
                Console.WriteLine(entry + " percentage: " + distribution.GetKeyPercentage(entry));
            }
        }
        
        private static void TestContinuousDistribution()
        {
            double startValue = rand.NextDouble();
            double step = 0.1;
            List<double> generatedValues = new List<double>();
            ContinuousDistribution distribution = new ContinuousDistribution(startValue, step, true);

            for (int i = 0; i < rand.Next(10, 20); i++)
            {
                generatedValues.Add(rand.NextDouble());
                distribution.AddValue(generatedValues[i], rand.Next(1, 5));
            }

            Console.WriteLine("\nTEST CONTINUOUS DISTRIBUTION\n");
            Console.WriteLine("Start value: " + startValue);
            Console.WriteLine("Total elements: " + distribution.Distribution.Count);
            Console.WriteLine("Total occurrences: " + distribution.TotOccurrences);
            for (int i = 0; i < distribution.Distribution.Count; i++)
            {
                Console.WriteLine(distribution.Distribution[i].LowerEnd + " frequency: " + distribution.GetValueFrequency(distribution.Distribution[i].LowerEnd + (step / 2)));
                Console.WriteLine(distribution.Distribution[i].LowerEnd + " relative frequency: " + distribution.GetValueRelativeFrequency(distribution.Distribution[i].LowerEnd + (step / 2)));
                Console.WriteLine(distribution.Distribution[i].LowerEnd + " percentage: " + distribution.GetValuePercentage(distribution.Distribution[i].LowerEnd + (step / 2)));
            }
        }
        
        static void Main(string[] args)
        {
            OnlineMeanVsNaiveMean();
            TestDiscreteDistribution();
            TestContinuousDistribution();
        }

    }
}
