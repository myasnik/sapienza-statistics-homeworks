﻿using System;

namespace Statistics___Homework_2A
{
    class Interval
    {
        private double lowerEnd;
        private double upperEnd;
        private bool rightComprehend;
        private bool leftComprehend;
        private int frequency = 0;

        public double LowerEnd { get => lowerEnd; private set => lowerEnd = value; }
        public double UpperEnd { get => upperEnd; private set => upperEnd = value; }
        public bool RightComprehend { get => rightComprehend; private set => rightComprehend = value; }
        public bool LeftComprehend { get => leftComprehend; private set => leftComprehend = value; }
        public int Frequency
        {
            get => frequency;
            set
            {
                if (Frequency + value < 0)
                {
                    throw new ArgumentException("Frequency can't become negative");
                }
                frequency = value;
            }
        }

        public Interval(double lowerEnd, double upperEnd, bool leftComprehend, bool rightComprehend)
        {
            if (lowerEnd >= upperEnd)
            {
                throw new ArgumentException("lowerEnd can't be greater or equal than upperEnd");
            }

            LowerEnd = lowerEnd;
            UpperEnd = upperEnd;
            RightComprehend = rightComprehend;
            LeftComprehend = leftComprehend;
        }

        public bool Contains(double val)
        {
            if (LeftComprehend && RightComprehend)
            {
                return val <= UpperEnd && val >= LowerEnd;
            }
            if (LeftComprehend)
            {
                return val < UpperEnd && val >= LowerEnd;
            } 
            else
            {
                return val <= UpperEnd && val > LowerEnd;
            }
        }

        public bool LessThan(double val)
        {
            if (RightComprehend)
            {
                return UpperEnd < val;
            } 
            else
            {
                return UpperEnd <= val;
            }
        }

        public bool MoreThan(double val)
        {
            if (LeftComprehend)
            {
                return LowerEnd > val;
            }
            else
            {
                return LowerEnd >= val;
            }
        }
    }
}
