﻿
namespace FormRectangle
{
    partial class FormRectangle
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.pictureBoxRectangle = new System.Windows.Forms.PictureBox();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxRectangle)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBoxRectangle
            // 
            this.pictureBoxRectangle.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBoxRectangle.Location = new System.Drawing.Point(12, 12);
            this.pictureBoxRectangle.Name = "pictureBoxRectangle";
            this.pictureBoxRectangle.Size = new System.Drawing.Size(776, 426);
            this.pictureBoxRectangle.TabIndex = 0;
            this.pictureBoxRectangle.TabStop = false;
            this.pictureBoxRectangle.MouseDown += new System.Windows.Forms.MouseEventHandler(this.PictureBoxRectangle_MouseDown);
            this.pictureBoxRectangle.MouseMove += new System.Windows.Forms.MouseEventHandler(this.PictureBoxRectangle_MouseMove);
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 10;
            this.timer1.Tick += new System.EventHandler(this.DrawRectangle);
            // 
            // FormRectangle
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.pictureBoxRectangle);
            this.Name = "FormRectangle";
            this.Text = "FormRectangle";
            this.Load += new System.EventHandler(this.FormRectangle_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxRectangle)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBoxRectangle;
        private System.Windows.Forms.Timer timer1;
    }
}

