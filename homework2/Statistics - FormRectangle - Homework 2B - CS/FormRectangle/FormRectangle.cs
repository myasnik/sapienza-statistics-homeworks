﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace FormRectangle
{
    public partial class FormRectangle : Form
    {
        private Rectangle rectangle;
        private Pen rectangleBorder;

        private Point firstPointDrag;

        public FormRectangle()
        {
            InitializeComponent();
            this.SetStyle(ControlStyles.OptimizedDoubleBuffer | ControlStyles.AllPaintingInWmPaint | ControlStyles.UserPaint, true);
            this.UpdateStyles();
        }

        private void FormRectangle_Load(object sender, EventArgs e)
        {
            rectangle = new Rectangle(100, 100, 200, 100);
            rectangleBorder = Pens.Orange;
            pictureBoxRectangle.Image = new Bitmap(pictureBoxRectangle.Width, pictureBoxRectangle.Height);
        }

        private void DrawRectangle(object sender, EventArgs e)
        {
            var g = Graphics.FromImage(pictureBoxRectangle.Image);
            g.Clear(this.BackColor);
            g.DrawRectangle(rectangleBorder, rectangle);
            pictureBoxRectangle.Refresh();
        }

        private void PictureBoxRectangle_MouseDown(object sender, MouseEventArgs e)
        {
            if ((e.Button == MouseButtons.Left || e.Button == MouseButtons.Right) && rectangle.Contains(PointToClient(MousePosition)))
            {
                firstPointDrag = MousePosition;
            }
            else
            {
                firstPointDrag = new Point();
            }
        }

        private void PictureBoxRectangle_MouseMove(object sender, MouseEventArgs e)
        {
            if (!firstPointDrag.IsEmpty)
            {
                Point tempPoint = MousePosition;
                Point resPoint = new Point(firstPointDrag.X - tempPoint.X, firstPointDrag.Y - tempPoint.Y);
                if (e.Button == MouseButtons.Left)
                {
                    rectangle.Location = new Point(rectangle.Location.X - resPoint.X, rectangle.Location.Y - resPoint.Y);
                }
                else if (e.Button == MouseButtons.Right)
                {
                    rectangle.Width += tempPoint.X - firstPointDrag.X;
                    rectangle.Height += tempPoint.Y - firstPointDrag.Y;
                }
                firstPointDrag = tempPoint;
            }
        }
    }
}
