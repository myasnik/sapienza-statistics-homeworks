﻿using System;
using System.Collections.Generic;

namespace Statistics___Homework_4
{
    class DiscreteDistribution
    {
        private Dictionary<Object, int> distribution;
        private int totOccurrences;

        public Dictionary<object, int> Distribution { get => distribution; private set => distribution = value; }
        public int TotOccurrences { get => totOccurrences; private set => totOccurrences = value; }

        public DiscreteDistribution()
        {
            Distribution = new Dictionary<Object, int>();
            TotOccurrences = 0;
        }

        protected void CheckValueToSum(int valueToSum)
        {
            if (valueToSum <= 0)
            {
                throw new ArgumentOutOfRangeException("valueToSum must be greater than 0");
            }
        }

        public void AddValue(Object key)
        {
            if (Distribution.ContainsKey(key))
            {
                Distribution[key]++;
            }
            else
            {
                Distribution.Add(key, 1);
            }
            TotOccurrences++;
        }

        public void AddValue(Object key, int valueToSum)
        {
            CheckValueToSum(valueToSum);
            if (Distribution.ContainsKey(key))
            {
                Distribution[key] += valueToSum;
            }
            else
            {
                Distribution.Add(key, valueToSum);
            }
            TotOccurrences += valueToSum;
        }

        public int GetKeyFrequency(Object key)
        {
            if (Distribution.ContainsKey(key))
            {
                return Distribution[key];
            }
            else
            {
                throw new KeyNotFoundException();
            }
        }

        public double GetKeyRelativeFrequency(Object key)
        {
            if (Distribution.ContainsKey(key))
            {
                return (double)Distribution[key] / (double)TotOccurrences;
            }
            else
            {
                throw new KeyNotFoundException();
            }
        }

        public double GetKeyPercentage(Object key)
        {
            if (Distribution.ContainsKey(key))
            {
                return ((double)Distribution[key] / (double)TotOccurrences) * 100;
            }
            else
            {
                throw new KeyNotFoundException();
            }
        }
    }
}

