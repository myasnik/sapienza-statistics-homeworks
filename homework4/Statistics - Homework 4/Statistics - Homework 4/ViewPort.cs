﻿using System.Drawing;

namespace Statistics___Homework_4
{
    class ViewPort
    {
        private Graphics graph;
        private Rectangle viewPortArea;
        private double xScale;
        private double yScale;

        public ViewPort(Graphics graph, Rectangle viewPortArea)
        {
            Graph = graph;
            ViewPortArea = viewPortArea;
            UpdateScale();
        }

        public Graphics Graph { get => graph; private set => graph = value; }
        public Rectangle ViewPortArea { get => viewPortArea; private set => viewPortArea = value; }
        public double XScale { get => xScale; private set => xScale = value; }
        public double YScale { get => yScale; private set => yScale = value; }

        public void FillEllipse(int x, int y, Color color, Size size)
        {
            Graph.FillEllipse(new SolidBrush(color), new Rectangle(TransformPoint(new Point(x, y)), ScaleSize(size)));
        }

        public void DrawPoint(int x, int y, Color color, Size size)
        {
            Graph.FillEllipse(new SolidBrush(color), new Rectangle(TransformPoint(new Point(x, y)), size));
        }

        public void DrawPointRescalingPos(int x, int y, Color color, Size size, int scaleX, int scaleY)
        {
            Point tempPoint = TransformPoint(new Point(x * scaleX, y * scaleY));
            Graph.FillEllipse(new SolidBrush(color), new Rectangle(tempPoint, size));
        }

        public void DrawString(string s, Color color, Point point, string fontFamily, int fontSize)
        {
            Size finalStringSize = ScaleSize(Graph.MeasureString(s, new Font(fontFamily, fontSize)).ToSize());
            Size stringSize = Graph.MeasureString(s, new Font(fontFamily, fontSize)).ToSize();
            while (fontSize > 1 && (finalStringSize.Width < stringSize.Width && finalStringSize.Height < stringSize.Height))
            {
                fontSize--;
                stringSize = Graph.MeasureString(s, new Font(fontFamily, fontSize)).ToSize();
            }
            Graph.DrawString(s, new Font(fontFamily, fontSize), new SolidBrush(color), TransformPoint(point));
        }

        public void DrawStringRescalingPos(string s, Color color, Point point, int scaleX, int scaleY, string fontFamily, int fontSize)
        {
            DrawString(s, color, new Point(point.X * scaleX, point.Y * scaleY), fontFamily, fontSize);
        }

        public void DrawRectangle(Color color, Rectangle r)
        {
            Graph.DrawRectangle(new Pen(new SolidBrush(color)), TransformRectangle(r));
        }

        public void DrawLine(Color color, Point start, Point end)
        {
            Graph.DrawLine(new Pen(new SolidBrush(color)), TransformPoint(start), TransformPoint(end));
        }

        public void DrawLineRescalingXPos(Color color, Point start, Point end, int scale)
        {
            Point tempStart = TransformPoint(start);
            Point tempEnd = TransformPoint(end);
            Graph.DrawLine(new Pen(new SolidBrush(color)), new Point(tempStart.X * scale, tempStart.Y), 
                new Point(tempEnd.X * scale, tempEnd.Y));
        }

        public void FillRectangle(Color color, Rectangle r)
        {
            Graph.FillRectangle(new SolidBrush(color), TransformRectangle(r));
        }

        private Point TransformPoint(Point point)
        {
            return new Point((int)(ViewPortArea.X + ((point.X - Graph.VisibleClipBounds.X) * XScale)),
                (int)(ViewPortArea.Y + ((point.Y - Graph.VisibleClipBounds.Y) * YScale)));
        }

        private Rectangle TransformRectangle(Rectangle r)
        {
            return new Rectangle(TransformPoint(r.Location), new Size((int)(r.Width * XScale), (int)(r.Height * YScale)));
        }

        public Size ScaleSize(Size size)
        {
            return new Size((int)(size.Width * XScale), (int)(size.Height * YScale));
        }

        public void Move(Point dest)
        {
            ViewPortArea = new Rectangle(dest.X, dest.Y, ViewPortArea.Width, ViewPortArea.Height);
        }

        public void Scale(Point init, Point end)
        {
            ViewPortArea = new Rectangle(ViewPortArea.X, ViewPortArea.Y, ViewPortArea.Width + end.X - init.X,
                ViewPortArea.Height + end.Y - init.Y);
            UpdateScale();
        }

        private void UpdateScale()
        {
            XScale = ViewPortArea.Width / Graph.VisibleClipBounds.Width;
            YScale = ViewPortArea.Height / Graph.VisibleClipBounds.Height;
        }
    }
}
