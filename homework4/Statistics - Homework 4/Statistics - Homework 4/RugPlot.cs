﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace Statistics___Homework_4
{
    class RugPlot
    {
        private List<int> values;
        private int lineHeight;
        private int SCALE_FACTOR = 46;

        public RugPlot(List<int> values, int lineHeight)
        {
            Values = values;
            LineHeight = lineHeight;
        }

        public List<int> Values { get => values; private set => values = value; }
        public int LineHeight { get => lineHeight; private set => lineHeight = value; }

        public void AddBar(int val)
        {
            Values.Add(val);
        }

        public void DrawPlot(ViewPort view)
        {
            Point startingCoord = new Point(0, (int)(view.Graph.VisibleClipBounds.Height + LineHeight * 3));
            foreach (int u in values)
            {
                //view.DrawLine(Color.Black, new Point(startingCoord.X + u, startingCoord.Y), new Point(startingCoord.X + u, startingCoord.Y + LineHeight));
                //view.DrawLineRescalingXPos(Color.Black, new Point(startingCoord.X + u, startingCoord.Y), new Point(startingCoord.X + u, startingCoord.Y + LineHeight), 10);
                view.DrawLine(Color.Black, new Point((startingCoord.X + u) * SCALE_FACTOR, startingCoord.Y), new Point((startingCoord.X + u) * SCALE_FACTOR, startingCoord.Y + LineHeight));
            }
        }
    }
}
