﻿using System;
using System.Collections.Generic;

namespace Statistics___Homework_4
{
    class ContinuousDistribution
    {
        private double step;
        private bool leftComprehend;
        private List<Interval> distribution;
        private int totOccurrences;

        public double Step { get => step; private set => step = value; }
        public bool LeftComprehend { get => leftComprehend; private set => leftComprehend = value; }
        public List<Interval> Distribution { get => distribution; private set => distribution = value; }
        public int TotOccurrences { get => totOccurrences; private set => totOccurrences = value; }

        public ContinuousDistribution(double startValue, double step, bool leftComprehend)
        {
            if (step <= 0)
            {
                throw new ArgumentException();
            }

            Step = step;
            LeftComprehend = leftComprehend;
            TotOccurrences = 0;
            Distribution = new List<Interval>();

            if (LeftComprehend)
            {
                Distribution.Add(new Interval(startValue, startValue + step, true, false));
            }
            else
            {
                Distribution.Add(new Interval(startValue, startValue + step, false, true));
            }
        }

        private void Append(int val)
        {
            for (; val > 0; val--)
            {
                if (LeftComprehend)
                {
                    Distribution.Add(new Interval(Distribution[Distribution.Count - 1].UpperEnd, Distribution[Distribution.Count - 1].UpperEnd + step, true, false));
                }
                else
                {
                    Distribution.Add(new Interval(Distribution[Distribution.Count - 1].UpperEnd, Distribution[Distribution.Count - 1].UpperEnd + step, false, true));
                }
            }
        }

        private void Prepend(int val)
        {
            for (; val > 0; val--)
            {
                if (LeftComprehend)
                {
                    Distribution.Insert(0, new Interval(Distribution[0].LowerEnd - step, Distribution[0].LowerEnd, true, false));
                }
                else
                {
                    Distribution.Insert(0, new Interval(Distribution[0].LowerEnd - step, Distribution[0].LowerEnd, false, true));
                }
            }
        }

        private int GetIntervalIndex(double val)
        {
            if (!Contains(val))
            {
                throw new IndexOutOfRangeException("The value requested isn't in the distribution");
            }
            for (int i = 0; i < Distribution.Count; i++)
            {
                if (Distribution[i].Contains(val))
                {
                    return i;
                }
            }
            throw new Exception("Unexpected exception");
        }

        public void AddValue(double val)
        {
            if (Distribution[Distribution.Count - 1].LessThan(val))
            {
                Append(1);
                AddValue(val);
            }
            else if (Distribution[0].MoreThan(val))
            {
                Prepend(1);
                AddValue(val);
            }
            else
            {
                foreach (Interval i in Distribution)
                {
                    if (i.Contains(val))
                    {
                        TotOccurrences++;
                        i.AddElement(val);
                        break;
                    }
                }
            }
        }

        public bool Contains(double val)
        {
            return ((LeftComprehend && (Distribution[Distribution.Count - 1].UpperEnd > val &&
                                        Distribution[0].LowerEnd <= val)) ||
                    (!LeftComprehend && (Distribution[Distribution.Count - 1].UpperEnd >= val &&
                                         Distribution[0].LowerEnd < val)));
        }

        public int GetValueFrequency(double val)
        {
            return Distribution[GetIntervalIndex(val)].Frequency;
        }

        public double GetValueRelativeFrequency(double val)
        {
            return Distribution[GetIntervalIndex(val)].Frequency == 0 ? 0 : (double)Distribution[GetIntervalIndex(val)].Frequency / (double)TotOccurrences;
        }

        public double GetValuePercentage(double val)
        {
            return Distribution[GetIntervalIndex(val)].Frequency == 0 ? 0 : ((double)Distribution[GetIntervalIndex(val)].Frequency / (double)TotOccurrences) * 100.0;
        }

        public int GetIndex(Interval i)
        {
            for (int j = 0; j < Distribution.Count; j++)
            {
                if (Distribution[j].GenericBoundsEquals(i)) return j;
            }
            throw new Exception("Index not found");
        }

        public Interval GetInterval(double v)
        {
            foreach (Interval i in Distribution)
            {
                if (i.Contains(v)) return i;
            }
            throw new Exception("Interval not found");
        }

        public int GetIndexGivenVal(double val)
        {
            return GetIndex(GetInterval(val));
        }
    }
}
