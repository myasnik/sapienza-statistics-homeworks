﻿using System.Collections.Generic;
using System.Drawing;

namespace Statistics___Homework_4
{
    class Legend
    {
        private string name;
        private PositionEnum pos;
        private Dictionary<Color, string> values = new Dictionary<Color, string>();

        public Legend(string name, Dictionary<Color, string> values, PositionEnum pos = PositionEnum.UpperRight)
        {
            Name = name;
            Values = values;
            Pos = pos;
        }

        public string Name { get => name; private set => name = value; }
        public Dictionary<Color, string> Values { get => values; private set => values = value; }
        internal PositionEnum Pos { get => pos; private set => pos = value; }

        public void AddElement(Color color, string s)
        {
            values.Add(color, s);
        }
    }
}
