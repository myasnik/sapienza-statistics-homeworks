﻿using System.Collections.Generic;
using System.Drawing;

namespace Statistics___Homework_4
{
    class HistogramPlot
    {
        private List<HistogramUnit> bars;
        private static int FONT_SIZE = 10;
        private static int RECT_H_MULT = 5;
        private EnumOrientation orientation;

        public HistogramPlot(List<HistogramUnit> bars, EnumOrientation orientation)
        {
            Bars = bars;
            Orientation = orientation;
        }

        internal List<HistogramUnit> Bars { get => bars; private set => bars = value; }
        internal EnumOrientation Orientation { get => orientation; private set => orientation = value; }

        public void AddBar(HistogramUnit bar)
        {
            Bars.Add(bar);
        }

        public void DrawPlot(ViewPort view)
        {
            int rectangleWidth;
            int startingX;
            int startingY;

            switch (Orientation)
            {
                case EnumOrientation.BOTTOMUP:
                    rectangleWidth = (int)view.Graph.VisibleClipBounds.Width / Bars.Count;
                    startingX = 0;
                    foreach (HistogramUnit u in Bars)
                    {
                        view.FillRectangle(u.Color, new Rectangle(new Point(startingX, (int)(view.Graph.VisibleClipBounds.Height - (u.Interval.Frequency * RECT_H_MULT))), new Size(rectangleWidth, u.Interval.Frequency * RECT_H_MULT)));
                        view.DrawString(u.ToString(), Color.Black, new Point(startingX, (int)view.Graph.VisibleClipBounds.Height), "arial", FONT_SIZE);
                        view.DrawLine(Color.Black, new Point(startingX + (rectangleWidth / 2), (int)view.Graph.VisibleClipBounds.Height), new Point(startingX + (rectangleWidth / 2), (int)(view.Graph.VisibleClipBounds.Height - (u.Interval.Frequency * RECT_H_MULT))));
                        view.DrawString(u.Interval.Mean.CurrentAvg.ToString(), Color.Black, new Point(startingX, (int)view.Graph.VisibleClipBounds.Height - (u.Interval.Frequency * RECT_H_MULT) - FONT_SIZE * 3), "arial", FONT_SIZE);
                        startingX += rectangleWidth;
                    }
                    break;
                case EnumOrientation.RIGHTLEFT:
                    rectangleWidth = (int)view.Graph.VisibleClipBounds.Height / Bars.Count;
                    startingY = (int)view.Graph.VisibleClipBounds.Height - rectangleWidth;
                    foreach (HistogramUnit u in Bars)
                    {
                        view.FillRectangle(u.Color, new Rectangle(new Point((int)(view.Graph.VisibleClipBounds.Width - (u.Interval.Frequency * RECT_H_MULT)), startingY), new Size(u.Interval.Frequency * RECT_H_MULT, rectangleWidth)));
                        view.DrawString(u.ToString(), Color.Black, new Point((int)view.Graph.VisibleClipBounds.Width, startingY), "arial", FONT_SIZE);
                        view.DrawLine(Color.Black, new Point((int)view.Graph.VisibleClipBounds.Width, startingY + (rectangleWidth / 2)), new Point((int)(view.Graph.VisibleClipBounds.Width - (u.Interval.Frequency * RECT_H_MULT)), startingY + (rectangleWidth / 2)));
                        view.DrawString(u.Interval.Mean.CurrentAvg.ToString(), Color.Black, new Point((int)view.Graph.VisibleClipBounds.Width - (u.Interval.Frequency * RECT_H_MULT) - FONT_SIZE * 3, startingY + FONT_SIZE * 4), "arial", FONT_SIZE);
                        startingY -= rectangleWidth;
                    }
                    break;

            }
        }
    }
}
