﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace Statistics___Homework_4
{
    public partial class Form : System.Windows.Forms.Form
    {
        Graphics g;
        ViewPort scatterHistoVP;
        ViewPort contingencyVP;

        private Point firstPointDrag;

        CSVParser treeCSV;
        ContinuousDistribution girthDist;
        ContinuousDistribution volumeDist;
        DiscreteDistribution bivariateDist;

        bool scatterHistoSelected;

        public Form()
        {
            InitializeComponent();
            SetStyle(ControlStyles.OptimizedDoubleBuffer | ControlStyles.AllPaintingInWmPaint | ControlStyles.UserPaint, true);
            UpdateStyles();
        }

        private void Form_Load(object sender, EventArgs e)
        {
            pictureBox.Image = new Bitmap(pictureBox.Width, pictureBox.Height);
            g = Graphics.FromImage(pictureBox.Image);

            scatterHistoVP = new ViewPort(g, new Rectangle(0, 0, (pictureBox.Width / 2) - 20, (pictureBox.Height / 2) + 200));
            contingencyVP = new ViewPort(g, new Rectangle((pictureBox.Height / 3) + (pictureBox.Height / 2) + 200, 0, (pictureBox.Width) + 100, (pictureBox.Height /2 ) + 200));

            treeCSV = new CSVParser("trees.csv", typeof(Tree), true);
            girthDist = new ContinuousDistribution(0, 10, true);
            volumeDist = new ContinuousDistribution(0, 10, true);
            bivariateDist = new DiscreteDistribution();

            foreach (Tree t in treeCSV.Data)
            {
                girthDist.AddValue(t.girth);
                volumeDist.AddValue(t.volume);
                bivariateDist.AddValue(new Tuple<double, double>(t.girth, t.volume));
            }

            Draw();
        }

        private void pictureBox_MouseDown(object sender, MouseEventArgs e)
        {
            if ((e.Button == MouseButtons.Left || e.Button == MouseButtons.Right) && 
                (scatterHistoVP.ViewPortArea.Contains(e.Location) ||
                contingencyVP.ViewPortArea.Contains(e.Location)))
            {
                firstPointDrag = e.Location;
                if (scatterHistoVP.ViewPortArea.Contains(e.Location))
                {
                    scatterHistoSelected = true;
                }
                else
                {
                    scatterHistoSelected = false;
                }
            }
            else
            {
                firstPointDrag = new Point();
            }
        }

        private void pictureBox_MouseMove(object sender, MouseEventArgs e)
        {
            if (!firstPointDrag.IsEmpty)
            {
                Point tempPoint = e.Location;
                Point resPoint = new Point(firstPointDrag.X - tempPoint.X, firstPointDrag.Y - tempPoint.Y);
                if (e.Button == MouseButtons.Left)
                {
                    if (scatterHistoVP.ViewPortArea.Contains(firstPointDrag) && scatterHistoSelected)
                    {
                        scatterHistoVP.Move(new Point(scatterHistoVP.ViewPortArea.Location.X - resPoint.X,
                            scatterHistoVP.ViewPortArea.Location.Y - resPoint.Y));
                    }
                    else if(contingencyVP.ViewPortArea.Contains(firstPointDrag) && !scatterHistoSelected)
                    {
                        contingencyVP.Move(new Point(contingencyVP.ViewPortArea.Location.X - resPoint.X,
                            contingencyVP.ViewPortArea.Location.Y - resPoint.Y));
                    }
                }
                else if (e.Button == MouseButtons.Right)
                {
                    if (scatterHistoVP.ViewPortArea.Contains(firstPointDrag) && scatterHistoSelected)
                    {
                        scatterHistoVP.Scale(firstPointDrag, tempPoint);
                    }
                    else if (contingencyVP.ViewPortArea.Contains(firstPointDrag) && !scatterHistoSelected)
                    {
                        contingencyVP.Scale(firstPointDrag, tempPoint);
                    }
                }
                firstPointDrag = tempPoint;
                Draw();
            }
        }

        private void Draw()
        {
            ScatterPlot sp = new ScatterPlot(new List<ScatterUnit>(), new Size(7, 7));
            HistogramPlot hpR = new HistogramPlot(new List<HistogramUnit>(), EnumOrientation.BOTTOMUP);
            HistogramPlot hpC = new HistogramPlot(new List<HistogramUnit>(), EnumOrientation.RIGHTLEFT);
            RugPlot rpR = new RugPlot(new List<int>(), 10);

            foreach (Tree t in treeCSV.Data)
            {
                sp.AddPoint(new ScatterUnit(Color.Green, new Point((int)t.girth, (int)t.volume)));
                rpR.AddBar((int)t.girth);
            }

            foreach (Interval i in girthDist.Distribution)
            {
                hpR.AddBar(new HistogramUnit(Color.Brown, i));
            }

            foreach (Interval i in volumeDist.Distribution)
            {
                hpC.AddBar(new HistogramUnit(Color.Brown, i));
            }

            ContingencyTable cont = new ContingencyTable(girthDist, volumeDist, bivariateDist, "Girth", "Volume");

            g.Clear(BackColor);
            g.DrawRectangle(Pens.Orange, scatterHistoVP.ViewPortArea);
            g.DrawRectangle(Pens.Orange, contingencyVP.ViewPortArea);
            sp.DrawPlot(scatterHistoVP);
            hpR.DrawPlot(scatterHistoVP);
            hpC.DrawPlot(scatterHistoVP);
            cont.DrawPlot(contingencyVP);
            rpR.DrawPlot(scatterHistoVP);
            pictureBox.Refresh();
        }
    }
}
