﻿
using System;
using System.Collections.Generic;
using System.Drawing;

namespace Statistics___Homework_4
{
    class ContingencyTable
    {
        private ContinuousDistribution rowDistribution;
        private ContinuousDistribution colDistribution;
        private DiscreteDistribution bivariateDistribution;
        private int[,] array;
        private string rowAttributeName;
        private string colAttributeName;

        public ContingencyTable(ContinuousDistribution rowDistribution, ContinuousDistribution colDistribution, DiscreteDistribution bivariateDistribution, string rowAttributeName, string colAttributeName)
        {
            RowAttributeName = rowAttributeName;
            ColAttributeName = colAttributeName;
            ColDistribution = colDistribution;
            RowDistribution = rowDistribution;
            BivariateDistribution = bivariateDistribution;
            array = new int[RowDistribution.Distribution.Count, ColDistribution.Distribution.Count];
            
            Array.Clear(array, 0, array.Length);
            foreach (KeyValuePair<object, int> kv in BivariateDistribution.Distribution)
            {
                Tuple<double, double> vals = (Tuple<double, double>)kv.Key;
                int rowIndex = RowDistribution.GetIndexGivenVal(vals.Item1);
                int colIndex = ColDistribution.GetIndexGivenVal(vals.Item2);
                array[rowIndex, colIndex]++;
            }
        }

        public string RowAttributeName { get => rowAttributeName; private set => rowAttributeName = value; }
        public string ColAttributeName { get => colAttributeName; private set => colAttributeName = value; }
        internal ContinuousDistribution RowDistribution { get => rowDistribution; private set => rowDistribution = value; }
        internal ContinuousDistribution ColDistribution { get => colDistribution; private set => colDistribution = value; }
        internal DiscreteDistribution BivariateDistribution { get => bivariateDistribution; private set => bivariateDistribution = value; }

        public void DrawPlot(ViewPort view)
        {
            Size rowAttributeNameSize = view.Graph.MeasureString(RowAttributeName, new Font("arial", 10)).ToSize();
            Size colAttributeNameSize = view.Graph.MeasureString(ColAttributeName, new Font("arial", 10)).ToSize();
            Point c = new Point(0, 0);
            Point r = new Point(c.X, c.Y + colAttributeNameSize.Height + 1);
            view.DrawString(RowAttributeName, Color.Black, r, "arial", 10);
            view.DrawString(ColAttributeName, Color.Black, c, "arial", 10);
            r.Y += rowAttributeNameSize.Height;
            c.X += colAttributeNameSize.Width;

            foreach (Interval i in RowDistribution.Distribution)
            {
                view.DrawString(i.ToString(), Color.Black, r, "arial", 10);
                r.Y += (int)view.Graph.MeasureString(i.ToString(), new Font("arial", 10)).Height;
            }

            foreach (Interval i in ColDistribution.Distribution)
            {
                view.DrawString(i.ToString(), Color.Black, c, "arial", 10);
                c.X += (int)view.Graph.MeasureString(i.ToString(), new Font("arial", 10)).Width;
            }

            r.Y = rowAttributeNameSize.Height + colAttributeNameSize.Height;
            r.X = rowAttributeNameSize.Width;
            c.X = colAttributeNameSize.Width + rowAttributeNameSize.Width;
            Point s = new Point(rowAttributeNameSize.Width * 2, rowAttributeNameSize.Height + colAttributeNameSize.Height);

            for (int i = 0; i < RowDistribution.Distribution.Count; i++)
            {
                for (int j = 0; j < ColDistribution.Distribution.Count; j++)
                {
                    view.DrawString(array[i,j].ToString(), Color.Black, s, "arial", 10);
                    s.X += colAttributeNameSize.Width - (int)view.Graph.MeasureString(array[i, j].ToString(), new Font("arial", 10)).Width + 5;
                }
                s.X = rowAttributeNameSize.Width * 2;
                s.Y += rowAttributeNameSize.Height;
            }

        }

    }
}
