﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Statistics___Homework_4
{
    class Tree
    {
        public int index;
        public double girth;
        public int height;
        public double volume;

        public Tree()
        {
            this.index = 0;
            this.girth = 0;
            this.height = 0;
            this.volume = 0;
        }

        public Tree(int index = 0, double girth = 0, int height = 0, double volume = 0)
        {
            this.index = index;
            this.girth = girth;
            this.height = height;
            this.volume = volume;
        }
    }
}
