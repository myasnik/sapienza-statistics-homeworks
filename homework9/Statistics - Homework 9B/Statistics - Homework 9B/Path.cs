﻿using System;
using System.Collections.Generic;

namespace Statistics___Homework_9B
{
    class Path
    {
        private OnlineAlgos onlineAlgos;
        private List<double> pathVals;
        private IRandVar var;

        public Path(IRandVar var, int steps)
        {
            Var = var;
            onlineAlgos = new OnlineAlgos();
            PathVals = new List<double>();

            for (int j = 0; j < steps; j++)
            {
                double val = var.GetSample();
                PathVals.Add(val);
                OnlineAlgos.AddValue(val);
            }
        }

        public Path(IRandVar var, int steps, EnumFunction enumFunction)
        {
            Var = var;
            onlineAlgos = new OnlineAlgos();
            PathVals = new List<double>();

            for (int j = 0; j < steps; j++)
            {
                double val = 0;
                switch (enumFunction)
                {
                    case EnumFunction.EXP:
                        val = Math.Exp(var.GetSample());
                        break;
                    case EnumFunction.SQRT:
                        val = Math.Sqrt(var.GetSample());
                        break;
                }
                if (Double.IsNaN(val)) val = 0;
                PathVals.Add(val);
                OnlineAlgos.AddValue(val);
            }
        }

        public void Sort()
        {
            PathVals.Sort();
        }

        public List<double> PathVals { get => pathVals; private set => pathVals = value; }
        public IRandVar Var { get => var; private set => var = value; }
        internal OnlineAlgos OnlineAlgos { get => onlineAlgos; private set => onlineAlgos = value; }
    }
}
