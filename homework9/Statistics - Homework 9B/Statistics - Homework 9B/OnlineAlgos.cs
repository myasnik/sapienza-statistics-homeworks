﻿namespace Statistics___Homework_9B
{
    class OnlineAlgos
    {
        private double currentAvg = 0;
        private int index = 0;
        private double currentSemiVar = 0;

        public double CurrentAvg { get => currentAvg; private set => currentAvg = value; }
        public int Index { get => index; private set => index = value; }
        public double CurrentSemiVar { get => currentSemiVar; private set => currentSemiVar = value; }
        public double CurrentVar { get { if (index >= 2) return (CurrentSemiVar / (double)index); else return 0; } private set => CurrentSemiVar = value; }

        public void AddValue(double val)
        {
            double delta1, delta2;
            Index++;
            delta1 = val - CurrentAvg;
            CurrentAvg += delta1 / Index;
            delta2 = val - CurrentAvg;
            CurrentSemiVar += delta1 * delta2;
        }
    }
}
