﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace Statistics___Homework_9B
{
    public partial class Form1 : Form
    {
        Graphics g;

        List<Path> normsPaths;
        List<Path> expPaths;
        List<Path> sqrt1Paths;
        List<Path> sqrt2Paths;
        List<double> means;
        List<double> variances;
        List<double> norms;
        List<double> exp;
        List<double> sqrt;
        List<double> sqrtsqrt;

        public Form1()
        {
            InitializeComponent();
            SetStyle(ControlStyles.OptimizedDoubleBuffer | ControlStyles.AllPaintingInWmPaint | ControlStyles.UserPaint, true);
            UpdateStyles();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            pictureBox.Image = new Bitmap(pictureBox.Width, pictureBox.Height);
            g = Graphics.FromImage(pictureBox.Image);

            maxRvLab.Text = rvBar.Maximum.ToString();
            minRvLab.Text = rvBar.Minimum.ToString();
            curRvNumLab.Text = rvBar.Minimum.ToString();
            pathsMaxLab.Text = pathsBar.Maximum.ToString();
            pathsMinLab.Text = pathsBar.Minimum.ToString();
            curPathsNumLab.Text = pathsBar.Minimum.ToString();

            DisableCheckboxes();
        }

        private void simulButton_Click(object sender, EventArgs e)
        {
            normsPaths = new List<Path>();
            expPaths = new List<Path>();
            sqrt1Paths = new List<Path>();
            sqrt2Paths = new List<Path>();
            norms = new List<double>();
            means = new List<double>();
            variances = new List<double>();
            exp = new List<double>();
            sqrt = new List<double>();
            sqrtsqrt = new List<double>();

            for (int i = 0; i < pathsBar.Value; i++)
            {
                normsPaths.Add(new Path(new NormalRandomVariable(), rvBar.Value));
                normsPaths[i].Sort();
                norms.AddRange(normsPaths[i].PathVals);
                expPaths.Add(new Path(new NormalRandomVariable(), rvBar.Value, EnumFunction.EXP));
                expPaths[i].Sort();
                exp.AddRange(expPaths[i].PathVals);
                sqrt1Paths.Add(new Path(new NormalRandomVariable(), rvBar.Value, EnumFunction.SQRT));
                sqrt2Paths.Add(new Path(new NormalRandomVariable(), rvBar.Value, EnumFunction.SQRT));
                for (int j = 0; j < rvBar.Value; j++)
                {
                    double res = sqrt1Paths[i].PathVals[j] / sqrt2Paths[i].PathVals[j];
                    if (Double.IsFinite(res)) sqrtsqrt.Add(res);
                }
                sqrt1Paths[i].Sort();
                sqrt.AddRange(sqrt1Paths[i].PathVals);
                means.Add(normsPaths[i].OnlineAlgos.CurrentAvg);
                variances.Add(normsPaths[i].OnlineAlgos.CurrentVar);
            }

            EnableCheckboxes();
            Draw();
        }

        private void Draw()
        {
            g.Clear(BackColor);

            if (normCheckBox.Checked) DrawHistogram(norms, Color.Orange);

            if (meanNormCheckBox.Checked) DrawHistogram(means, Color.Green);

            if (varNormCheckBox.Checked) DrawHistogram(variances, Color.GreenYellow);

            if (expNormCheckBox.Checked) DrawHistogram(exp, Color.Red);

            if (squaNormCheckBox.Checked) DrawHistogram(sqrt, Color.Black);

            if (squasquaNormCheckBox.Checked) DrawHistogram(sqrtsqrt, Color.Blue);

            pictureBox.Refresh();
        }

        private void DrawHistogram(List<double> vals, Color color)
        {
            vals.Sort();
            HistogramPlot hist = new HistogramPlot(vals[0], vals[0], vals[vals.Count - 1], vals.Count, color, EnumOrientation.BOTTOMUP);
            for (int i = 0; i < vals.Count - 1; i++)
            {
                hist.AddNumber(vals[i]);
            }

            ViewPort vp = new ViewPort(g, new RectangleF(0, 0, pictureBox.Width, pictureBox.Height), vals[0], vals[vals.Count - 1], 0, hist.GetMaxFreq());
            hist.DrawPlot(vp);
        }

        private void pathsBar_MouseUp(object sender, MouseEventArgs e)
        {
            curPathsNumLab.Text = pathsBar.Value.ToString();
            DisableCheckboxes();
        }

        private void rvBar_MouseUp(object sender, MouseEventArgs e)
        {
            curRvNumLab.Text = rvBar.Value.ToString();
            DisableCheckboxes();
        }

        private void checkedChanged(object sender, EventArgs e)
        {
            Draw();
        }

        private void DisableCheckboxes()
        {
            normCheckBox.Enabled = false;
            expNormCheckBox.Enabled = false;
            meanNormCheckBox.Enabled = false;
            squaNormCheckBox.Enabled = false;
            squasquaNormCheckBox.Enabled = false;
            varNormCheckBox.Enabled = false;
        }

        private void EnableCheckboxes()
        {
            normCheckBox.Enabled = true;
            expNormCheckBox.Enabled = true;
            meanNormCheckBox.Enabled = true;
            squaNormCheckBox.Enabled = true;
            squasquaNormCheckBox.Enabled = true;
            varNormCheckBox.Enabled = true;
        }
    }
}
