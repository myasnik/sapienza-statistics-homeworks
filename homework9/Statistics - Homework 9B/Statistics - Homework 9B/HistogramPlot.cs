﻿using System.Collections.Generic;
using System.Drawing;

namespace Statistics___Homework_9B
{
    class HistogramPlot
    {
        private List<HistogramUnit> bars;
        private double rectangleWidth;
        private static int FONT_SIZE = 10;
        private EnumOrientation orientation;
        bool leftComprehend;
        bool rightComprehend;

        public HistogramPlot(List<HistogramUnit> bars, EnumOrientation orientation)
        {
            Bars = bars;
            Orientation = orientation;
        }

        public HistogramPlot(double pos, double len, int steps, Color barColor, EnumOrientation orientation)
        {
            Bars = new List<HistogramUnit>();
            RectangleWidth = len / steps;
            for (int i = 0; i < steps; i++)
            {
                Bars.Add(new HistogramUnit(barColor, new Interval(pos, pos + RectangleWidth, true, false)));
                pos += RectangleWidth;
            }
            Orientation = orientation;
        }

        public HistogramPlot(double pos, double start, double stop, int steps, Color barColor, EnumOrientation orientation)
        {
            Bars = new List<HistogramUnit>();
            RectangleWidth = (stop - start) / steps;
            for (int i = 0; i < steps; i++)
            {
                Bars.Add(new HistogramUnit(barColor, new Interval(pos, pos + RectangleWidth, true, false)));
                pos += RectangleWidth;
            }
            Orientation = orientation;
        }
        public HistogramPlot(double pos, double start, double stop, int steps, Color barColor, EnumOrientation orientation, bool leftComprehend, bool rightComprehend)
        {
            Bars = new List<HistogramUnit>();
            RectangleWidth = (stop - start) / steps;
            for (int i = 0; i < steps; i++)
            {
                if (i == steps - 1) Bars.Add(new HistogramUnit(barColor, new Interval(pos, stop, leftComprehend, rightComprehend)));
                else Bars.Add(new HistogramUnit(barColor, new Interval(pos, pos + RectangleWidth, leftComprehend, rightComprehend)));
                pos += RectangleWidth;
            }
            Orientation = orientation;
        }


        internal List<HistogramUnit> Bars { get => bars; private set => bars = value; }
        internal EnumOrientation Orientation { get => orientation; private set => orientation = value; }
        public double RectangleWidth { get => rectangleWidth; private set => rectangleWidth = value; }

        public void AddBar(HistogramUnit bar)
        {
            Bars.Add(bar);
        }

        public void AddNumber(double val)
        {
            foreach (HistogramUnit u in Bars)
            {
                if (u.Interval.Contains(val))
                {
                    u.Interval.AddElement(val);
                }
            }
        }

        public double GetMaxFreq()
        {
            double i = 0;
            foreach (HistogramUnit u in Bars)
            {
                if (u.Interval.Frequency > i) i = u.Interval.Frequency;
            }
            return i;
        }

        public void DrawPlot(ViewPort view)
        {
            double startingX;
            double startingY;

            switch (Orientation)
            {
                case EnumOrientation.BOTTOMUP:
                    startingX = Bars[0].Interval.LowerEnd;
                    foreach (HistogramUnit u in Bars)
                    {
                        view.FillRectangle(u.Color, new RectangleF((float)startingX, u.Interval.Frequency, (float)RectangleWidth, u.Interval.Frequency));
                        view.DrawString(u.ToString(), Color.Black, new PointF((float)startingX, 0), "arial", FONT_SIZE);
                        startingX += RectangleWidth;
                    }
                    break;
                case EnumOrientation.RIGHTLEFT:
                    // Still not perfect
                    startingY = Bars[0].Interval.UpperEnd;
                    foreach (HistogramUnit u in Bars)
                    {
                        view.FillRectangle(u.Color, new RectangleF(view.ViewPortArea.Width - u.Interval.Frequency, (float)startingY, u.Interval.Frequency, (float)RectangleWidth));
                        view.DrawString(u.ToString(), Color.Black, new PointF(view.ViewPortArea.Width, (float)startingY), "arial", FONT_SIZE);
                        startingY += RectangleWidth;
                    }
                    break;
                case EnumOrientation.LEFTRIGHT:
                    startingY = Bars[0].Interval.UpperEnd;
                    foreach (HistogramUnit u in Bars)
                    {
                        view.FillRectangle(u.Color, new RectangleF(0, (float)startingY, u.Interval.Frequency, (float)RectangleWidth));
                        view.DrawString(u.ToString(), Color.Black, new PointF(0, (float)startingY), "arial", FONT_SIZE);
                        startingY += RectangleWidth;
                    }
                    break;
            }
        }
    }
}
