﻿
namespace Statistics___Homework_9B
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBox = new System.Windows.Forms.PictureBox();
            this.simulButton = new System.Windows.Forms.Button();
            this.pathsBar = new System.Windows.Forms.TrackBar();
            this.rvBar = new System.Windows.Forms.TrackBar();
            this.rvLab = new System.Windows.Forms.Label();
            this.pathsLab = new System.Windows.Forms.Label();
            this.maxRvLab = new System.Windows.Forms.Label();
            this.pathsMaxLab = new System.Windows.Forms.Label();
            this.minRvLab = new System.Windows.Forms.Label();
            this.pathsMinLab = new System.Windows.Forms.Label();
            this.curRvNumLab = new System.Windows.Forms.Label();
            this.curPathsNumLab = new System.Windows.Forms.Label();
            this.normCheckBox = new System.Windows.Forms.CheckBox();
            this.meanNormCheckBox = new System.Windows.Forms.CheckBox();
            this.varNormCheckBox = new System.Windows.Forms.CheckBox();
            this.expNormCheckBox = new System.Windows.Forms.CheckBox();
            this.squaNormCheckBox = new System.Windows.Forms.CheckBox();
            this.squasquaNormCheckBox = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pathsBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rvBar)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox
            // 
            this.pictureBox.Location = new System.Drawing.Point(13, 84);
            this.pictureBox.Name = "pictureBox";
            this.pictureBox.Size = new System.Drawing.Size(1418, 547);
            this.pictureBox.TabIndex = 0;
            this.pictureBox.TabStop = false;
            // 
            // simulButton
            // 
            this.simulButton.Location = new System.Drawing.Point(1098, 12);
            this.simulButton.Name = "simulButton";
            this.simulButton.Size = new System.Drawing.Size(243, 55);
            this.simulButton.TabIndex = 1;
            this.simulButton.Text = "Perform simulation";
            this.simulButton.UseVisualStyleBackColor = true;
            this.simulButton.Click += new System.EventHandler(this.simulButton_Click);
            // 
            // pathsBar
            // 
            this.pathsBar.Location = new System.Drawing.Point(249, 33);
            this.pathsBar.Maximum = 50;
            this.pathsBar.Minimum = 10;
            this.pathsBar.Name = "pathsBar";
            this.pathsBar.Size = new System.Drawing.Size(157, 45);
            this.pathsBar.TabIndex = 2;
            this.pathsBar.Value = 10;
            this.pathsBar.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pathsBar_MouseUp);
            // 
            // rvBar
            // 
            this.rvBar.Location = new System.Drawing.Point(45, 33);
            this.rvBar.Maximum = 100;
            this.rvBar.Minimum = 10;
            this.rvBar.Name = "rvBar";
            this.rvBar.Size = new System.Drawing.Size(149, 45);
            this.rvBar.TabIndex = 3;
            this.rvBar.Value = 10;
            this.rvBar.MouseUp += new System.Windows.Forms.MouseEventHandler(this.rvBar_MouseUp);
            // 
            // rvLab
            // 
            this.rvLab.AutoSize = true;
            this.rvLab.Location = new System.Drawing.Point(75, 9);
            this.rvLab.Name = "rvLab";
            this.rvLab.Size = new System.Drawing.Size(101, 15);
            this.rvLab.TabIndex = 4;
            this.rvLab.Text = "Random variables";
            // 
            // pathsLab
            // 
            this.pathsLab.AutoSize = true;
            this.pathsLab.Location = new System.Drawing.Point(312, 9);
            this.pathsLab.Name = "pathsLab";
            this.pathsLab.Size = new System.Drawing.Size(36, 15);
            this.pathsLab.TabIndex = 5;
            this.pathsLab.Text = "Paths";
            // 
            // maxRvLab
            // 
            this.maxRvLab.AutoSize = true;
            this.maxRvLab.Location = new System.Drawing.Point(169, 63);
            this.maxRvLab.Name = "maxRvLab";
            this.maxRvLab.Size = new System.Drawing.Size(38, 15);
            this.maxRvLab.TabIndex = 6;
            this.maxRvLab.Text = "label1";
            // 
            // pathsMaxLab
            // 
            this.pathsMaxLab.AutoSize = true;
            this.pathsMaxLab.Location = new System.Drawing.Point(378, 63);
            this.pathsMaxLab.Name = "pathsMaxLab";
            this.pathsMaxLab.Size = new System.Drawing.Size(38, 15);
            this.pathsMaxLab.TabIndex = 7;
            this.pathsMaxLab.Text = "label1";
            // 
            // minRvLab
            // 
            this.minRvLab.AutoSize = true;
            this.minRvLab.Location = new System.Drawing.Point(44, 63);
            this.minRvLab.Name = "minRvLab";
            this.minRvLab.Size = new System.Drawing.Size(38, 15);
            this.minRvLab.TabIndex = 8;
            this.minRvLab.Text = "label1";
            // 
            // pathsMinLab
            // 
            this.pathsMinLab.AutoSize = true;
            this.pathsMinLab.Location = new System.Drawing.Point(249, 63);
            this.pathsMinLab.Name = "pathsMinLab";
            this.pathsMinLab.Size = new System.Drawing.Size(38, 15);
            this.pathsMinLab.TabIndex = 9;
            this.pathsMinLab.Text = "label1";
            // 
            // curRvNumLab
            // 
            this.curRvNumLab.AutoSize = true;
            this.curRvNumLab.Location = new System.Drawing.Point(97, 63);
            this.curRvNumLab.Name = "curRvNumLab";
            this.curRvNumLab.Size = new System.Drawing.Size(38, 15);
            this.curRvNumLab.TabIndex = 10;
            this.curRvNumLab.Text = "label1";
            // 
            // curPathsNumLab
            // 
            this.curPathsNumLab.AutoSize = true;
            this.curPathsNumLab.Location = new System.Drawing.Point(312, 63);
            this.curPathsNumLab.Name = "curPathsNumLab";
            this.curPathsNumLab.Size = new System.Drawing.Size(38, 15);
            this.curPathsNumLab.TabIndex = 11;
            this.curPathsNumLab.Text = "label1";
            // 
            // normCheckBox
            // 
            this.normCheckBox.AutoSize = true;
            this.normCheckBox.Location = new System.Drawing.Point(444, 12);
            this.normCheckBox.Name = "normCheckBox";
            this.normCheckBox.Size = new System.Drawing.Size(89, 19);
            this.normCheckBox.TabIndex = 12;
            this.normCheckBox.Text = "Normal(0,1)";
            this.normCheckBox.UseVisualStyleBackColor = true;
            this.normCheckBox.CheckedChanged += new System.EventHandler(this.checkedChanged);
            // 
            // meanNormCheckBox
            // 
            this.meanNormCheckBox.AutoSize = true;
            this.meanNormCheckBox.Location = new System.Drawing.Point(557, 12);
            this.meanNormCheckBox.Name = "meanNormCheckBox";
            this.meanNormCheckBox.Size = new System.Drawing.Size(136, 19);
            this.meanNormCheckBox.TabIndex = 13;
            this.meanNormCheckBox.Text = "Mean of Normal(0,1)";
            this.meanNormCheckBox.UseVisualStyleBackColor = true;
            this.meanNormCheckBox.CheckedChanged += new System.EventHandler(this.checkedChanged);
            // 
            // varNormCheckBox
            // 
            this.varNormCheckBox.AutoSize = true;
            this.varNormCheckBox.Location = new System.Drawing.Point(714, 12);
            this.varNormCheckBox.Name = "varNormCheckBox";
            this.varNormCheckBox.Size = new System.Drawing.Size(150, 19);
            this.varNormCheckBox.TabIndex = 14;
            this.varNormCheckBox.Text = "Variance of Normal(0,1)";
            this.varNormCheckBox.UseVisualStyleBackColor = true;
            this.varNormCheckBox.CheckedChanged += new System.EventHandler(this.checkedChanged);
            // 
            // expNormCheckBox
            // 
            this.expNormCheckBox.AutoSize = true;
            this.expNormCheckBox.Location = new System.Drawing.Point(667, 48);
            this.expNormCheckBox.Name = "expNormCheckBox";
            this.expNormCheckBox.Size = new System.Drawing.Size(84, 19);
            this.expNormCheckBox.TabIndex = 15;
            this.expNormCheckBox.Text = "exp(N(0,1))";
            this.expNormCheckBox.UseVisualStyleBackColor = true;
            this.expNormCheckBox.CheckedChanged += new System.EventHandler(this.checkedChanged);
            // 
            // squaNormCheckBox
            // 
            this.squaNormCheckBox.AutoSize = true;
            this.squaNormCheckBox.Location = new System.Drawing.Point(761, 48);
            this.squaNormCheckBox.Name = "squaNormCheckBox";
            this.squaNormCheckBox.Size = new System.Drawing.Size(103, 19);
            this.squaNormCheckBox.TabIndex = 16;
            this.squaNormCheckBox.Text = "N(0,1) squared";
            this.squaNormCheckBox.UseVisualStyleBackColor = true;
            this.squaNormCheckBox.CheckedChanged += new System.EventHandler(this.checkedChanged);
            // 
            // squasquaNormCheckBox
            // 
            this.squasquaNormCheckBox.AutoSize = true;
            this.squasquaNormCheckBox.Location = new System.Drawing.Point(883, 48);
            this.squasquaNormCheckBox.Name = "squasquaNormCheckBox";
            this.squasquaNormCheckBox.Size = new System.Drawing.Size(191, 19);
            this.squasquaNormCheckBox.TabIndex = 17;
            this.squasquaNormCheckBox.Text = "N(0,1) squared / N(0,1) squared";
            this.squasquaNormCheckBox.UseVisualStyleBackColor = true;
            this.squasquaNormCheckBox.CheckedChanged += new System.EventHandler(this.checkedChanged);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1443, 643);
            this.Controls.Add(this.squasquaNormCheckBox);
            this.Controls.Add(this.squaNormCheckBox);
            this.Controls.Add(this.expNormCheckBox);
            this.Controls.Add(this.varNormCheckBox);
            this.Controls.Add(this.meanNormCheckBox);
            this.Controls.Add(this.normCheckBox);
            this.Controls.Add(this.curPathsNumLab);
            this.Controls.Add(this.curRvNumLab);
            this.Controls.Add(this.pathsMinLab);
            this.Controls.Add(this.minRvLab);
            this.Controls.Add(this.pathsMaxLab);
            this.Controls.Add(this.maxRvLab);
            this.Controls.Add(this.pathsLab);
            this.Controls.Add(this.rvLab);
            this.Controls.Add(this.rvBar);
            this.Controls.Add(this.pathsBar);
            this.Controls.Add(this.simulButton);
            this.Controls.Add(this.pictureBox);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pathsBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rvBar)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox;
        private System.Windows.Forms.Button simulButton;
        private System.Windows.Forms.TrackBar pathsBar;
        private System.Windows.Forms.TrackBar rvBar;
        private System.Windows.Forms.Label rvLab;
        private System.Windows.Forms.Label pathsLab;
        private System.Windows.Forms.Label maxRvLab;
        private System.Windows.Forms.Label pathsMaxLab;
        private System.Windows.Forms.Label minRvLab;
        private System.Windows.Forms.Label pathsMinLab;
        private System.Windows.Forms.Label curRvNumLab;
        private System.Windows.Forms.Label curPathsNumLab;
        private System.Windows.Forms.CheckBox normCheckBox;
        private System.Windows.Forms.CheckBox meanNormCheckBox;
        private System.Windows.Forms.CheckBox varNormCheckBox;
        private System.Windows.Forms.CheckBox expNormCheckBox;
        private System.Windows.Forms.CheckBox squaNormCheckBox;
        private System.Windows.Forms.CheckBox squasquaNormCheckBox;
    }
}

