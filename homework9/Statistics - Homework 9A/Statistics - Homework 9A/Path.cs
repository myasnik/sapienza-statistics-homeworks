﻿using System;
using System.Collections.Generic;
using System.Drawing;

namespace Statistics___Homework_9A
{
    class Path
    {
        private IRandVar var;
        private List<double> calculatedSequence;
        private List<double> calculatedPath;
        private double tot = 0;

        public Path(IRandVar var, int steps)
        {
            Var = var;
            CalculatedSequence = new List<double>();
            CalculatedPath = new List<double>();
            for (int i = 0; i < steps; i++)
            {
                double sample = Var.GetSample();
                CalculatedSequence.Add(sample);
                tot += Var.StdDev * Math.Sqrt(1.0 / steps) * sample;
                CalculatedPath.Add(tot);
            }
        }

        public List<double> CalculatedSequence { get => calculatedSequence; private set => calculatedSequence = value; }
        public List<double> CalculatedPath { get => calculatedPath; private set => calculatedPath = value; }
        public double Positive { get => tot; private set => tot = value; }
        internal IRandVar Var { get => var; set => var = value; }

        public void DrawPath(ViewPort view, Color color)
        {
            List<PointF> points = new List<PointF>();
            for (int i = 0; i < CalculatedPath.Count; i++)
            {
                points.Add(new PointF(i, (float)CalculatedPath[i]));
            }
            view.DrawCurve(color, points);
        }
    }
}
