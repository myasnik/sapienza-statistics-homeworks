﻿using System.Collections.Generic;
using System.Drawing;

namespace Statistics___Homework_9A
{
    class ViewPort
    {
        private Graphics graph;
        private RectangleF viewPortArea;
        private double xScale;
        private double yScale;
        private double minX;
        private double maxX;
        private double minY;
        private double maxY;

        public ViewPort(Graphics graph, RectangleF viewPortArea, double minX, double maxX, double minY, double maxY)
        {
            Graph = graph;
            ViewPortArea = viewPortArea;
            MinX = minX;
            MaxX = maxX;
            MinY = minY;
            MaxY = maxY;
            UpdateScale();
        }

        public Graphics Graph { get => graph; private set => graph = value; }
        public RectangleF ViewPortArea { get => viewPortArea; private set => viewPortArea = value; }
        public double XScale { get => xScale; private set => xScale = value; }
        public double YScale { get => yScale; private set => yScale = value; }
        public double MinX { get => minX; private set => minX = value; }
        public double MaxX { get => maxX; private set => maxX = value; }
        public double MinY { get => minY; private set => minY = value; }
        public double MaxY { get => maxY; private set => maxY = value; }

        public void FillEllipse(int x, int y, Color color, Size size)
        {
            Graph.FillEllipse(new SolidBrush(color), new Rectangle(TransformPoint(new Point(x, y)), ScaleSize(size)));
        }

        public void DrawPoint(int x, int y, Color color, Size size)
        {
            Graph.FillEllipse(new SolidBrush(color), new Rectangle(TransformPoint(new Point(x, y)), size));
        }

        public void DrawString(string s, Color color, Point point, string fontFamily, int fontSize, StringFormatFlags orientation = StringFormatFlags.NoClip)
        {
            Size finalStringSize = ScaleSize(Graph.MeasureString(s, new Font(fontFamily, fontSize)).ToSize());
            Size stringSize = Graph.MeasureString(s, new Font(fontFamily, fontSize)).ToSize();
            while (fontSize > 1 && (finalStringSize.Width < stringSize.Width && finalStringSize.Height < stringSize.Height))
            {
                fontSize--;
                stringSize = Graph.MeasureString(s, new Font(fontFamily, fontSize)).ToSize();
            }
            if (orientation == StringFormatFlags.NoClip)
            {
                Graph.DrawString(s, new Font(fontFamily, fontSize), new SolidBrush(color), TransformPoint(point));
            }
            else
            {
                Graph.DrawString(s, new Font(fontFamily, fontSize), new SolidBrush(color), TransformPoint(point), new StringFormat(orientation));
            }
        }

        public void DrawString(string s, Color color, PointF point, string fontFamily, int fontSize, StringFormatFlags orientation = StringFormatFlags.NoClip)
        {
            SizeF finalStringSize = ScaleSize(Graph.MeasureString(s, new Font(fontFamily, fontSize)));
            SizeF stringSize = Graph.MeasureString(s, new Font(fontFamily, fontSize));
            while (fontSize > 1 && (finalStringSize.Width < stringSize.Width && finalStringSize.Height < stringSize.Height))
            {
                fontSize--;
                stringSize = Graph.MeasureString(s, new Font(fontFamily, fontSize));
            }
            if (orientation == StringFormatFlags.NoClip)
            {
                Graph.DrawString(s, new Font(fontFamily, fontSize), new SolidBrush(color), TransformPoint(point));
            } else
            {
                Graph.DrawString(s, new Font(fontFamily, fontSize), new SolidBrush(color), TransformPoint(point), new StringFormat(orientation));
            }
        }

        public void DrawRectangle(Color color, Rectangle r)
        {
            Graph.DrawRectangle(new Pen(new SolidBrush(color)), TransformRectangle(r));
        }

        public void DrawLine(Color color, Point start, Point end)
        {
            Graph.DrawLine(new Pen(new SolidBrush(color)), TransformPoint(start), TransformPoint(end));
        }

        public void DrawLine(Color color, PointF start, PointF end)
        {
            Graph.DrawLine(new Pen(new SolidBrush(color)), TransformPoint(start), TransformPoint(end));
        }

        public void FillRectangle(Color color, Rectangle r)
        {
            Graph.FillRectangle(new SolidBrush(color), TransformRectangle(r));
        }

        public void FillRectangle(Color color, RectangleF r)
        {
            Graph.FillRectangle(new SolidBrush(color), TransformRectangle(r));
        }

        public void DrawCurve(Color color, List<PointF> points)
        {
            List<PointF> transformedPoints = new List<PointF>();
            foreach (PointF p in points)
            {
                transformedPoints.Add(TransformPoint(p));
            }
            Graph.DrawCurve(new Pen(new SolidBrush(color)), transformedPoints.ToArray());
        }

        private PointF TransformPoint(PointF point)
        {
            return new PointF((float)(ViewPortArea.X + ((point.X - MinX) * XScale)),
                (float)(ViewPortArea.Y + ViewPortArea.Height - ((point.Y - MinY) * YScale)));
        }

        private Point TransformPoint(Point point)
        {
            return new Point((int)(ViewPortArea.X + ((point.X - MinX) * XScale)),
                (int)(ViewPortArea.Y + ViewPortArea.Height - ((point.Y - MinY) * YScale)));
        }

        private Rectangle TransformRectangle(Rectangle r)
        {
            return new Rectangle(TransformPoint(r.Location), ScaleSize(new Size(r.Width, r.Height)));
        }

        private RectangleF TransformRectangle(RectangleF r)
        {
            return new RectangleF(TransformPoint(r.Location), ScaleSize(new SizeF(r.Width, r.Height)));
        }

        public Size ScaleSize(Size size)
        {
            return new Size((int)(size.Width * XScale), (int)(size.Height * YScale));
        }

        public SizeF ScaleSize(SizeF size)
        {
            return new SizeF((float)(size.Width * XScale), (float)(size.Height * YScale));
        }

        public void Move(PointF dest)
        {
            ViewPortArea = new RectangleF(dest.X, dest.Y, ViewPortArea.Width, ViewPortArea.Height);
        }

        public void Scale(PointF init, PointF end)
        {
            ViewPortArea = new RectangleF(ViewPortArea.X, ViewPortArea.Y, ViewPortArea.Width + end.X - init.X,
                ViewPortArea.Height + end.Y - init.Y);
            UpdateScale();
        }

        private void UpdateScale()
        {
            XScale = ViewPortArea.Width / (MaxX - MinX);
            YScale = ViewPortArea.Height / (MaxY - MinY);
        }
    }
}
