﻿namespace Statistics___Homework_9A
{
    interface IRandVar
    {
        public string ToString();
        public double GetSample();

        double Mean { get; }
        double StdDev { get; }
        double Variance { get; }
    }
}
