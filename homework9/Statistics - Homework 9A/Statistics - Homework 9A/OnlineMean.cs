﻿namespace Statistics___Homework_9A
{
    class OnlineMean
    {
        private double currentAvg = 0;
        private int index = 0;

        public double CurrentAvg { get => currentAvg; private set => currentAvg = value; }
        public int Index { get => index; private set => index = value; }

        public void AddValue(double val)
        {
            Index++;
            CurrentAvg += ((val - CurrentAvg) / Index);
        }
    }
}