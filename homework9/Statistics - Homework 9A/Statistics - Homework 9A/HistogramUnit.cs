﻿using System;
using System.Drawing;

namespace Statistics___Homework_9A
{
    class HistogramUnit
    {
        private Color color;
        private Interval interval;

        public HistogramUnit(Color color, Interval interval)
        {
            Color = color;
            Interval = interval;
        }

        public Color Color { get => color; private set => color = value; }
        internal Interval Interval { get => interval; private set => interval = value; }

        public override string ToString()
        {
            string s = "";
            if (Interval.LeftComprehend) s += "["; else s += "(";
            s += Math.Round(Interval.LowerEnd, 3) + "," + Math.Round(Interval.UpperEnd, 3);
            if (Interval.RightComprehend) s += "]"; else s += ")";
            s += " " + Interval.Frequency;
            return s;
        }
    }
}
