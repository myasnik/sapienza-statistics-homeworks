﻿using System.Collections.Generic;
using System.Drawing;

namespace Statistics___Homework_5
{
    class ScatterPlot
    {
        private List<ScatterUnit> points;
        private Size pointSize;

        public ScatterPlot(List<ScatterUnit> points, Size pointSize)
        {
            Points = points;
            PointSize = pointSize;
        }

        internal List<ScatterUnit> Points { get => points; private set => points = value; }
        public Size PointSize { get => pointSize; private set => pointSize = value; }

        public void AddPoint(ScatterUnit point)
        {
            Points.Add(point);
        }

        public void DrawPlot(ViewPort view)
        {
            foreach (ScatterUnit u in points)
            {
                view.DrawPoint(u.Point.X, u.Point.Y, u.Color, PointSize);
            }
            foreach (ScatterUnit u in points)
            {
                view.DrawString(u.ToString(), Color.Black, new Point(u.Point.X, u.Point.Y), "arial", 8);
            }
        }
    }
}
