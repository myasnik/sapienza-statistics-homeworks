﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace Statistics___Homework_5
{
    public partial class Form1 : Form
    {
        Graphics g;
        ViewPort vp;

        private Point firstPointDrag;

        CSVParser treeCSV;
        CSVParser cityCSV;

        List<double> x;
        List<double> y;

        public Form1()
        {
            InitializeComponent();
            SetStyle(ControlStyles.OptimizedDoubleBuffer | ControlStyles.AllPaintingInWmPaint | ControlStyles.UserPaint, true);
            UpdateStyles();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            pictureBox.Image = new Bitmap(pictureBox.Width, pictureBox.Height);
            g = Graphics.FromImage(pictureBox.Image);

            vp = new ViewPort(g, new Rectangle(0, 0, pictureBox.Width, pictureBox.Height), 5, 25, 5, 90);

            treeCSV = new CSVParser("trees.csv", typeof(Tree), true);
            cityCSV = new CSVParser("cities.csv", typeof(City), true);
            x = new List<double>();
            y = new List<double>();

            foreach (Tree t in treeCSV.Data)
            {
                x.Add(t.girth);
                y.Add(t.volume);
            }
            //foreach (City c in cityCSV.Data)
            //{
            //    x.Add(c.latD);
            //    y.Add(c.latM);
            //}

            Draw();
        }

        private void pictureBox_MouseDown(object sender, MouseEventArgs e)
        {
            if ((e.Button == MouseButtons.Left || e.Button == MouseButtons.Right) &&
                vp.ViewPortArea.Contains(e.Location))
            {
                firstPointDrag = e.Location;
            }
            else
            {
                firstPointDrag = new Point();
            }
        }

        private void pictureBox_MouseMove(object sender, MouseEventArgs e)
        {
            if (!firstPointDrag.IsEmpty)
            {
                Point tempPoint = e.Location;
                Point resPoint = new Point(firstPointDrag.X - tempPoint.X, firstPointDrag.Y - tempPoint.Y);
                if (e.Button == MouseButtons.Left)
                {
                    vp.Move(new Point(vp.ViewPortArea.Location.X - resPoint.X,
                        vp.ViewPortArea.Location.Y - resPoint.Y));
                }
                else if (e.Button == MouseButtons.Right)
                {
                    vp.Scale(firstPointDrag, tempPoint);
                }
                firstPointDrag = tempPoint;
                Draw();
            }
        }

        private void Draw()
        {
            ScatterPlot sp = new ScatterPlot(new List<ScatterUnit>(), new Size(7, 7));
            RegressionLine rInv = new RegressionLine(x, y, Color.Red, true);
            RegressionLineX rNorm = new RegressionLineX(new StandardDeviation(x), new StandardDeviation(y), Color.Blue);
            foreach (Tree t in treeCSV.Data)
            {
                sp.AddPoint(new ScatterUnit(Color.Green, new Point((int)t.girth, (int)t.volume)));
            }

            //foreach (City c in cityCSV.Data)
            //{
            //    sp.AddPoint(new ScatterUnit(Color.Green, new Point(c.latD, c.latM)));
            //}

            g.Clear(BackColor);
            g.DrawRectangle(Pens.Orange, vp.ViewPortArea);
            sp.DrawPlot(vp);
            rInv.DrawRegressionLine(vp);
            rNorm.DrawRegressionLine(vp);
            pictureBox.Refresh();
        }
    }

}
