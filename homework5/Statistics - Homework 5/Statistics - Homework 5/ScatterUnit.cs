﻿using System.Drawing;

namespace Statistics___Homework_5
{
    class ScatterUnit
    {
        private Color color;
        private Point point;

        public ScatterUnit(Color color, Point point)
        {
            Color = color;
            Point = point;
        }

        public Color Color { get => color; private set => color = value; }
        public Point Point { get => point; private set => point = value; }

        public override string ToString()
        {
            return Point.X + "," + Point.Y;
        }
    }
}
