﻿using System.Drawing;

namespace Statistics___Homework_5
{
    class ViewPort
    {
        private Graphics graph;
        private Rectangle viewPortArea;
        private double xScale;
        private double yScale;
        private int minX;
        private int maxX;
        private int minY;
        private int maxY;

        public ViewPort(Graphics graph, Rectangle viewPortArea, int minX, int maxX, int minY, int maxY)
        {
            Graph = graph;
            ViewPortArea = viewPortArea;
            MinX = minX;
            MaxX = maxX;
            MinY = minY;
            MaxY = maxY;
            UpdateScale();
        }

        public Graphics Graph { get => graph; private set => graph = value; }
        public Rectangle ViewPortArea { get => viewPortArea; private set => viewPortArea = value; }
        public double XScale { get => xScale; private set => xScale = value; }
        public double YScale { get => yScale; private set => yScale = value; }
        public int MinX { get => minX; private set => minX = value; }
        public int MaxX { get => maxX; private set => maxX = value; }
        public int MinY { get => minY; private set => minY = value; }
        public int MaxY { get => maxY; private set => maxY = value; }

        public void FillEllipse(int x, int y, Color color, Size size)
        {
            Graph.FillEllipse(new SolidBrush(color), new Rectangle(TransformPoint(new Point(x, y)), ScaleSize(size)));
        }

        public void DrawPoint(int x, int y, Color color, Size size)
        {
            Graph.FillEllipse(new SolidBrush(color), new Rectangle(TransformPoint(new Point(x, y)), size));
        }

        public void DrawString(string s, Color color, Point point, string fontFamily, int fontSize)
        {
            Size finalStringSize = ScaleSize(Graph.MeasureString(s, new Font(fontFamily, fontSize)).ToSize());
            Size stringSize = Graph.MeasureString(s, new Font(fontFamily, fontSize)).ToSize();
            while (fontSize > 1 && (finalStringSize.Width < stringSize.Width && finalStringSize.Height < stringSize.Height))
            {
                fontSize--;
                stringSize = Graph.MeasureString(s, new Font(fontFamily, fontSize)).ToSize();
            }
            Graph.DrawString(s, new Font(fontFamily, fontSize), new SolidBrush(color), TransformPoint(point));
        }

        public void DrawRectangle(Color color, Rectangle r)
        {
            Graph.DrawRectangle(new Pen(new SolidBrush(color)), TransformRectangle(r));
        }

        public void DrawLine(Color color, Point start, Point end)
        {
            Graph.DrawLine(new Pen(new SolidBrush(color)), TransformPoint(start), TransformPoint(end));
        }

        public void FillRectangle(Color color, Rectangle r)
        {
            Graph.FillRectangle(new SolidBrush(color), TransformRectangle(r));
        }

        private Point TransformPoint(Point point)
        {
            return new Point((int)(ViewPortArea.X + ((point.X - MinX) * XScale)),
                (int)(ViewPortArea.Y + ViewPortArea.Height - ((point.Y - MinY) * YScale)));
        }

        private Rectangle TransformRectangle(Rectangle r)
        {
            return new Rectangle(TransformPoint(r.Location), new Size((int)(r.Width * XScale), (int)(r.Height * YScale)));
        }

        public Size ScaleSize(Size size)
        {
            return new Size((int)(size.Width * XScale), (int)(size.Height * YScale));
        }

        public void Move(Point dest)
        {
            ViewPortArea = new Rectangle(dest.X, dest.Y, ViewPortArea.Width, ViewPortArea.Height);
        }

        public void Scale(Point init, Point end)
        {
            ViewPortArea = new Rectangle(ViewPortArea.X, ViewPortArea.Y, ViewPortArea.Width + end.X - init.X,
                ViewPortArea.Height + end.Y - init.Y);
            UpdateScale();
        }

        private void UpdateScale()
        {
            XScale = ViewPortArea.Width / (MaxX - MinX);
            YScale = ViewPortArea.Height / (MaxY - MinY);
        }
    }
}
