﻿using System;
using System.Drawing;

namespace Statistics___Homework_5
{
    class RegressionLineX
    {
        private StandardDeviation xStDev;
        private StandardDeviation yStDev;
        private double correlation = 0;
        private double slope;
        private double yIntercept;
        private Color color;

        public RegressionLineX(StandardDeviation xStDev, StandardDeviation yStDev, Color color)
        {
            XStDev = xStDev;
            YStDev = yStDev;
            Color = color;

            if (XStDev.Data.Count != YStDev.Data.Count) throw new Exception("Standard regression data lengths can't be different");

            // Calculate correlation
            for (int i = 0; i < XStDev.Data.Count; i++)
            {
                Correlation += (XStDev.Data[i] - XStDev.Mean.CurrentAvg) * (YStDev.Data[i] - YStDev.Mean.CurrentAvg);
            }
            Correlation = Correlation / (XStDev.StdDeviation * YStDev.StdDeviation);
            Correlation = Correlation / (XStDev.Data.Count - 1);

            // Calculate slope
            Slope = Correlation * (YStDev.StdDeviation / XStDev.StdDeviation);

            // Calculate YIntercept
            YIntercept = YStDev.Mean.CurrentAvg - (Slope * XStDev.Mean.CurrentAvg);
        }

        public double Correlation { get => correlation; private set => correlation = value; }
        public double Slope { get => slope; private set => slope = value; }
        public double YIntercept { get => yIntercept; private set => yIntercept = value; }
        public Color Color { get => color; private set => color = value; }
        internal StandardDeviation XStDev { get => xStDev; private set => xStDev = value; }
        internal StandardDeviation YStDev { get => yStDev; private set => yStDev = value; }

        public void DrawRegressionLine(ViewPort view)
        {
            Point start = new Point((int)((view.MinY - YIntercept)/Slope), view.MinY);
            Point end = new Point(view.MaxX, (int)(Slope * view.MaxX) + (int)YIntercept);
            view.DrawLine(Color, start, end);
        }
    }
}
