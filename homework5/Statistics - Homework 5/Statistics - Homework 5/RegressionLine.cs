﻿using System;
using System.Collections.Generic;
using System.Drawing;

namespace Statistics___Homework_5
{
    class RegressionLine
    {
        private List<double> x;
        private List<double> y;
        private OnlineMean meanX;
        private OnlineMean meanY;
        private OnlineMean meanXY;
        private OnlineMean meanSquared;
        private double cov;
        private double var;
        private Color color;
        private bool inverted;

        public RegressionLine(List<double> x, List<double> y, Color color, bool inverted)
        {
            X = x;
            Y = y;
            Color = color;
            Inverted = inverted;

            MeanX = new OnlineMean();
            MeanY = new OnlineMean();
            MeanXY = new OnlineMean();
            MeanSquared = new OnlineMean();

            if (X.Count != Y.Count) throw new Exception("Standard regression data lengths can't be different");

            // Calculate cov
            for (int i = 0; i < X.Count; i++)
            {
                MeanX.AddValue(X[i]);
                MeanY.AddValue(Y[i]);
                MeanXY.AddValue(X[i] * Y[i]);
                if (Inverted) MeanSquared.AddValue(Y[i] * Y[i]);
                else MeanSquared.AddValue(X[i] * X[i]);
            }
            Cov = MeanXY.CurrentAvg - (MeanX.CurrentAvg * MeanY.CurrentAvg);

            // Calculate var
            Var = Inverted ? MeanSquared.CurrentAvg - MeanY.CurrentAvg : MeanSquared.CurrentAvg - MeanX.CurrentAvg;
        }

        public Color Color { get => color; private set => color = value; }
        public double Cov { get => cov; private set => cov = value; }
        public double Var { get => var; private set => var = value; }
        public List<double> X { get => x; private set => x = value; }
        public List<double> Y { get => y; private set => y = value; }
        public bool Inverted { get => inverted; private set => inverted = value; }
        internal OnlineMean MeanX { get => meanX; private set => meanX = value; }
        internal OnlineMean MeanY { get => meanY; private set => meanY = value; }
        internal OnlineMean MeanXY { get => meanXY; private set => meanXY = value; }
        internal OnlineMean MeanSquared { get => meanSquared; private set => meanSquared = value; }

        public void DrawRegressionLine(ViewPort view)
        {
            Point start = new Point(GetXGivenY(view.MinY), view.MinY);
            Point end = new Point(GetXGivenY(view.MaxY), view.MaxY);
            view.DrawLine(Color, start, end);
        }

        private int GetXGivenY(int y)
        {
            return Inverted ? (int)(((Cov / Var) * (y - MeanY.CurrentAvg)) + MeanX.CurrentAvg) :
                (int)((Var / Cov) * (y - MeanY.CurrentAvg + ((Cov / Var) * MeanX.CurrentAvg)));
        }

        private int GetYGivenX(int x)
        {
            return Inverted ? (int)((Var / Cov) * (x - MeanX.CurrentAvg + ((Cov / Var) * MeanY.CurrentAvg))) :
                (int)(((Cov / Var) * (x - MeanX.CurrentAvg)) + MeanY.CurrentAvg);
        }
    }
}
