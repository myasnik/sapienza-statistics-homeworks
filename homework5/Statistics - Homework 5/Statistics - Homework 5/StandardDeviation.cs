﻿using System.Collections.Generic;
using static System.Math;

namespace Statistics___Homework_5
{
    class StandardDeviation
    {
        private List<double> data;
        private OnlineMean mean;
        private double stdDeviation = 0;

        public StandardDeviation(List<int> data)
        {
            Data = data.ConvertAll(x => (double)x);
            computeMean();
            computeDeviation();
        }

        public StandardDeviation(List<double> data)
        {
            Data = data;
            computeMean();
            computeDeviation();
        }

        public double StdDeviation { get => stdDeviation; private set => stdDeviation = value; }
        public List<double> Data { get => data; private set => data = value; }
        internal OnlineMean Mean { get => mean; private set => mean = value; }

        private void computeDeviation()
        {
            foreach (double e in Data)
            {
                stdDeviation += Pow((e - Mean.CurrentAvg), 2);
            }
            stdDeviation = Sqrt(stdDeviation / Data.Count);
        }

        private void computeMean()
        {
            Mean = new OnlineMean();
            foreach (double e in Data)
            {
                mean.AddValue(e);
            }
        }
    }
}
