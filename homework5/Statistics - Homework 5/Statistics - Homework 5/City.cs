﻿namespace Statistics___Homework_5
{
    class City
    {
        public int latD;
        public int latM;
        public int latS;
        public string ns;
        public int lonD;
        public int lonM;
        public int lonS;
        public string ew;
        public string cityName;
        public string stateName;

        public City()
        {
            this.latD = 0;
            this.latM = 0;
            this.latS = 0;
            this.ns = "";
            this.lonD = 0;
            this.lonM = 0;
            this.lonS = 0;
            this.ew = "";
            this.cityName = "";
            this.stateName = "";
        }

        public City(int latD = 0, int latM = 0, int latS = 0, string ns = null, int lonD = 0, int lonM = 0, int lonS = 0, string ew = null, string cityName = null, string stateName = null)
        {
            this.latD = latD;
            this.latM = latM;
            this.latS = latS;
            this.ns = ns;
            this.lonD = lonD;
            this.lonM = lonM;
            this.lonS = lonS;
            this.ew = ew;
            this.cityName = cityName;
            this.stateName = stateName;
        }
    }
}
