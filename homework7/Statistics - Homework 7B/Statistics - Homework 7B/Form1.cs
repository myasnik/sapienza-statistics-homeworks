﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace Statistics___Homework_7B
{
    public partial class Form1 : Form
    {
        Graphics g;
        ViewPort vp;
        ViewPort endHistVp;
        ViewPort midHistVp;

        Random rand;
        EnumPath enumPath;
        List<Path> paths;

        public Form1()
        {
            InitializeComponent();
            SetStyle(ControlStyles.OptimizedDoubleBuffer | ControlStyles.AllPaintingInWmPaint | ControlStyles.UserPaint, true);
            UpdateStyles();
        }
        private void Form1_Load(object sender, EventArgs e)
        {
            rand = new Random();
            pictureBox.Image = new Bitmap(pictureBox.Width, pictureBox.Height);
            g = Graphics.FromImage(pictureBox.Image);

            radRadio.Checked = true;
            enumPath = EnumPath.RADEMACHER;

            UpdateBars();
        }

        private void numVarsBar_ValueChanged(object sender, EventArgs e)
        {
            UpdateBars();
        }

        private void Draw()
        {
            paths = new List<Path>();

            double maxY = 1;
            double minY = 0;
            switch (enumPath)
            {
                case EnumPath.RADEMACHER:
                    for (int i = 0; i < repetitionsBar.Value; i++)
                    {
                        paths.Add(new Path(new RademacherRandomVariable(), numVarsBar.Value));
                    }
                    break;
                case EnumPath.NORMAL:
                    for (int i = 0; i < repetitionsBar.Value; i++)
                    {
                        paths.Add(new Path(new NormalRandomVariable(), numVarsBar.Value));
                    }
                    break;
            }

            foreach (Path p in paths)
            {
                foreach (double e in p.CalculatedPath)
                {
                    if (e > maxY) maxY = e;
                    if (e < minY) minY = e;
                }
            }

            vp = new ViewPort(g, new Rectangle(0, 0, pictureBox.Width, pictureBox.Height), 0, numVarsBar.Value, minY, maxY);
            endHistVp = new ViewPort(g, new RectangleF(pictureBox.Width - pictureBox.Width / 10, 0, pictureBox.Width / 10, pictureBox.Height), 0, repetitionsBar.Value, minY, maxY);
            midHistVp = new ViewPort(g, new RectangleF(histPosBar.Value * (float)vp.XScale, 0, pictureBox.Width / 10, pictureBox.Height), 0, repetitionsBar.Value, minY, maxY);
            HistogramPlot endHist = new HistogramPlot(minY, Math.Abs(maxY) + Math.Abs(minY), 15, Color.Orange, EnumOrientation.LEFTRIGHT);
            HistogramPlot midHist = new HistogramPlot(minY, Math.Abs(maxY) + Math.Abs(minY), 30, Color.Orange, EnumOrientation.LEFTRIGHT);

            for (int i = 0; i < repetitionsBar.Value; i++)
            {
                endHist.AddNumber(paths[i].CalculatedPath[paths[i].CalculatedPath.Count - 1]);
                midHist.AddNumber(paths[i].CalculatedPath[histPosBar.Value - 1]);
            }

            g.Clear(BackColor);
            foreach (Path p in paths)
            {
                p.DrawPath(vp, Color.FromArgb(rand.Next(256), rand.Next(256), rand.Next(256)));
            }
            endHist.DrawPlot(endHistVp);
            midHist.DrawPlot(midHistVp);
            pictureBox.Refresh();
        }

        private void runButton_Click(object sender, EventArgs e)
        {
            Draw();
        }

        private void radRadio_CheckedChanged(object sender, EventArgs e)
        {
            if (radRadio.Checked)
            {
                normRadio.Checked = false;
                enumPath = EnumPath.RADEMACHER;
            }
        }

        private void normRelRadio_CheckedChanged(object sender, EventArgs e)
        {
            if (normRadio.Checked)
            {
                radRadio.Checked = false;
                enumPath = EnumPath.NORMAL;
            }
        }

        private void UpdateBars()
        {
            histPosUpperBoundLabel.Text = numVarsBar.Value.ToString();
            histPosBar.Maximum = numVarsBar.Value;
            histPosBar.Value = histPosBar.Minimum;
        }
    }
}
