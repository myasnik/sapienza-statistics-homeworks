﻿using System;

namespace Statistics___Homework_7B
{
    class RademacherRandomVariable: IRandVar
    {
        Random _random;

        public double Mean => 0;

        public double StdDev => 1;

        public double Variance => StdDev * StdDev;

        public RademacherRandomVariable()
        {
            _random = new Random();
        }

        public override string ToString()
        {
            return $"Rademacher(mean = {Mean}, StDev = {StdDev})";
        }

        public double GetSample()
        {
            var r = Math.Round(_random.NextDouble(), 1);
            if (r < 0.5)
            {
                return 1;
            }
            else if (r > 0.5)
            {
                return -1;
            }
            else
            {
                return 0;
            }
        }
    }
}
