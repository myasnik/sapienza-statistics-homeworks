﻿using System;

namespace Statistics___Homework_7B
{
    // Source: https://github.com/mathnet/mathnet-numerics/blob/master/src/Numerics/Distributions/Normal.cs
    class NormalRandomVariable: IRandVar
    {
        Random _random;

        readonly double _mean;
        readonly double _stdDev;

        public NormalRandomVariable()
            : this(0.0, 1.0)
        {
        }

        public NormalRandomVariable(double mean, double stddev)
        {
            if (!IsValidParameterSet(mean, stddev))
            {
                throw new ArgumentException("Invalid parametrization for the distribution.");
            }

            _random = new Random();
            _mean = mean;
            _stdDev = stddev;
        }

        public override string ToString()
        {
            return $"Normal(μ = {_mean}, σ = {_stdDev})";
        }

        public static bool IsValidParameterSet(double mean, double stddev)
        {
            return stddev >= 0.0 && !double.IsNaN(mean);
        }

        public double Mean => _mean;

        public double StdDev => _stdDev;

        public double Variance => _stdDev * _stdDev;

        public double GetSample()
        {
            return SampleUnchecked(_random, _mean, _stdDev);
        }

        internal static double SampleUnchecked(Random rnd, double mean, double stddev)
        {
            double x;
            while (!PolarTransform(rnd.NextDouble(), rnd.NextDouble(), out x, out _))
            {
            }

            return mean + (stddev * x);
        }

        static bool PolarTransform(double a, double b, out double x, out double y)
        {
            var v1 = (2.0 * a) - 1.0;
            var v2 = (2.0 * b) - 1.0;
            var r = (v1 * v1) + (v2 * v2);
            if (r >= 1.0 || r == 0.0)
            {
                x = 0;
                y = 0;
                return false;
            }

            var fac = Math.Sqrt(-2.0 * Math.Log(r) / r);
            x = v1 * fac;
            y = v2 * fac;
            return true;
        }
    }
}
