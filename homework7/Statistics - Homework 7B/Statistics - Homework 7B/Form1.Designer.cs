﻿
namespace Statistics___Homework_7B
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.numVarsBar = new System.Windows.Forms.TrackBar();
            this.varLowerBoundLabel = new System.Windows.Forms.Label();
            this.varNumLabel = new System.Windows.Forms.Label();
            this.varUpperBoundLabel = new System.Windows.Forms.Label();
            this.upperBoundRepLabel = new System.Windows.Forms.Label();
            this.repetitions = new System.Windows.Forms.Label();
            this.lowerBoundRepLabel = new System.Windows.Forms.Label();
            this.repetitionsBar = new System.Windows.Forms.TrackBar();
            this.histPosUpperBoundLabel = new System.Windows.Forms.Label();
            this.histPosLabel = new System.Windows.Forms.Label();
            this.histPosLowerBoundLabel = new System.Windows.Forms.Label();
            this.histPosBar = new System.Windows.Forms.TrackBar();
            this.pictureBox = new System.Windows.Forms.PictureBox();
            this.runButton = new System.Windows.Forms.Button();
            this.radRadio = new System.Windows.Forms.RadioButton();
            this.normRadio = new System.Windows.Forms.RadioButton();
            ((System.ComponentModel.ISupportInitialize)(this.numVarsBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repetitionsBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.histPosBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // numVarsBar
            // 
            this.numVarsBar.Location = new System.Drawing.Point(66, 46);
            this.numVarsBar.Maximum = 10000;
            this.numVarsBar.Minimum = 100;
            this.numVarsBar.Name = "numVarsBar";
            this.numVarsBar.Size = new System.Drawing.Size(179, 45);
            this.numVarsBar.TabIndex = 0;
            this.numVarsBar.Value = 100;
            this.numVarsBar.ValueChanged += new System.EventHandler(this.numVarsBar_ValueChanged);
            // 
            // varLowerBoundLabel
            // 
            this.varLowerBoundLabel.AutoSize = true;
            this.varLowerBoundLabel.Location = new System.Drawing.Point(66, 76);
            this.varLowerBoundLabel.Name = "varLowerBoundLabel";
            this.varLowerBoundLabel.Size = new System.Drawing.Size(25, 15);
            this.varLowerBoundLabel.TabIndex = 1;
            this.varLowerBoundLabel.Text = "100";
            // 
            // varNumLabel
            // 
            this.varNumLabel.AutoSize = true;
            this.varNumLabel.Location = new System.Drawing.Point(97, 24);
            this.varNumLabel.Name = "varNumLabel";
            this.varNumLabel.Size = new System.Drawing.Size(114, 15);
            this.varNumLabel.TabIndex = 2;
            this.varNumLabel.Text = "Number of variables";
            // 
            // varUpperBoundLabel
            // 
            this.varUpperBoundLabel.AutoSize = true;
            this.varUpperBoundLabel.Location = new System.Drawing.Point(214, 75);
            this.varUpperBoundLabel.Name = "varUpperBoundLabel";
            this.varUpperBoundLabel.Size = new System.Drawing.Size(37, 15);
            this.varUpperBoundLabel.TabIndex = 3;
            this.varUpperBoundLabel.Text = "10000";
            // 
            // upperBoundRepLabel
            // 
            this.upperBoundRepLabel.AutoSize = true;
            this.upperBoundRepLabel.Location = new System.Drawing.Point(477, 75);
            this.upperBoundRepLabel.Name = "upperBoundRepLabel";
            this.upperBoundRepLabel.Size = new System.Drawing.Size(31, 15);
            this.upperBoundRepLabel.TabIndex = 7;
            this.upperBoundRepLabel.Text = "3000";
            // 
            // repetitions
            // 
            this.repetitions.AutoSize = true;
            this.repetitions.Location = new System.Drawing.Point(391, 24);
            this.repetitions.Name = "repetitions";
            this.repetitions.Size = new System.Drawing.Size(66, 15);
            this.repetitions.TabIndex = 6;
            this.repetitions.Text = "Repetitions";
            // 
            // lowerBoundRepLabel
            // 
            this.lowerBoundRepLabel.AutoSize = true;
            this.lowerBoundRepLabel.Location = new System.Drawing.Point(329, 76);
            this.lowerBoundRepLabel.Name = "lowerBoundRepLabel";
            this.lowerBoundRepLabel.Size = new System.Drawing.Size(25, 15);
            this.lowerBoundRepLabel.TabIndex = 5;
            this.lowerBoundRepLabel.Text = "100";
            // 
            // repetitionsBar
            // 
            this.repetitionsBar.Location = new System.Drawing.Point(329, 46);
            this.repetitionsBar.Maximum = 3000;
            this.repetitionsBar.Minimum = 100;
            this.repetitionsBar.Name = "repetitionsBar";
            this.repetitionsBar.Size = new System.Drawing.Size(179, 45);
            this.repetitionsBar.TabIndex = 4;
            this.repetitionsBar.Value = 100;
            // 
            // histPosUpperBoundLabel
            // 
            this.histPosUpperBoundLabel.AutoSize = true;
            this.histPosUpperBoundLabel.Location = new System.Drawing.Point(738, 74);
            this.histPosUpperBoundLabel.Name = "histPosUpperBoundLabel";
            this.histPosUpperBoundLabel.Size = new System.Drawing.Size(31, 15);
            this.histPosUpperBoundLabel.TabIndex = 11;
            this.histPosUpperBoundLabel.Text = "3000";
            // 
            // histPosLabel
            // 
            this.histPosLabel.AutoSize = true;
            this.histPosLabel.Location = new System.Drawing.Point(621, 23);
            this.histPosLabel.Name = "histPosLabel";
            this.histPosLabel.Size = new System.Drawing.Size(109, 15);
            this.histPosLabel.TabIndex = 10;
            this.histPosLabel.Text = "Histogram position";
            // 
            // histPosLowerBoundLabel
            // 
            this.histPosLowerBoundLabel.AutoSize = true;
            this.histPosLowerBoundLabel.Location = new System.Drawing.Point(590, 75);
            this.histPosLowerBoundLabel.Name = "histPosLowerBoundLabel";
            this.histPosLowerBoundLabel.Size = new System.Drawing.Size(19, 15);
            this.histPosLowerBoundLabel.TabIndex = 9;
            this.histPosLowerBoundLabel.Text = "10";
            // 
            // histPosBar
            // 
            this.histPosBar.Location = new System.Drawing.Point(590, 45);
            this.histPosBar.Maximum = 3000;
            this.histPosBar.Minimum = 10;
            this.histPosBar.Name = "histPosBar";
            this.histPosBar.Size = new System.Drawing.Size(179, 45);
            this.histPosBar.TabIndex = 8;
            this.histPosBar.Value = 10;
            // 
            // pictureBox
            // 
            this.pictureBox.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBox.Location = new System.Drawing.Point(13, 113);
            this.pictureBox.Name = "pictureBox";
            this.pictureBox.Size = new System.Drawing.Size(1336, 586);
            this.pictureBox.TabIndex = 12;
            this.pictureBox.TabStop = false;
            // 
            // runButton
            // 
            this.runButton.Location = new System.Drawing.Point(1135, 24);
            this.runButton.Name = "runButton";
            this.runButton.Size = new System.Drawing.Size(175, 65);
            this.runButton.TabIndex = 13;
            this.runButton.Text = "Run simulation";
            this.runButton.UseVisualStyleBackColor = true;
            this.runButton.Click += new System.EventHandler(this.runButton_Click);
            // 
            // radRadio
            // 
            this.radRadio.AutoSize = true;
            this.radRadio.Location = new System.Drawing.Point(859, 45);
            this.radRadio.Name = "radRadio";
            this.radRadio.Size = new System.Drawing.Size(91, 19);
            this.radRadio.TabIndex = 14;
            this.radRadio.TabStop = true;
            this.radRadio.Text = "Rademacher";
            this.radRadio.UseVisualStyleBackColor = true;
            this.radRadio.CheckedChanged += new System.EventHandler(this.radRadio_CheckedChanged);
            // 
            // normRadio
            // 
            this.normRadio.AutoSize = true;
            this.normRadio.Location = new System.Drawing.Point(1002, 45);
            this.normRadio.Name = "normRadio";
            this.normRadio.Size = new System.Drawing.Size(65, 19);
            this.normRadio.TabIndex = 16;
            this.normRadio.TabStop = true;
            this.normRadio.Text = "Normal";
            this.normRadio.UseVisualStyleBackColor = true;
            this.normRadio.CheckedChanged += new System.EventHandler(this.normRelRadio_CheckedChanged);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1361, 711);
            this.Controls.Add(this.normRadio);
            this.Controls.Add(this.radRadio);
            this.Controls.Add(this.runButton);
            this.Controls.Add(this.pictureBox);
            this.Controls.Add(this.histPosUpperBoundLabel);
            this.Controls.Add(this.histPosLabel);
            this.Controls.Add(this.histPosLowerBoundLabel);
            this.Controls.Add(this.histPosBar);
            this.Controls.Add(this.upperBoundRepLabel);
            this.Controls.Add(this.repetitions);
            this.Controls.Add(this.lowerBoundRepLabel);
            this.Controls.Add(this.repetitionsBar);
            this.Controls.Add(this.varUpperBoundLabel);
            this.Controls.Add(this.varNumLabel);
            this.Controls.Add(this.varLowerBoundLabel);
            this.Controls.Add(this.numVarsBar);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.numVarsBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repetitionsBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.histPosBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TrackBar numVarsBar;
        private System.Windows.Forms.Label varLowerBoundLabel;
        private System.Windows.Forms.Label varNumLabel;
        private System.Windows.Forms.Label varUpperBoundLabel;
        private System.Windows.Forms.Label upperBoundRepLabel;
        private System.Windows.Forms.Label repetitions;
        private System.Windows.Forms.Label lowerBoundRepLabel;
        private System.Windows.Forms.TrackBar repetitionsBar;
        private System.Windows.Forms.Label histPosUpperBoundLabel;
        private System.Windows.Forms.Label histPosLabel;
        private System.Windows.Forms.Label histPosLowerBoundLabel;
        private System.Windows.Forms.TrackBar histPosBar;
        private System.Windows.Forms.PictureBox pictureBox;
        private System.Windows.Forms.Button runButton;
        private System.Windows.Forms.RadioButton radRadio;
        private System.Windows.Forms.RadioButton normRadio;
    }
}

