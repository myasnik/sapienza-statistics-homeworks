﻿using System;

namespace Statistics___Homework_7A
{
    public class UniformRandomVariable: IRandVar
    {
        private Random _rand;
        private int lowerBound;
        private int upperBound;

        public UniformRandomVariable(int lowerBound, int upperBound)
        {
            Rand = new Random();
            LowerBound = lowerBound;
            UpperBound = upperBound;
        }

        public double GetSample()
        {
            return Rand.Next(LowerBound, UpperBound);
        }

        public Random Rand { get => _rand; private set => _rand = value; }
        public int LowerBound { get => lowerBound; private set => lowerBound = value; }
        public int UpperBound { get => upperBound; private set => upperBound = value; }

        public double Mean => UpperBound - LowerBound;

        public double StdDev => throw new NotImplementedException();

        public double Variance => throw new NotImplementedException();
    }
}
