﻿
namespace Statistics___Homework_7A
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Codice generato da Progettazione Windows Form

        /// <summary>
        /// Metodo necessario per il supporto della finestra di progettazione. Non modificare
        /// il contenuto del metodo con l'editor di codice.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.rvBar = new System.Windows.Forms.TrackBar();
            this.label1 = new System.Windows.Forms.Label();
            this.drawButton = new System.Windows.Forms.Button();
            this.pictureBox = new System.Windows.Forms.PictureBox();
            this.pBar = new System.Windows.Forms.TrackBar();
            this.label2 = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.startTimerButton = new System.Windows.Forms.Button();
            this.varLab = new System.Windows.Forms.Label();
            this.pathLab = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.rvBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pBar)).BeginInit();
            this.SuspendLayout();
            // 
            // rvBar
            // 
            this.rvBar.Location = new System.Drawing.Point(55, 42);
            this.rvBar.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.rvBar.Maximum = 500;
            this.rvBar.Minimum = 1;
            this.rvBar.Name = "rvBar";
            this.rvBar.Size = new System.Drawing.Size(348, 45);
            this.rvBar.TabIndex = 0;
            this.rvBar.TickStyle = System.Windows.Forms.TickStyle.None;
            this.rvBar.Value = 10;
            this.rvBar.MouseUp += new System.Windows.Forms.MouseEventHandler(this.rvBar_MouseUp);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(172, 15);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(101, 15);
            this.label1.TabIndex = 1;
            this.label1.Text = "Random variables";
            // 
            // drawButton
            // 
            this.drawButton.Location = new System.Drawing.Point(845, 15);
            this.drawButton.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.drawButton.Name = "drawButton";
            this.drawButton.Size = new System.Drawing.Size(263, 72);
            this.drawButton.TabIndex = 2;
            this.drawButton.Text = "Draw";
            this.drawButton.UseVisualStyleBackColor = true;
            this.drawButton.Click += new System.EventHandler(this.drawButton_Click);
            // 
            // pictureBox
            // 
            this.pictureBox.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBox.Location = new System.Drawing.Point(13, 111);
            this.pictureBox.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.pictureBox.Name = "pictureBox";
            this.pictureBox.Size = new System.Drawing.Size(1402, 665);
            this.pictureBox.TabIndex = 3;
            this.pictureBox.TabStop = false;
            // 
            // pBar
            // 
            this.pBar.Location = new System.Drawing.Point(423, 42);
            this.pBar.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.pBar.Maximum = 15000;
            this.pBar.Minimum = 1;
            this.pBar.Name = "pBar";
            this.pBar.Size = new System.Drawing.Size(330, 45);
            this.pBar.TabIndex = 4;
            this.pBar.Value = 1;
            this.pBar.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pBar_MouseUp);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(568, 15);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(36, 15);
            this.label2.TabIndex = 5;
            this.label2.Text = "Paths";
            // 
            // timer1
            // 
            this.timer1.Interval = 500;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // startTimerButton
            // 
            this.startTimerButton.Enabled = false;
            this.startTimerButton.Location = new System.Drawing.Point(1144, 15);
            this.startTimerButton.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.startTimerButton.Name = "startTimerButton";
            this.startTimerButton.Size = new System.Drawing.Size(253, 72);
            this.startTimerButton.TabIndex = 6;
            this.startTimerButton.Text = "Start simulation timer";
            this.startTimerButton.UseVisualStyleBackColor = true;
            this.startTimerButton.Click += new System.EventHandler(this.startTimerButton_Click);
            // 
            // varLab
            // 
            this.varLab.AutoSize = true;
            this.varLab.Location = new System.Drawing.Point(201, 72);
            this.varLab.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.varLab.Name = "varLab";
            this.varLab.Size = new System.Drawing.Size(38, 15);
            this.varLab.TabIndex = 7;
            this.varLab.Text = "label3";
            // 
            // pathLab
            // 
            this.pathLab.AutoSize = true;
            this.pathLab.Location = new System.Drawing.Point(568, 72);
            this.pathLab.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.pathLab.Name = "pathLab";
            this.pathLab.Size = new System.Drawing.Size(38, 15);
            this.pathLab.TabIndex = 8;
            this.pathLab.Text = "label4";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1429, 789);
            this.Controls.Add(this.pathLab);
            this.Controls.Add(this.varLab);
            this.Controls.Add(this.startTimerButton);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.pBar);
            this.Controls.Add(this.pictureBox);
            this.Controls.Add(this.drawButton);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.rvBar);
            this.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.rvBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pBar)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TrackBar rvBar;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button drawButton;
        private System.Windows.Forms.PictureBox pictureBox;
        private System.Windows.Forms.TrackBar pBar;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Button startTimerButton;
        private System.Windows.Forms.Label varLab;
        private System.Windows.Forms.Label pathLab;

    }
}

