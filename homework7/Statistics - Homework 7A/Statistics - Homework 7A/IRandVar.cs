﻿namespace Statistics___Homework_7A
{
    interface IRandVar
    {
        public string ToString();
        public double GetSample();

        double Mean { get; }
        double StdDev { get; }
        double Variance { get; }
    }
}
