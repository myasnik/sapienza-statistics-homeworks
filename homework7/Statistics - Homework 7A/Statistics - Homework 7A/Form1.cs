﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace Statistics___Homework_7A
{
    public partial class Form1 : Form
    {
        private int varsNum = 10;
        private int pathsNum = 10;

        Graphics g;
        ViewPort vp;

        private Random rand = new Random();

        public List<double> means;
        public UniformRandomVariable var;
        private List<Path> paths;
        bool timerEnabled = false;

        public Form1()
        {
            InitializeComponent();
            SetStyle(ControlStyles.OptimizedDoubleBuffer | ControlStyles.AllPaintingInWmPaint | ControlStyles.UserPaint, true);
            UpdateStyles();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            pictureBox.Image = new Bitmap(pictureBox.Width, pictureBox.Height);
            g = Graphics.FromImage(pictureBox.Image);
            
            int inf = rand.Next(0, 1000);
            int sup = rand.Next(inf, 1000);
            var = new UniformRandomVariable(inf, sup);
        }

        private void drawButton_Click(object sender, EventArgs e)
        {
            means = new List<double>();
            paths = new List<Path>();
            for (int i = 0; i < pathsNum; i++)
            {
                paths.Add(new Path(var, varsNum));
                means.Add(paths[i].Mean.CurrentAvg);
            }

            Draw();

            startTimerButton.Enabled = true;
        }

        private void rvBar_MouseUp(object sender, MouseEventArgs e)
        {
            varsNum = rvBar.Value;
            varLab.Text = varsNum.ToString();
        }

        private void pBar_MouseUp(object sender, MouseEventArgs e)
        {
            pathsNum = pBar.Value;
            pathLab.Text = pathsNum.ToString();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            int newVarsNum = (int)(varsNum * 1.1);
            pathsNum = varsNum * 10;

            if (!(newVarsNum > rvBar.Maximum || pathsNum > pBar.Maximum))
            {
                means = new List<double>();
                paths = new List<Path>();
                for (int i = 0; i < pathsNum; i++)
                {
                    paths.Add(new Path(var, newVarsNum));
                    means.Add(paths[i].Mean.CurrentAvg);
                }

                Draw();

                rvBar.Value = newVarsNum;
                varLab.Text = varsNum.ToString();
                pBar.Value = pathsNum;
                pathLab.Text = pathsNum.ToString();
            }
            else
            {
                timer1.Enabled = false;
                startTimerButton.Text = "Start simulation timer";
                timerEnabled = false;
            }
            varsNum = newVarsNum;
        }

        private void startTimerButton_Click(object sender, EventArgs e)
        {
            if (timerEnabled)
            {
                timer1.Enabled = false;
                startTimerButton.Text = "Start simulation timer";
                timerEnabled = false;
            }
            else
            {
                timer1.Enabled = true;
                startTimerButton.Text = "Stop simulation timer";
                timerEnabled = true;
            }
        }

        private void Draw()
        {
            g.Clear(BackColor);
            means.Sort();
            vp = new ViewPort(g, new RectangleF(0, 0, pictureBox.Width, pictureBox.Height), means[0], means[means.Count - 1], 0, means.Count);

            double currentY = 0;
            double newY;

            int count = 0;
            for (int i = 0; i < means.Count - 1; i++)
            {
                if (means[i] == means[i + 1])
                {
                    count++;
                    vp.DrawLine(Color.Black, new PointF((float)means[i], (float)currentY), new PointF((float)means[i + 1], (float)currentY));

                }
                else
                {
                    newY = currentY + count + 1;
                    vp.DrawLine(Color.Black, new PointF((float)means[i], (float)currentY), new PointF((float)means[i], (float)newY));
                    vp.DrawLine(Color.Black, new PointF((float)means[i], (float)newY), new PointF((float)means[i + 1], (float)newY));
                    currentY = newY;
                    count = 0;
                }
            }
            pictureBox.Refresh();
        }
    }
}
