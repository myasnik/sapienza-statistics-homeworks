﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace Statistics___Homework_6
{
    public partial class Form1 : Form
    {
        Graphics g;
        ViewPort vp;
        ViewPort endHistVp;
        ViewPort midHistVp;

        Random rand;
        EnumPathFrequency enumPathFrequency;
        List<Path> paths;

        public Form1()
        {
            InitializeComponent();
            SetStyle(ControlStyles.OptimizedDoubleBuffer | ControlStyles.AllPaintingInWmPaint | ControlStyles.UserPaint, true);
            UpdateStyles();
        }
        private void Form1_Load(object sender, EventArgs e)
        {
            rand = new Random();
            pictureBox.Image = new Bitmap(pictureBox.Width, pictureBox.Height);
            g = Graphics.FromImage(pictureBox.Image);

            relRadio.Checked = true;
            enumPathFrequency = EnumPathFrequency.RELATIVE;

            UpdateBars();
        }

        private void numVarsBar_ValueChanged(object sender, EventArgs e)
        {
            UpdateBars();
        }

        private void Draw()
        {
            paths = new List<Path>();

            double rnd = rand.NextDouble();

            for (int i = 0; i < repetitionsBar.Value; i++)
            {
                paths.Add(new Path(new Bernoulli(rnd), numVarsBar.Value, enumPathFrequency));
            }

            double maxY = 1;
            switch (enumPathFrequency)
            {
                case EnumPathFrequency.RELATIVE:
                    maxY = 1;
                    break;
                case EnumPathFrequency.ABSOLUTE:
                case EnumPathFrequency.NORMALIZED:
                    foreach (Path p in paths)
                    {
                        foreach (double e in p.CalculatedPath)
                        {
                            if (e > maxY) maxY = e;
                        }
                    }
                    break;
            }
            vp = new ViewPort(g, new Rectangle(0, 0, pictureBox.Width, pictureBox.Height), 0, numVarsBar.Value, 0, maxY);
            endHistVp = new ViewPort(g, new RectangleF(pictureBox.Width - pictureBox.Width / 10, 0, pictureBox.Width / 10, pictureBox.Height), 0, repetitionsBar.Value, 0, maxY);
            midHistVp = new ViewPort(g, new RectangleF(histPosBar.Value * (float)vp.XScale, 0, pictureBox.Width / 10, pictureBox.Height), 0, repetitionsBar.Value, 0, maxY);
            HistogramPlot endHist = new HistogramPlot(maxY, 15, Color.Orange, EnumOrientation.LEFTRIGHT);
            HistogramPlot midHist = new HistogramPlot(maxY, 30, Color.Orange, EnumOrientation.LEFTRIGHT);

            for (int i = 0; i < repetitionsBar.Value; i++)
            {
                endHist.AddNumber(paths[i].CalculatedPath[paths[i].CalculatedPath.Count - 1]);
                midHist.AddNumber(paths[i].CalculatedPath[histPosBar.Value - 1]);
            }

            g.Clear(BackColor);
            foreach (Path p in paths)
            {
                p.DrawPath(vp, Color.FromArgb(rand.Next(256), rand.Next(256), rand.Next(256)));
            }
            endHist.DrawPlot(endHistVp);
            midHist.DrawPlot(midHistVp);
            pictureBox.Refresh();
        }

        private void runButton_Click(object sender, EventArgs e)
        {
            Draw();
        }

        private void relRadio_CheckedChanged(object sender, EventArgs e)
        {
            if (relRadio.Checked)
            {
                absRadio.Checked = false;
                normRadio.Checked = false;
                enumPathFrequency = EnumPathFrequency.RELATIVE;
            }
        }

        private void absRadio_CheckedChanged(object sender, EventArgs e)
        {
            if (absRadio.Checked)
            {
                relRadio.Checked = false;
                normRadio.Checked = false;
                enumPathFrequency = EnumPathFrequency.ABSOLUTE;
            }
        }

        private void stdRelRadio_CheckedChanged(object sender, EventArgs e)
        {
            if (normRadio.Checked)
            {
                relRadio.Checked = false;
                absRadio.Checked = false;
                enumPathFrequency = EnumPathFrequency.NORMALIZED;
            }
        }

        private void UpdateBars()
        {
            histPosUpperBoundLabel.Text = numVarsBar.Value.ToString();
            histPosBar.Maximum = numVarsBar.Value;
            histPosBar.Value = histPosBar.Minimum;
        }
    }
}
