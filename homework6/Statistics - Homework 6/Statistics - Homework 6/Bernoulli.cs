﻿using System;

namespace Statistics___Homework_6
{
    public class Bernoulli : IRandVar
    {
        Random _random;

        double _p;

        Random IRandVar._random { get => _random; set => _random = value; }
        double IRandVar._p { get => _p; set => _p = value; }

        public Bernoulli(double p)
        {
            if (!IsValidParameterSet(p))
            {
                throw new ArgumentException("Invalid parametrization for the distribution.");
            }

            _random = new Random();
            _p = p;
        }

        public override string ToString()
        {
            return $"Bernoulli(p = {_p})";
        }

        public bool IsValidParameterSet(double p)
        {
            return p >= 0.0 && p <= 1.0;
        }

        public double GetSample()
        {
            if (_random.NextDouble() < _p)
            {
                return 1;
            }

            return 0;
        }
    }
}