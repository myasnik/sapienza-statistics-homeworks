﻿using System;

namespace Statistics___Homework_6
{
    interface IRandVar
    {
        Random _random { get; set; }

        double _p { get; set; }

        public string ToString();

        public bool IsValidParameterSet(double p);

        public double GetSample();
    }
}
