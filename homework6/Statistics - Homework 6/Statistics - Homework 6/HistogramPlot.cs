﻿using System.Collections.Generic;
using System.Drawing;

namespace Statistics___Homework_6
{
    class HistogramPlot
    {
        private List<HistogramUnit> bars;
        private static int FONT_SIZE = 10;
        private static int RECT_H_MULT = 5;
        private EnumOrientation orientation;

        public HistogramPlot(List<HistogramUnit> bars, EnumOrientation orientation)
        {
            Bars = bars;
            Orientation = orientation;
        }

        public HistogramPlot(double len, int steps, Color barColor, EnumOrientation orientation)
        {
            double pos = 0;
            Bars = new List<HistogramUnit>();
            for (int i = 0; i < steps; i++)
            {
                Bars.Add(new HistogramUnit(barColor, new Interval(pos, pos + (len / steps), true, false)));
                pos += (len / steps);
            }
            Orientation = orientation;
        }

        internal List<HistogramUnit> Bars { get => bars; private set => bars = value; }
        internal EnumOrientation Orientation { get => orientation; private set => orientation = value; }

        public void AddBar(HistogramUnit bar)
        {
            Bars.Add(bar);
        }

        public void AddNumber(double val)
        {
            foreach(HistogramUnit u in Bars)
            {
                if (u.Interval.Contains(val))
                {
                    u.Interval.AddElement(val);
                }
            }
        }

        public void DrawPlot(ViewPort view)
        {
            int rectangleWidth;
            double startingX;
            double startingY;

            switch (Orientation)
            {
                case EnumOrientation.BOTTOMUP:
                    // TOFIX in inext assignment
                    rectangleWidth = (int)view.Graph.VisibleClipBounds.Width / Bars.Count;
                    startingX = 0;
                    foreach (HistogramUnit u in Bars)
                    {
                        view.FillRectangle(u.Color, new Rectangle(new Point((int)startingX, (int)(view.Graph.VisibleClipBounds.Height - (u.Interval.Frequency * RECT_H_MULT))), new Size(rectangleWidth, u.Interval.Frequency * RECT_H_MULT)));
                        view.DrawString(u.ToString(), Color.Black, new Point((int)startingX, (int)view.Graph.VisibleClipBounds.Height), "arial", FONT_SIZE);
                        view.DrawLine(Color.Black, new Point((int)startingX + (rectangleWidth / 2), (int)view.Graph.VisibleClipBounds.Height), new Point((int)startingX + (rectangleWidth / 2), (int)(view.Graph.VisibleClipBounds.Height - (u.Interval.Frequency * RECT_H_MULT))));
                        view.DrawString(u.Interval.Mean.CurrentAvg.ToString(), Color.Black, new Point((int)startingX, (int)view.Graph.VisibleClipBounds.Height - (u.Interval.Frequency * RECT_H_MULT) - FONT_SIZE * 3), "arial", FONT_SIZE);
                        startingX += rectangleWidth;
                    }
                    break;
                case EnumOrientation.RIGHTLEFT:
                    // TOFIX in next assignment
                    rectangleWidth = (int)view.Graph.VisibleClipBounds.Height / Bars.Count;
                    startingY = (int)view.Graph.VisibleClipBounds.Height - rectangleWidth;
                    foreach (HistogramUnit u in Bars)
                    {
                        view.FillRectangle(u.Color, new Rectangle(new Point((int)(view.Graph.VisibleClipBounds.Width - (u.Interval.Frequency * RECT_H_MULT)), (int)startingY), new Size(u.Interval.Frequency * RECT_H_MULT, rectangleWidth)));
                        view.DrawString(u.ToString(), Color.Black, new Point((int)view.Graph.VisibleClipBounds.Width, (int)startingY), "arial", FONT_SIZE);
                        view.DrawLine(Color.Black, new Point((int)view.Graph.VisibleClipBounds.Width, (int)startingY + (rectangleWidth / 2)), new Point((int)(view.Graph.VisibleClipBounds.Width - (u.Interval.Frequency * RECT_H_MULT)), (int)startingY + (rectangleWidth / 2)));
                        view.DrawString(u.Interval.Mean.CurrentAvg.ToString(), Color.Black, new Point((int)view.Graph.VisibleClipBounds.Width - (u.Interval.Frequency * RECT_H_MULT) - FONT_SIZE * 3, (int)startingY + FONT_SIZE * 4), "arial", FONT_SIZE);
                        startingY -= rectangleWidth;
                    }
                    break;
                case EnumOrientation.LEFTRIGHT:
                    startingY = Bars[0].Interval.UpperEnd - Bars[0].Interval.LowerEnd;
                    foreach (HistogramUnit u in Bars)
                    {
                        view.FillRectangle(u.Color, new RectangleF(0, (float)startingY, u.Interval.Frequency, (float)(u.Interval.UpperEnd - u.Interval.LowerEnd)));
                        view.DrawString(u.ToString(), Color.Black, new PointF(0, (float)startingY), "arial", FONT_SIZE);
                        startingY += u.Interval.UpperEnd - u.Interval.LowerEnd;
                    }
                    break;
            }
        }
    }
}
