﻿using System;

namespace Statistics___Homework_6
{
    class OnlineMean
    {
        private double currentAvg = 0;
        private int index = 0;

        public double CurrentAvg { get => Math.Round(currentAvg, 2); private set => currentAvg = value; }
        public int Index { get => index; private set => index = value; }

        public void AddValue(double val)
        {
            Index++;
            CurrentAvg = CurrentAvg + ((val - CurrentAvg) / Index);
        }
    }
}
